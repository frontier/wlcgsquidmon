//TODO make a reliable sorting of the hosts-table
//TODO add zoom capability to the history chart

var Failover = new function()
    {
    var self = this;
    // JS weirdness: By default, when not specified otherwise, functions
    //  have the object "this" pointing to the Global scope (a.k.a. "Window")
    //  So to reference to this object, use "self" instead.
    self.config_file = "config.json";
    self.time_chart = dc.barChart("#time-chart");
    self.group_chart = dc.pieChart("#group-chart");
    self.hosts_table = dc.dataTable("#hosts-table");
    self.date_format = d3.time.format("%b %d, %Y %I:%M %p");
    self.sites_legend_item_size = 19;
    self.sites_legend_item_gap = 1;
    self.time_chart_width = 1024;
    self.time_chart_height = 500;
    self.groups_base_dim = 200;
    self.groups_legend_width = 200;
    self.groups_radius = self.groups_base_dim/2 - 15;
    self.time_zone_offset = 0;
    self.start = function()
        {
        d3.json(self.config_file,
            function (error, config)
                {
                self.create_objects(config);
                let experiment = config["experiment"]
                if (experiment == "Cvmfs") {
                    document.title = "CVMFS Failover History"
                    document.getElementById("header").innerHTML = document.title;
                } else {
                    document.title = "Frontier Failover History (" + experiment + ")"
                    document.getElementById("header").innerHTML = "Frontier Failover History";
                }
                self.record_file_nosquid = "failover-record-nosquid.tsv"
                self.record_file_emails = "email-record.tsv"
                queue().defer(d3.tsv, self.record_file_nosquid)
                       .defer(d3.tsv, self.record_file_emails)
                       .await(self.first_setup);
                }
            );
        };
    self.create_objects = function(config)
        {
        self.config = config;
        //alert(self.config.toSource());
        self.period = config.history.period;
        self.extent_span = 3.6e6 * config.history.span;
        self.periodObj = minuteBunch(self.period);
        self.periodRange = self.periodObj.range;
        self.addH = function(p, d) { return p + d.Hits; };
        self.remH = function(p, d) { return p - d.Hits; };
        self.ini = function() { return 0; };
        self.ndx = crossfilter();
        self.all = self.ndx.groupAll().reduce(self.addH, self.remH, self.ini);
        self.site_D = self.ndx.dimension(dc.pluck('Sites'))
        self.bandwidth_D = self.ndx.dimension(dc.pluck('Bandwidth'));
        self.hits_D = self.ndx.dimension(dc.pluck('Hits'));
        self.host_D = self.ndx.dimension(dc.pluck('Host'));
        self.group_D = self.ndx.dimension(
            function(d)
                {
                return self.config.groups[d.Group];
                }
            );
        self.time_site_D = self.ndx.dimension( function(d) { return [d.Timestamp, d.Sites]; });
        self.group_G = self.group_D.group().reduce(self.addH, self.remH, self.ini);
        self.squid_G = self.site_D.group().reduce(self.addH, self.remH, self.ini);
        self.time_sites_G = self.time_site_D.group().reduce(self.addH, self.remH, self.ini);
        self.hits_G = self.hits_D.group().reduce(self.addH, self.remH, self.ini);
        self.TimestampDimension = self.ndx.dimension(function(d) {return d.Timestamp;});
        self.HitsSumGroup = self.TimestampDimension.group().reduce(
            function(p, v) {p[v.Sites] = (p[v.Sites] || 0) + v.Hits;return p;},
            function(p, v) {p[v.Sites] = (p[v.Sites] || 0) - v.Hits;return p;},
            function() {return {};}
            );
        };
    self.first_setup = function(error, dataset, emails)
        {
        self.setup_base();
        self.setup_update(dataset, emails);
        dc.renderAll();
        self.apply_url_filters();
        };
    self.setup_base = function()
        {
        // The time series chart
        self.time_chart
            .width(self.time_chart_width)
            .height(self.time_chart_height)
            .xAxisLabel("Time")
            .elasticY(true)
            .renderHorizontalGridLines(true)
            .brushOn(false)
            .renderlet(
            function(chart)
                {
                chart.selectAll(".dc-legend-item").on("click", function(d) { self.site_filter(d.name); });
                chart.selectAll(".sub .bar").on("click", function(d) { self.site_filter(d.layer); });
                }
            );
        self.time_chart.xAxis().ticks(d3.time.hours, 2);
        // Add listeners to rotate labels and refresh data table
        var rotate_fun = function(d)
            {
            return "rotate(-90) translate(-25, -12)";
            };
        var axis_tick_rotate = function(chart)
            {
            chart.selectAll("svg g g.axis.x g.tick text").attr("transform", rotate_fun);
            }
        self.time_chart.on("postRedraw", axis_tick_rotate);
        self.time_chart.on("postRender", axis_tick_rotate);
        // Site filtering actions
        self.site_filter = function (name)
            {
            var short_n = name.split('\n')[0] + (name.contains('\n') ? ', ...' : '');
            self.site_D.filterExact(name);
            self.time_chart.turnOnControls();
            dc.redrawAll();
            // This must be run after redrawAll, else it does not render
            self.time_chart.select('.filter').text(short_n).property('title', name);
            // Blind update of URL query string
            history.pushState(null, '', '?site=' + encodeURIComponent(name));
            //alert(name.toSource());
            }
        // Color map
        let colorScheme = d3.scale.category20();
        let groups = Object.values(self.config.groups);
        self.groupChartColors = Object.assign({}, ...groups.map((x, i) => ({[x]: colorScheme(i)})))
        // The group chart
        self.group_chart
            .width(self.groups_base_dim)
            .height(self.groups_base_dim)
            .radius(self.groups_radius)
            .innerRadius(0.3*self.groups_radius)
            .dimension(self.group_D)
            .group(self.group_G)
            .minAngleForLabel(0)
            .colors(function(d) {return self.groupChartColors[d]; })
            .title(function(d) { return d.key + ": " + d.value + " Hits"; })
            .label(
            function (d)
                {
                if (self.group_chart.hasFilter() && !self.group_chart.hasFilter(d.key))
                    {
                    return "0%";
                    }
                return (100 * d.value / self.all.value()).toFixed(2) + "%";
                }
            ).legend( dc.legend().x(self.groups_base_dim).y(50).gap(10).legendText(function(d, i) { return d.name;}) );
        // Table widget for displaying failover details
        self.sort_order = {false: d3.ascending,true: d3.descending};
        self.current_sort_order = false;
        self.table_field_map = { 'Host': 'Host', 'Time': 'Timestamp', 'Hits': 'Hits','Bandwidth' : 'Bandwidth' };
        self.hosts_table_filter_control = d3.select('#ht-reset');
        self.hosts_table.dimension(self.site_D).group(function(d) { return d.Sites; }).columns(
            [
            function(d)
                {
                var host = d.Host;
                return '<span title="IP: ' + d.Ip + '">' + d.Host + '</span>';
                },
            function(d) { return self.date_format(d.Timestamp); },
            function(d) { return '<span title="~ ' + d.HitsRate.toFixed(2) + ' queries/sec">' + d.Hits + '</span>'; },
            function(d) { return '<span title="~ ' + size_natural(d.BandwidthRate) + '/sec">' + size_natural(d.Bandwidth) + '</span>'; }
            ]
            ).sortBy(dc.pluck('Timestamp')).order(d3.descending).size(Infinity).renderlet(function(table){table.selectAll(".dc-table-group").classed("info", true);}).renderlet(
            function(table)
                {
                table.selectAll(".dc-table-column._0").on('click',
                    function()
                        {
                        var host = d3.select(this).select('span').text();
                        self.host_D.filterExact(host);
                        self.hosts_table_filter_control.selectAll('.reset').style('display', null);
                        self.hosts_table_filter_control.selectAll('.filter').style('display', null);
                        dc.redrawAll();
                        // This must be run after redrawAll, else it does not render
                        self.hosts_table_filter_control.selectAll('.filter').text('Host ' + host);
                        }
                    )
                }
            );
        // Sorting functionality of fields
        self.hosts_table_headers = self.hosts_table.selectAll('thead th');
        self.hosts_table_headers.on("click",
            function(d)
                {
                var header = d3.select(this);
                var selected = header.select('.header-text').text();
                var field = self.table_field_map[selected];
                var glyph = header.select('.glyphicon');
                var all = self.hosts_table_headers.select('.glyphicon');
                self.hosts_table_headers.classed('header-inactive',
                    function()
                        {
                        var current = d3.select(this).select('.header-text').text();
                        return !(current == selected);
                        }
                    );
                self.hosts_table_headers.classed('header-active',
                    function()
                        {
                        var current = d3.select(this).select('.header-text').text();
                        return (current == selected);
                        }
                    );
                self.current_sort_order = !self.current_sort_order;
                glyph.classed({'glyphicon-chevron-down': self.current_sort_order,'glyphicon-chevron-up': !self.current_sort_order});
                self.hosts_table.order(self.sort_order[self.current_sort_order]);
                self.hosts_table.sortBy(dc.pluck(field));
                self.hosts_table.redraw();
                }
            );
        };
    self.setup_update = function(dataset, emails)
        {
        self.allowed_name_length = 50
        //alert(dataset.toSource());
        //alert(emails.toSource());
        self.dataset = dataset;
        self.emails_rec = emails;
        self.set_time_offset();
        self.ndx.add(self.parse_failover_records(dataset));
        self.emails_data = self.parse_email_records(emails);
        self.site_list = self.site_D.group().all().map(dc.pluck('key'));
        //alert(self.site_list.toSource());
        self.site_names_len = flatten_array(//site name length in number of letters
            self.site_list.map(
                function(s)
                    {
                    //alert(s.split('\n').toSource());
                    return s.split('\n').map( function(s){ return s.length });
                    }
                )
            );
        self.site_names_len_px = flatten_array(//site name length in pixels
            self.site_list.map(
                function(s)
                    {
                    var container = d3.select('body').append('svg');
                    container.append('text').attr("x", -99999).attr("y", -99999).attr("font-size", 11).text(s);//.dc-legend {font-size: 11px;} in dc.css
                    //container.append('text').attr({ x: -99999, y: -99999 }).text(s);
                    var size = container.node().getBBox();
                    container.remove();
                    //console.log(s.split('\n')+" "+size.width);
                    return size.width;
                    }
                )
            );
        //alert(self.site_names_len_px.toSource());
        if (Math.max.apply(0, self.site_names_len) > self.allowed_name_length)
            {
            self.site_longest_name = 350//length of 50 something characters in px
            }else{
            self.site_longest_name = Math.max.apply(0, self.site_names_len_px);//length of longest site name in pixels
            }
        self.legend_line_length = self.site_longest_name + 28; //from dc.css(label has 3px padding from left and right + 12px of color box + 5px of left and right margin of item div)
        //alert(self.legend_line_length.toSource());
        self.num_lines = self.site_names_len.length;//number of sites
        //alert(self.num_lines.toSource());
        self.sites_legend_space_v = self.num_lines * (self.sites_legend_item_size + self.sites_legend_item_gap);//nlines*height per line=total height
        //alert(self.sites_legend_space_v.toSource());
        //<22
        self.sites_legend_columns1 = 1;//only one columns on the side (when there is <22 sites)
        //alert(self.sites_legend_columns1.toSource());
        self.sites_legend_space_h1 = (self.legend_line_length) * self.sites_legend_columns1;
        //alert(self.sites_legend_space_h1.toSource());
        //>22
        //alert((self.time_chart_width/(self.site_longest_name)).toSource());
        if ((self.time_chart_width/(self.legend_line_length))-Math.floor(self.time_chart_width/(self.legend_line_length))>0.5)
            {
            self.sites_legend_columns2 = Math.ceil(self.time_chart_width/(self.legend_line_length));
            }else{
            self.sites_legend_columns2 = Math.floor(self.time_chart_width/(self.legend_line_length));
            }

        //alert(self.sites_legend_columns2.toSource());
        self.sites_legend_space_v_total2 = (self.sites_legend_space_v/self.sites_legend_columns2);
        //alert(self.sites_legend_space_v_total2.toSource());
        self.update_time_extent(self.period, self.extent_span);
        // The time series chart
        self.sites_color_scale = hsl_set(self.site_list.length, 70, 50);
        if (self.num_lines < 22)
            {
            var customWidth = self.time_chart_width;
            var customHeight = self.time_chart_height;
            var customMargin = { top: 30, right: 30 + self.sites_legend_space_h1, bottom: 70, left: 80 };
            var customLegend = dc.legend().x(1024-self.sites_legend_space_h1).y(10).itemWidth(150).itemHeight(self.sites_legend_item_size).gap(self.sites_legend_item_gap);
            }else{
            var customWidth = self.sites_legend_columns2*self.legend_line_length;
            var customHeight = self.time_chart_height+self.sites_legend_space_v_total2;
            var customMargin = { top: 20+self.sites_legend_space_v_total2, right: 30 , bottom: 100, left: 80 };
            //var customLegend = dc.legend().x(30).y(5).legendWidth(self.time_chart_width).itemWidth(self.time_chart_width/self.sites_legend_columns2).itemHeight(self.sites_legend_item_size).gap(1).horizontal(true);
            var customLegend = dc.legend().x(30).y(5).legendWidth(self.sites_legend_columns2*self.legend_line_length).itemWidth(self.legend_line_length).itemHeight(self.sites_legend_item_size).gap(self.sites_legend_item_gap).horizontal(true);
            }
        function sel_stack(i)
            {
            return function(d)
                {
                if(d.value[i] == null)
                    {
                    return 0;
                    }else{
                    return d.value[i];
                    }
                };
            }
        self.time_chart
            .yAxisLabel("Hits")
            .width(customWidth)
            .height(customHeight)
            .dimension(self.TimestampDimension)
            .group(self.HitsSumGroup, self.site_list[0], sel_stack(self.site_list[0]))
            .margins(customMargin)
            .title(function(d) { return this.layer + ': ' + d.value[this.layer]; })
            .ordinalColors(self.sites_color_scale)
            .x(d3.time.scale().domain(self.extent))
            .xUnits(self.periodRange)
            //.y(yScale)
            .legend(customLegend);
        for(var i = 1; i<self.site_list.length; ++i)
            {
            if (self.site_list[i].length > self.allowed_name_length)
                {
                //alert(self.site_list[i].substring(0,50)+'...');
                self.time_chart.stack(self.HitsSumGroup, self.site_list[i].substring(0,self.allowed_name_length)+'...', sel_stack(self.site_list[i].substring(0,self.allowed_name_length)+'...'));
                }else{
                self.time_chart.stack(self.HitsSumGroup, self.site_list[i], sel_stack(self.site_list[i]));
                }
            }
        self.time_chart.render();
        // Email record
        self.email_table_update(self.emails_data);
        d3.select('#email-history').text(self.config["history"]["span"]);
        d3.select('#email-period').text(self.config["emails"]["periodicity"]);
        };
    self.set_time_offset = function()
        {
        var offsets = {"local": new Date().getTimezoneOffset(),"utc": 0,"cern": -120};
        var e = document.getElementById("timezone"),chosen_offset = e.options[e.selectedIndex].value;
        // D3 or JS will be default render times in local zone
        self.time_zone_offset = -60e3*(offsets[chosen_offset] - offsets['local']);
        };
    self.email_table_update = function(emails_data)
        {
        var table = d3.select("#emails-table-div table");
        table.selectAll("tbody").remove();
        var body = table.append("tbody"), rows = body.selectAll("tr").data(emails_data).enter().append("tr"),columns =
            [
            function(d) { return '<ul class="no-bullets"><li>' + d.Sites.split('\n').join(';</li><li>') + "</li></ul>"; },
            function(d) { return '<ul class="no-bullets"><li>' + d.Addresses.join('</li><li>') + "</li></ul>"; },
            function(d) { return self.date_format(d.Timestamp); }
            ];
        columns.forEach(
            function(f, i)
                {
                rows.append("td").html(f);
                }
            );
        };
    self.parse_failover_records = function(dataset)
        {
        // The input element needs to remain intact, so a
        // copy is prepared
        //alert(dataset.toSource());
        var processed = [];
        dataset.forEach(
            function(d)
                {
                var row = {};
                // The timestamp points to the end of a period.
                //  this must be accounted for for plotting.
                //alert(d.Timestamp.toSource());
                row.Timestamp = new Date(1e3*(+d.Timestamp - 3600) + self.time_zone_offset);
                row.Timestamp.setMinutes(0);
                row.Timestamp.setSeconds(0);
                //alert(d.Hits.toSource());
                row.Hits = +d.Hits;
                //alert(d.HitsRate.toSource());
                row.HitsRate = +d.HitsRate;
                //alert(d.Bandwidth.toSource());
                row.Bandwidth = +d.Bandwidth;
                //alert(d.BandwidthRate.toSource());
                row.BandwidthRate = +d.BandwidthRate;
                //alert(d.Sites.toSource());
                row.Sites = d.Sites;
                //alert(d.Host.toSource());
                row.Host = d.Host;
                //alert(d.Ip.toSource());
                row.Ip = d.Ip;
                //alert(d.Group.toSource());
                row.Group = d.Group;
                processed.push(row);
                }
            );
        return processed;
        };
    self.parse_email_records = function(emails)
        {
        // The input element needs to remain intact, so a
        // copy is prepared
        var processed = [];
        emails.forEach(
            function(d)
                {
                var row = {};
                row.Timestamp = new Date(1000 * (+d.Timestamp));
                row.Timestamp.setMinutes(0);
                row.Timestamp.setSeconds(0);
                row.Sites = d.Sites.replace(/; /g, '\n');
                row.Addresses = d.Addresses.replace(/@/g, '_AT_').split(", ");
                processed.push(row);
                }
            );
        return processed;
        };
    self.reload = function()
        {
        queue().defer(d3.tsv, self.record_file_nosquid).defer(d3.tsv, self.record_file_emails).await(
            function (error, dataset, emails)
                {
                self.ndx.remove();
                self.setup_update(dataset, emails);
                dc.redrawAll();
                }
            );
        };
    self.redraw_offset = function()
        {
        self.ndx.remove();
        self.setup_update(self.dataset, self.emails_rec);
        dc.renderAll();
        };
    self.update_time_extent = function(period, extent_span)
        {
        var periodObj = minuteBunch(period);
        var periodRange = periodObj.range;
        var hour = 3.6e6;
        var now = new Date();
        //var now = new Date(2018, 4, 16, 00, 00);//10
        //var now = new Date(2018, 9, 23, 14, 00);//32
        //var now = new Date(2018, 11, 19, 18, 00);//99
        var this_hour = periodObj(now).getTime() + self.time_zone_offset;
        var extent = [new Date(this_hour - extent_span),new Date(this_hour)];
        var extent_pad = [new Date(this_hour - extent_span - hour),new Date(this_hour + hour)];
        // Do show the currently plotted time span
        d3.select("#date-start").attr("datetime", extent[0]).text(self.date_format(extent[0]));
        d3.select("#date-end").attr("datetime", extent[1]).text(self.date_format(extent[1]));
        self.extent = extent_pad;
        };
    self.time_chart_reset = function()
        {
        self.site_D.filterAll();
        self.time_chart.turnOffControls();
        dc.redrawAll();
        // Blind update of URL query string
        history.pushState(null, '', '?');
        };
    self.hosts_table_reset = function()
        {
        self.host_D.filterAll();
        self.hosts_table_filter_control.selectAll('.reset').style('display', 'none');
        self.hosts_table_filter_control.selectAll('.filter').style('display', 'none');
        dc.redrawAll();
        };
    self.apply_url_filters = function()
        {
        var url_params = getUrlVars();
        if (url_params.length > 0)
            {
            if ("site" in url_params)
                {
                self.site_filter(url_params["site"])
                }
            }
        };
    };

Failover.start();
