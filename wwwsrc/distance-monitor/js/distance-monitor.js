let Pages = new function() {
    let self = this;
    // JS weirdness: By default, when not specified otherwise, functions
    // have the object "this" pointing to the Global scope (a.k.a. "Window")
    // So to reference to this object, use "self" instead.
    self.date_format = d3.timeFormat('%b %d, %Y %I:%M %p');
    // Initial time chart dimensions
    self.timeSvgSize = {width: 1140, height: 550};
    self.timeChartMargin = {top: 20, right: 30, bottom: 70, left: 80 };
    self.timeChartSize = {width: self.timeSvgSize.width - self.timeChartMargin.left - self.timeChartMargin.right,
                          height: self.timeSvgSize.height - self.timeChartMargin.bottom - self.timeChartMargin.top};
    self.timeChartBarMargin = 0.75;
    // Initial legend dimensions
    self.legendMargin = {top: 20, right: 30, bottom: 10, left: 80};
    self.legendSize = {width: self.timeChartSize.width, height: 10};
    // Initial group chart dimensions
    self.groupSvgSize = {width: 1140, height: 200};
    self.groupChartMargin = {top: 30, right: 30, bottom: 70, left: 400 };
    self.groupChartSize = {width: self.groupSvgSize.width - self.groupChartMargin.left - self.groupChartMargin.right,
                           height: self.groupSvgSize.height - self.groupChartMargin.bottom - self.groupChartMargin.top};
    self.groupChartBarMargin = 3.5;
    self.groupChartBarHeight = 30;

    // Selected pages
    self.selectedPages = [];
    // Selected machine groups
    self.selectedGroups = [];
    // Max value in time chart - top of highest bar
    self.timeChartMaxY = -1;
    // Offset from local timezone
    self.timezoneOffset = 0;
    // Animation length
    self.animationLength = 200;
    // Parse URL parameters
    let urlParams = Object.fromEntries(new URLSearchParams(window.location.search));
    if ('pages' in urlParams) {
        self.selectedPages = urlParams['pages'].split(',').filter(Boolean);
    }
    if ('groups' in urlParams) {
        self.selectedGroups = urlParams['groups'].split(',').filter(Boolean);
    }
    if ('experiment' in urlParams) {
        self.experiment = urlParams['experiment']
        self.configFile = 'config_' + self.experiment + '.json';
    }

    document.title = "Average distance between client and server (" + self.experiment + ")"

    self.start = function() {
        // Downlowd config
        d3.json(self.configFile).then(function (config) {
            self.config = config;
            // Period in seconds
            self.period = config.history.period * 60 * 1000;
            // Total period in seconds
            self.totalPeriod = config.history.span * 3600 * 1000;
            self.updateTimeRange();
            self.downloadRecords();
        });
    };
    self.downloadRecords = function() {
        d3.tsv('records_' + self.experiment + '.tsv').then(function (dataset) {
            self.dataset = self.parseRecords(dataset);
            self.pickRecords();
            self.prepareGroupChartDictionaries();
            self.prepareTimeChartDictionaries();

            self.calculateGroupAverages();
            self.initializeGroupChart();
            self.drawGroupChart();

            self.calculatePageAverages();
            self.initializeTimeChart();
            self.drawTimeChart();
            self.drawTimeChartLegend();

            self.updateUrlSearchParams();
            self.updateSelectedDisplay();
        });
    }
    self.reload = function() {
        self.downloadRecords();
    }
    self.resetGroups = function() {
        self.toggleGroup();
    }
    self.resetPages = function() {
        self.togglePage();
    }

    self.prepareGroupChartDictionaries = function() {
        // Get list of unique group names
        self.allGroups = Object.keys(self.config.groups);
        // Full names
        self.fullGroupNames = self.config.groups;
        // Reverse group names
        self.reverseFullGroupNames = Object.assign({}, ...self.allGroups.map(x => ({[self.fullGroupNames[x]]: x})));
        // CSS class map
        self.groupClasses = Object.assign({}, ...self.allGroups.map(x => ({[x]: 'group-' + x.replace(/[\W_-]+/g, "_")})));
        // Color map
        let colorScheme = d3.scaleOrdinal(d3.schemePaired);
        self.groupColors = Object.assign({}, ...self.allGroups.map((x, i) => ({[x]: colorScheme(i)})));
    }
    self.pickRecords = function() {
        let filteredRecords = self.dataset;
        if (self.selectedGroups.length > 0) {
            filteredRecords = filteredRecords.filter(e => self.selectedGroups.includes(e.Group));
        }

        let byTimestampAndPage = {};
        for (let item of filteredRecords) {
            if (!(item.Timestamp in byTimestampAndPage)) {
                byTimestampAndPage[item.Timestamp] = {};
            }
            if (!(item.Page in byTimestampAndPage[item.Timestamp])) {
                byTimestampAndPage[item.Timestamp][item.Page] = [];
            }
            byTimestampAndPage[item.Timestamp][item.Page].push(item);
        }
        self.byTimestampAndPage = byTimestampAndPage;

        let pickedRecords = {};
        for (let timestamp of Object.keys(byTimestampAndPage)) {
            let oneHour = [];
            for (let page of Object.keys(byTimestampAndPage[timestamp])) {
                let items = byTimestampAndPage[timestamp][page];
                let hitsSum = d3.sum(items, d => d.Hits);
                let distanceSum = d3.sum(items, d => d.Distance * d.Hits);
                let distanceAvg = distanceSum / hitsSum;
                oneHour.push({'page': page,
                              'average': distanceAvg,
                              'items': items})
            }
            oneHour = oneHour.sort((a, b) => b.average - a.average).slice(0, 10).reverse();
            if (self.selectedPages.length > 0) {
                oneHour = oneHour.filter(e => self.selectedPages.includes(e.page));
            }
            pickedRecords[timestamp] = {};
            for (let page of oneHour) {
                pickedRecords[timestamp][page.page] = page.items;
            }
        }
        self.pickedRecords = pickedRecords;
    }
    self.calculateGroupAverages = function() {
        let groups = {};
        for (let timestamp of Object.keys(self.pickedRecords)) {
            for (let page of Object.keys(self.pickedRecords[timestamp])) {
                for (let item of self.pickedRecords[timestamp][page]) {
                    if (!(item.Group in groups)) {
                        groups[item.Group] = [];
                    }
                    groups[item.Group].push(item);
                }
            }
        }
        let groupAverages = [];
        for (let group of Object.keys(groups)) {
            let items = groups[group];
            let hitsSum = d3.sum(items, d => d.Hits);
            let distanceSum = d3.sum(items, d => d.Distance * d.Hits);
            let distanceAvg = distanceSum / hitsSum;
            groupAverages.push({'group': group,
                                'name': self.fullGroupNames[group],
                                'distance': distanceAvg,
                                'hits': hitsSum})
        }
        for (let group of self.allGroups) {
            if (!groupAverages.map(x => x.group).includes(group)) {
                groupAverages.push({'group': group,
                                    'name': self.fullGroupNames[group],
                                    'distance': 0,
                                    'hits': 0});
            }
        }
        groupAverages = groupAverages.sort((a, b) => (a.name > b.name) ? 1 : -1);
        self.groupAverages = groupAverages;
    }
    self.initializeGroupChart = function() {
        // Set height based on number of groups
        self.groupChartSize.height = self.allGroups.length * (self.groupChartBarHeight + 2 * self.groupChartBarMargin);
        self.groupSvgSize.height = self.groupChartSize.height + self.groupChartMargin.bottom + self.groupChartMargin.top;

        // Set width based on group name length
        let longestGroupName = Math.min(d3.max(self.allGroups, x => self.labelWidth(self.fullGroupNames[x])), self.groupSvgSize.width * 0.75)
        self.groupChartMargin.left = longestGroupName + 20;
        self.groupSvgSize.width = self.timeChartMargin.left + self.timeChartSize.width + self.groupChartMargin.right;
        self.groupChartSize.width = self.groupSvgSize.width - self.groupChartMargin.left - self.groupChartMargin.right;

        d3.select('#group-chart').selectAll('svg').remove()
        let svg = d3.select('#group-chart')
                    .append('svg')
                    .attr('width', self.groupSvgSize.width)
                    .attr('height', self.groupSvgSize.height)

        let chart = svg.append('g')
                       .attr('transform', 'translate(' + self.groupChartMargin.left + ',' + self.groupChartMargin.top + ')');

        // X Axis
        let x = d3.scaleLinear().range([0, self.groupChartSize.width])
        let xAxis = d3.axisBottom(x);

        // Y Axis
        let y = d3.scaleLinear().range([0, self.groupChartSize.height])
        let yAxis = d3.axisLeft(y).ticks(self.allGroups.length)

        // Add x axis
        chart.append('g').attr('class', 'x axis').attr('transform', 'translate(0,' + self.groupChartSize.height + ')')

        // Add y axis
        chart.append('g').attr('class', 'y axis')

        // Add x axis title
        svg.append('text')
           .text('Average distance, km')
           .style('text-anchor', 'middle')
           .attr('font-size', '12')
           .attr('font-family', 'sans-serif')
           .attr('transform', 'translate(' + (self.groupChartMargin.left + self.groupChartSize.width / 2) + ', ' + (self.groupSvgSize.height - 15) + ')')

        // Save as global variables
        self.groupChart = chart;
        self.groupChartAxisX = xAxis;
        self.groupChartAxisY = yAxis;
        self.groupChartX = x;
        self.groupChartY = y;

        self.updateGroupChartAxisX()
        self.updateGroupChartAxisY()
    }
    self.drawGroupChart = function() {
        self.groupChart.selectAll('rect').remove()
        self.groupChart.selectAll('rect')
                       .data(self.groupAverages)
                       .enter()
                       .append('rect')
                       // Initial bar position and size
                       .attr('class', function(d) { return 'group-bar ' + self.groupClasses[d.group]; })
                       .style('opacity', 1)
                       .style('fill', function(d) { return self.groupColors[d.group]; })
                       .attr('transform', function(d, i) {
                           return 'translate(' + (self.groupChartX(0) + 1) + ',' +  (self.groupChartY(i + 1) - self.groupChartBarHeight / 2 + self.groupChartBarMargin / 2) + ')';
                       })
                       .attr('width', 0)
                       .attr('height', self.groupChartBarHeight)
                       // Tooltip
                       .append('svg:title').text(function(d){ return d.name + ': ' + d.distance.toFixed(2) + ' km (' + d.hits + ' hits)';})
                       .select(function() { return this.parentNode; }) // Return to rect after adding title

                       // Mouse actions
                       .on('mousemove', function(event, d) {
                           d3.selectAll('.group-bar').style('opacity', 0.3);
                           d3.selectAll('.group-bar.' + self.groupClasses[d.group]).style('opacity', 1);
                       })
                       .on('mouseout', function(d) {
                           d3.selectAll('.group-bar').style('opacity', 1);
                       })
                       .on('mousedown', function(event, d) {
                           self.toggleGroup(d.group);
                       })
                       // Animation
                       .transition()
                       .duration(self.animationLength)
                       .ease(d3.easeBackOut.overshoot(1.75))
                       .attr('width', function(d) {return Math.max(0, self.groupChartX(d.distance) - 1)})
    }
    self.toggleGroup = function(group) {
        if (self.selectedGroups.length == 0 || !group) {
            self.selectedGroups = group ? [group] : [];
        } else {
            if (self.selectedGroups.includes(group)) {
                self.selectedGroups = self.selectedGroups.filter(x => x != group);
            } else {
                self.selectedGroups.push(group);
                if (self.selectedGroups.length == self.allGroups.length) {
                     self.selectedGroups = [];
                 }
            }
        }
        self.pickRecords();
        self.prepareTimeChartDictionaries();

        self.calculateGroupAverages();
        self.updateGroupChartAxisX();
        self.drawGroupChart();

        self.calculatePageAverages();
        self.updateTimeChartAxisY();
        self.drawTimeChart();
        self.drawTimeChartLegend();

        self.updateUrlSearchParams();
        self.updateSelectedDisplay();
    }

    self.prepareTimeChartDictionaries = function() {
        let byTimestampAndPage = self.byTimestampAndPage;
        let pages = [];
        for (let timestamp of Object.keys(byTimestampAndPage)) {
            let oneHour = [];
            for (let page of Object.keys(byTimestampAndPage[timestamp])) {
                let items = byTimestampAndPage[timestamp][page];
                let hitsSum = d3.sum(items, d => d.Hits);
                let distanceSum = d3.sum(items, d => d.Distance * d.Hits);
                let distanceAvg = distanceSum / hitsSum;
                oneHour.push({'page': page,
                              'average': distanceAvg})
            }
            oneHour = oneHour.sort((a, b) => b.average - a.average).slice(0, 10).reverse();
            for (let page of oneHour) {
                pages.push(page.page)
            }
        }

        self.allPages = Array.from(new Set(pages)).sort();

        // Short page names
        self.shortPageNames = Object.assign({}, ...self.allPages.map((x) => ({[x]: x.length > 50 ? x.substring(0, 47) + '...' : x})));
        // Color map
        self.pageColors = Object.assign({}, ...self.allPages.map((x, i) => ({[x]: 'hsl(' + (i * (360 / self.allPages.length)) % 360 + ', ' + (60 + (10 - i % 3 * 10)) + '%, ' + (40 + i % 3 * 10) + '%)'})));
        // CSS class map
        self.pageClasses = Object.assign({}, ...self.allPages.map((x, i) => ({[x]: 'page-' + i})));
    }
    self.calculatePageAverages = function() {
        let bars = [];
        let column = 0;
        for (let timestamp of Object.keys(self.pickedRecords).map(x => parseInt(x)).sort()) {
            let barSum = 0;
            let oneHourBars = []
            for (let page of Object.keys(self.pickedRecords[timestamp]).sort()) {

                let items = self.pickedRecords[timestamp][page];
                let hitsSum = d3.sum(items, d => d.Hits);
                let distanceAvg = d3.sum(items, d => d.Distance * d.Hits) / hitsSum;
                let bar = {'distance': distanceAvg,
                           'page': page,
                           'hits': hitsSum,
                           'date': timestamp,
                           'column': column};
                oneHourBars.push(bar)
            }
            oneHourBars = oneHourBars.sort((a, b) => b.distance - a.distance).reverse();
            for (let bar of oneHourBars) {
                bar['start'] = barSum;
                bar['end'] = barSum + bar['distance'];
                bars.push(bar)
                barSum += bar['distance'];
            }
            column += 1;
        }
        self.timeChartBars = bars;
    }

    self.initializeTimeChart = function() {
        d3.select('#time-chart').selectAll('svg').remove()
        let svg = d3.select('#time-chart')
                    .append('svg')
                    .attr('id', 'time-chart-svg')
                    .attr('width', self.timeSvgSize.width)
                    .attr('height', self.timeSvgSize.height)

        let chart = svg.append('g')
                       .attr('id', 'timechart-box')
                       .attr('transform', 'translate(' + self.timeChartMargin.left + ',' + self.timeChartMargin.top + ')');

        // X Axis
        let x = d3.scaleTime().range([0, self.timeChartSize.width])
        let xAxis = d3.axisBottom(x).ticks(d3.timeHour.every(2));

        // Y Axis
        let y = d3.scaleLinear().range([0, self.timeChartSize.height])
        let yAxis = d3.axisLeft(y);

        // Add x axis
        chart.append('g').attr('class', 'x axis').attr('transform', 'translate(0,' + self.timeChartSize.height + ')')

        // Add y axis
        chart.append('g').attr('class', 'y axis')

        // Add x axis title
        chart.append('text')
             .text('Average distance, km')
             .style('text-anchor', 'middle')
             .attr('font-size', '12')
             .attr('font-family', 'sans-serif')
             .attr('transform', 'rotate(-90) translate(-' + (self.timeChartSize.height / 2) + ', -' + (self.timeChartMargin.left - 15) + ')')

        // Add y axis title
        chart.append('text')
             .text('Time')
             .style('text-anchor', 'middle')
             .attr('font-size', '12')
             .attr('font-family', 'sans-serif')
             .attr('transform', 'translate(' + (self.timeChartSize.width / 2) + ', ' + (self.timeChartSize.height + self.timeChartMargin.bottom - 15) + ')')

        // Save as global variables
        self.timeChartSvg = svg;
        self.timeChart = chart;
        self.timeChartAxisX = xAxis;
        self.timeChartAxisY = yAxis;
        self.timeChartX = x;
        self.timeChartY = y;

        self.timeChartMaxY = 0;
        self.updateTimeChartAxisX();
        self.updateTimeChartAxisY();
    }

    self.updateTimeChartAxisX = function() {
        self.timeChartX.domain(self.timeRange);
        self.timeChartAxisX.scale(self.timeChartX)
        self.timeChart.selectAll('.x.axis')
                      .transition()
                      .duration(self.animationLength)
                      .call(self.timeChartAxisX);
        // Rotate X axis labels 45 degrees
        self.timeChart.selectAll('.x.axis .tick>text')
                      .attr('transform', 'rotate(-45) translate(-5, -5)')
        self.timeChart.selectAll('.x.axis .tick>text')
                      .style('text-anchor', 'end')
        // Bar width depends on domain
        let now = new Date();
        self.timeChartBarWidth = self.timeChartX(now) - self.timeChartX(now - self.period) - 2 * self.timeChartBarMargin;
    }
    self.updateTimeChartAxisY = function() {
        // Add 5% margin
        let maxY = d3.max(self.timeChartBars, x => x.end);
        if (!maxY) {
            maxY = 1;
        }
        self.timeChartY.domain([Math.max(1, maxY * 1.05), 0])
        self.timeChartAxisY.scale(self.timeChartY)
        self.timeChart.selectAll('.y.axis')
                      .transition()
                      .duration(self.animationLength)
                      .call(self.timeChartAxisY);
        self.timeChart.selectAll('.x.axis .tick>text')
                      .style('text-anchor', 'end')
    }
    self.updateGroupChartAxisX = function() {
        self.groupChartX.domain([0, Math.max(1, d3.max(self.groupAverages, x => x.distance) * 1.05)]);
        self.groupChartAxisX.scale(self.groupChartX);
        self.groupChart.selectAll('.x.axis')
                       .transition()
                       .duration(self.animationLength)
                       .call(self.groupChartAxisX);
    }
    self.updateGroupChartAxisY = function() {
        self.groupChartY.domain([0.5, self.allGroups.length + 0.5]);
        let groupNames = Object.keys(self.reverseFullGroupNames).sort();
        self.groupChartAxisY.scale(self.groupChartY)
                            .ticks(groupNames.length)
                            .tickFormat(function (d, i) {return groupNames[i]});
        self.groupChart.selectAll('.y.axis')
                       .transition()
                       .duration(self.animationLength)
                       .call(self.groupChartAxisY);

        self.groupChart.selectAll(".y.axis .tick")
                       .on('mousedown', function(event, d) {
                           d = self.groupAverages[d - 1];
                           self.toggleGroup(d.group);
                       })
    }

    self.drawTimeChartGrayLines = function() {
        self.timeChart.selectAll('line.grayline')
                      .transition()
                      .duration(self.animationLength / 2)
                      .style('opacity', 0)
                      .remove()

        self.timeChart.selectAll('line .grayline')
                      .data(self.timeChartAxisY.scale().ticks().filter(Boolean))
                      .enter()
                      .append('line')
                      .attr('class', 'grayline')
                      .style('stroke', 'currentColor')
                      .style('stroke-width', '1')
                      .style('opacity', 0)
                      .attr('x1', 0)
                      .attr('y1', function(d) { return self.timeChartY(d) + 0.5 })
                      .attr('x2', self.timeChartSize.width)
                      .attr('y2', function(d) { return self.timeChartY(d) + 0.5 })
                      .transition()
                      .duration(self.animationLength / 2)
                      .delay(self.animationLength / 2)
                      .style('opacity', 0.15)
    }
    self.drawTimeChart = function() {
        // Y=0
        let y0 = self.timeChartY(0);

        // Remove old bars
        self.timeChart.selectAll('rect.bar')
                      .transition()
                      .duration(self.animationLength / 4)
                      .delay((data, index) => data.column * 10)
                      .ease(d3.easeQuadIn)
                      .attr('height', function(d) { return 0 })
                      .attr('transform', function(d) {
                          return 'translate(' + (self.timeChartX(d.date + self.timezoneOffset) + self.timeChartBarMargin) + ',' + y0 + ')';
                      })
                      .remove()

        self.drawTimeChartGrayLines()
        // Draw new bars
        self.timeChart.selectAll('rect .bar')
                      .data(self.timeChartBars)
                      .enter()
                      .append('rect')
                      // Initial bar position and size
                      .attr('class', function(d) { return 'bar ' + self.pageClasses[d.page]; })
                      .style('opacity', 1)
                      .attr('fill', function(d) { return self.pageColors[d.page]; })
                      .attr('transform', function(d) {
                          return 'translate(' + (self.timeChartX(d.date + self.timezoneOffset) + self.timeChartBarMargin) + ',' +  y0 + ')';
                      })
                      .attr('width', self.timeChartBarWidth)
                      .attr('height', 0)
                      // Tooltip
                      .append('svg:title').text(function(d){return d.page + ': ' + d.distance.toFixed(2) + ' km (' + d.hits + ' hits)';})
                      .select(function() { return this.parentNode; }) // Return to rect after adding title
                      // Mouse actions
                      .on('mousemove', function(event, d) {
                          d3.selectAll('.bar').style('opacity', 0.3)
                          d3.selectAll('.bar.' + self.pageClasses[d.page]).style('opacity', 1)
                      })
                      .on('mouseout', function(d) {
                          self.setSelectedPagesOpacity();
                      })
                      .on('mousedown', function(event, d) {
                          self.togglePage(d.page);
                      })
                      // Animation
                      .transition()
                      .delay((d) => self.animationLength / 4 + d.column * (self.animationLength / 20))
                      .duration(self.animationLength)
                      .ease(d3.easeBackOut.overshoot(1.75))
                      .attr('height', function(d) { return self.timeChartY(d.start) - self.timeChartY(d.end) })
                      .attr('transform', function(d) {
                          return 'translate(' + (self.timeChartX(d.date + self.timezoneOffset) + self.timeChartBarMargin) + ',' + self.timeChartY(d.end) + ')';
                      })

        if (self.selectedPages.length > 0) {
            // Make not selected groups transparent
            d3.selectAll('.bar.legend').style('opacity', 0.3);
            for (let page of self.selectedPages) {
                d3.selectAll('.bar.legend.' + self.pageClasses[page]).style('opacity', 1)
            }
        }
    }

    self.togglePage = function(page) {
        if (self.selectedPages.length == 0 || !page) {
            self.selectedPages = page ? [page] : [];
        } else {
            if (self.selectedPages.includes(page)) {
                self.selectedPages = self.selectedPages.filter(x => x != page);
            } else {
                self.selectedPages.push(page);
                if (self.selectedPages.length == self.allPages.length) {
                    self.selectedPages = [];
                }
            }
        }
        if (self.selectedPages.length > 0) {
            d3.selectAll('.bar.legend').style('opacity', 0.3)
            for (let selectedPage of self.selectedPages) {
                d3.selectAll('.bar.legend.' + self.pageClasses[selectedPage]).style('opacity', 1)
            }
        } else {
            d3.selectAll('.bar.legend').style('opacity', 1)
        }

        self.pickRecords();
        self.prepareTimeChartDictionaries();

        self.calculateGroupAverages();
        self.updateGroupChartAxisX();
        self.drawGroupChart();

        self.calculatePageAverages();
        self.updateTimeChartAxisY();
        self.drawTimeChart();

        self.updateUrlSearchParams();
        self.updateSelectedDisplay();
    }

    self.setSelectedPagesOpacity = function() {
        if (self.selectedPages.length > 0) {
            d3.selectAll('.bar.legend').style('opacity', 0.3)
            for (let page of self.selectedPages) {
                d3.selectAll('.bar.legend.' + self.pageClasses[page]).style('opacity', 1)
            }
        } else {
            d3.selectAll('.bar.legend').style('opacity', 1)
        }
        d3.selectAll('rect.bar').style('opacity', 1)
    }

    self.drawTimeChartLegend = function() {
        let values = [];
        let itemY = 0;
        let itemX = 0;
        let legend = undefined;

        d3.selectAll('#legend-box').remove()

        let longestName = d3.max(self.allPages, x => self.labelWidth(self.shortPageNames[x])) + 15;
        let columns = parseInt(self.timeChartSize.width / longestName);
        let spacing = (self.timeChartSize.width - columns * longestName) / (columns - 1);
        itemX = self.timeChartMargin.left
        itemY = 30
        for (let page of self.allPages) {
            if (values.length % columns == 0) {
                itemX = self.timeChartMargin.left
                itemY += 16
            } else {
                itemX += longestName + spacing
            }
            values.push({'page': page,
                         'cx': itemX,
                         'cy': itemY - 3.5,
                         'tx': itemX + 10,
                         'ty': itemY})

        }
        legend = self.timeChart
                     .select(function() { return this.parentNode; })
                     .append('g')
                     .attr('id', 'legend-box')
                     .selectAll('circle')
                     .data(values)
                     .enter()

        legend.append('circle')
              .attr('cx', function (d) { return d.cx})
              .attr('cy', function (d) { return d.cy})
              .attr('class', function(d) { return 'legend bar ' + self.pageClasses[d.page]; })
              .attr('r', 7)
              .style('fill', function (d) { return self.pageColors[d.page]; })
              .on('mousemove', function(event, d) {
                  d3.selectAll('.bar').style('opacity', 0.3);
                  d3.selectAll('.bar.' + self.pageClasses[d.page]).style('opacity', 1);
              })
              .on('mouseout', function(d) {
                  self.setSelectedPagesOpacity()
              })
              .on('mousedown', function(event, d) {
                  self.togglePage(d.page);
              })
              .attr('display', 'block')
        legend.append('text')
              .attr('transform', function (d) { return 'translate(' + (d.tx) + ', ' + (d.ty) + ')'})
              .text(function (d) { return self.shortPageNames[d.page]; })
              .attr('class', function(d) { return 'legend bar ' + self.pageClasses[d.page]; })
              .style('font-family', 'sans-serif')
              .style('font-size', '11px')
              .on('mousemove', function(event, d) {
                  d3.selectAll('.bar').style('opacity', 0.3);
                  d3.selectAll('.bar.' + self.pageClasses[d.page]).style('opacity', 1);
              })
              .on('mouseout', function(d) {
                  self.setSelectedPagesOpacity()
              })
              .on('mousedown', function(event, d) {
                  self.togglePage(d.page);
              })
              .attr('display', 'block')

        if (self.selectedPages.length > 0) {
            d3.selectAll('.bar.legend').style('opacity', 0.3)
            for (let page of self.selectedPages) {
                d3.selectAll('.bar.legend.' + self.pageClasses[page]).style('opacity', 1)
            }
        }

        d3.select('#timechart-box').attr('transform', 'translate(' + self.timeChartMargin.left + ',' + (self.timeChartMargin.top + itemY) + ')');
        d3.select('#time-chart-svg').attr('height', self.timeSvgSize.height + itemY);
    }
    self.labelWidth = function(text) {
        let container = d3.select('body').append('svg');
        container.append('text').attr('x', -99999).attr('y', -99999).attr('font-size', '11px').text(text).style('font-family', 'sans-serif');
        let size = container.node().getBBox();
        container.remove();
        return size.width;
    }
    self.parseRecords = function(dataset) {
        // Parse timestamps and set to beginning of an hour, parse numbers
        for (let item of dataset) {
            item.Date = new Date(parseInt(item.Timestamp) * 1000);
            item.Date.setMinutes(0);
            item.Date.setSeconds(0);
            item.Timestamp = item.Date.getTime()
            if (item.Timestamp < self.timeRange[0]) {
                continue
            }
            item.Distance = parseFloat(item.Distance);
            item.Page = item.Page;
            item.Group = item.Group;
            item.Hits = parseInt(item.Hits);
        }
        return dataset;
    };

    self.updateTimeRange = function() {
        // Hour in milliseconds
        const hour = 60 * 60 * 1000;
        // Current hour with 0 minutes and 0 seconds
        let thisHour = new Date();
        thisHour.setMinutes(0);
        thisHour.setSeconds(0);
        thisHour.setMilliseconds(0);
        // Whole time range with additional hour in the beginning and next hour as end
        let timeRange = [new Date(thisHour.getTime() - self.totalPeriod - hour + self.timezoneOffset),
                         new Date(thisHour.getTime() + hour + self.timezoneOffset)];
        self.timeRange = timeRange;
        // Update labels
        d3.select('#date-start').attr('datetime', timeRange[0]).text(self.date_format(timeRange[0]));
        d3.select('#date-end').attr('datetime', timeRange[1]).text(self.date_format(timeRange[1]));
    };

    self.updateUrlSearchParams = function() {
        let urlParams = Object.fromEntries(new URLSearchParams(window.location.search));
        // If not all pages are visible - add visible ones to url
        if (self.selectedPages.length > 0) {
            urlParams['pages'] = self.selectedPages.join(',');
        } else {
            delete urlParams['pages'];
        }
        // If not all groups are visible - add visible ones to url
        if (self.selectedGroups.length > 0) {
            urlParams['groups'] = self.selectedGroups.join(',');
        } else {
            delete urlParams['groups'];
        }
        let search = '?' + new URLSearchParams(urlParams).toString();
        // Update URL without realoading
        if (history.replaceState) {
            let newurl = window.location.protocol + '//' + window.location.host + window.location.pathname + (search == '?' ? '' : search) ;
            window.history.replaceState({path:newurl}, '', newurl);
        }
    }
    self.getTimezoneOffset = function(timezoneName) {
        // Return offset of a given timezone from local timezone
        let date = new Date()
        const tz = (new Date()).toLocaleString('en', {timeZone: timezoneName, timeStyle: 'long'}).split(' ').slice(-1)[0];
        const dateString = date.toString();
        const offset = Date.parse(`${dateString} UTC`) - Date.parse(`${dateString} ${tz}`);
        return offset + (new Date()).getTimezoneOffset() * 60 * 1000
    }
    self.updateTimeZone = function() {
        let dropdown = document.getElementById('timezone')
        let chosen = dropdown.options[dropdown.selectedIndex].value;
        let previousTimezoneOffset = self.timezoneOffset;
        if (chosen == 'local') {
            self.timezoneOffset = 0;
        } else if (chosen == 'utc') {
            self.timezoneOffset = (new Date()).getTimezoneOffset() * 60 * 1000;
        } else if (chosen == 'cern') {
            self.timezoneOffset = self.getTimezoneOffset('Europe/Zurich');
        } else if (chosen == 'fnal') {
            self.timezoneOffset = self.getTimezoneOffset('America/Chicago');
        }
        if (previousTimezoneOffset != self.timezoneOffset) {
            // If offset changed - update time range and X axis of time chart
            self.updateTimeRange();
            self.updateTimeChartAxisX();
        }
    }
    self.updateSelectedDisplay = function () {
        if (self.selectedPages.length > 0) {
            $('#time-chart-subheader').show()
            $('#time-chart-selected').html(self.selectedPages.sort().join(', '))
        } else {
            $('#time-chart-subheader').hide()
        }
        if (self.selectedGroups.length > 0) {
            $('#machine-group-subheader').show()
            $('#machine-group-selected').html(self.selectedGroups.map(x => self.fullGroupNames[x]).sort().join(', '))
        } else {
            $('#machine-group-subheader').hide()
        }
    }
};

Pages.start();
