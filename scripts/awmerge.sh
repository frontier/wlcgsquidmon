#!/bin/bash
# Merge awstats files from multiple machines into one
# Merges all files in input directories that have changed relative
#   to the output directory
# See end of script for specific machine directories that are merged
# Written by Dave Dykstra August 2012
# Turned into code template by Luis Linares 2014

AWSTATSPARSER=/home/squidmon/etc/awstats/AwstatsParser
AWSTATSCONFDIR="/home/squidmon/etc/awstats/wwwroot/cgi-bin"
AWSTATSPL=$AWSTATSCONFDIR/awstats.pl
HERE=$PWD
cd /home/squidmon/data/awstats

domerge()
{
    OUTPATH=$1
    OUTCONFIG=`basename $OUTPATH`
    shift
    date
    AWSTATSCONF=$AWSTATSCONFDIR/awstats.$OUTCONFIG.conf
    if [ ! -f $AWSTATSCONF ]
    then
	echo "Creating $AWSTATSCONF"
	sed 's,^SiteDomain=.*,SiteDomain="'$OUTCONFIG'",;s,^DirData=.*,DirData="'$PWD/$OUTPATH'",' $AWSTATSCONFDIR/awstats.`basename $1`.conf >$AWSTATSCONF
    fi
    echo "Merging awstats from $* into $OUTPATH"
    if [ ! -d $OUTPATH ]; then mkdir $OUTPATH; fi

    # Need to handle missing files; if there's no traffic, the
    #  files don't get generated

    find $* -name "awstats*.txt"|sed 's/.*awstats\([0-9]*\).*/\1/'|sort -u|while read MERGENUM; do
	OUTFILE=$OUTPATH/awstats$MERGENUM.$OUTCONFIG
	INFILES=""
	NUMINFILES=0
	for INPATH; do
	    # the -0 removes the suffix from just the filename for the
	    #  first squid when there are multiple squids
	    INFILE="$INPATH/awstats$MERGENUM.`basename $INPATH -0`.txt"
	    if [ -f $INFILE ]; then
		INFILES="$INFILES $INFILE"
		let NUMINFILES+=1
	    fi
	done
	if [ $NUMINFILES -eq 0 ]; then
	    echo "What, no input files?  Unexpected programming error, for MERGENUM $MERGENUM"
	    continue
	fi
	if [ -f $OUTFILE.txt ] && [ -z "`find $INFILES -newer $OUTFILE.txt`" ]; then
	    # outfile is up to date, silently skip
	    continue
	fi

	if [ $NUMINFILES -gt 1 ]; then
	    # use a temporary directory to avoid race conditions when
	    #  using $AWSTATSPL to update the file; it requires the file name
	    #  to be in the expected form so can't just use a different
	    #  name in the same directory
	    TMPDIR=/tmp/awmerge
	    mkdir -p $TMPDIR
	    rm -f $TMPDIR/*
	    TMPFILE=$TMPDIR/awstats$MERGENUM.$OUTCONFIG.txt
	    # the default memory limit of 128M was proven to be not quite enough
	    php -d memory_limit=512M $AWSTATSPARSER/awparse.php $INFILES > $TMPFILE
	    # Running AWSTATSPL recreates the BEGIN_MAP section
	    DATABASEBREAK=""
	    case $MERGENUM in
		????????) DATABASEBREAK="-databasebreak=day";;
	    esac
	    $AWSTATSPL -LogFile=/dev/null -config=$OUTCONFIG $DATABASEBREAK -migrate=$TMPFILE|grep -v Warning
	    mv $TMPFILE $OUTFILE.txt
	    echo "Updated $OUTFILE.txt"
	    rm -rf $TMPDIR
	else
	    echo "Copying $OUTFILE.txt from one input file"
	    cp $INFILES $OUTFILE.txt
	fi

    done
    echo

    dodnscachemerge "$*" $OUTPATH $OUTCONFIG
    dodnscachemerge "$*" $OUTPATH $OUTCONFIG ".old"
    echo

    # if nothing got generated, remove the empty directory to avoid
    # alarms complaining about awstats upload errors
    rmdir $OUTPATH 2>/dev/null || true
}

# $1 - list of awstats that should be merged
# $2 - $OUTPATH
# $3 - $OUTCONFIG
# $4 - suffix of dns cache file
dodnscachemerge() {
    suffix=${4:-}
    declare -A dnscache
    for directory in $1; do
        base=${directory#*/}
        base=${base/-0/""}
        file="${directory}/dnscachelastupdate.${base}.txt${suffix}"
        if [ ! -f "$file" ]; then
            echo "$file file does not exist"
            continue
        fi

        while IFS= read -r line
        do
            read -a fields <<< $line
            dnscache[${fields[1]}]="${fields[2]}"
        done < $file
    done

    if [ "${#dnscache[@]}" -ne 0 ]; then
        output="${2}/dnscachelastupdate.${3}.txt${suffix}"
        outputnew=${output}.new
        timestamp=${fields[0]}
        for key in "${!dnscache[@]}"; do
            printf "%-16s %-16s %-1s\n" $timestamp $key "${dnscache[$key]}" >> $outputnew
        done
        mv $outputnew $output
        echo "Updated $output"
    fi
}

#NOTE: the combined data directory goes first
DIRNAME=`dirname $0`
if [ $DIRNAME = . ]; then DIRNAME=$HERE; fi
source $DIRNAME/../etc/awmerge_lines.sh

echo "`date` Finished"
echo
