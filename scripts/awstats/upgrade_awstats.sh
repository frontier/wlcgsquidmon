#!/bin/bash
# This script upgrades awstats to the desired version.
# It creates /home/squidmon/etc/awstats_new/ directory.
# If everything looks fine, you can manually move that
# directory to /home/squidmon/etc/awstats/, then go to
# /home/squidmon/scripts/awstats-client/ directory and
# run these two scripts:
# awstats-client.py
# awstats-rsyncd.py
# in order to create new configuration files.

if [ -z "$1" ]; then
    echo "Usage: upgrade_awstats.sh version"
    echo "Example: upgrade_awstats.sh 7.7"
    exit 1
fi

# Download awstats tarball
version=$1
file="awstats-${version}.tar.gz"
url="https://sourceforge.net/projects/awstats/files/AWStats/${version}/${file}"
temporary_directory="/tmp/awstats/"
mkdir $temporary_directory
downloaded_file=${temporary_directory}${file}
wget -q -O $downloaded_file $url
trap "rm -rf $temporary_directory" 0

# Unzip awstats tarball
unziped_file="${downloaded_file/'.tar.gz'/}/"
if [ -f "$downloaded_file" ]; then
    tar -xzf $downloaded_file -C $temporary_directory
else
    echo "ERROR: can not download $file from $url"
    exit 1
fi

# Apply patches
file="frontier-awstats.spec"
gitlab="https://gitlab.cern.ch/frontier/rpms/frontier-awstats/-/raw/master/"
url="${gitlab}SPECS/${file}"
downloaded_file=${temporary_directory}${file}
wget -q -O $downloaded_file $url
if [ -f "$downloaded_file" ]; then
    patches=`grep -oP "Patch[0-9]*: \K.*" $downloaded_file`
else
    echo "ERROR: can not download $file from $url"
    exit 1
fi
for patch in $patches; do
    file=${patch##*/}
    url="${gitlab}SOURCES/${file}"
    downloaded_file=${temporary_directory}${file}
    wget -q -O $downloaded_file $url
    if [ -f "$downloaded_file" ]; then
        patch -d $unziped_file -p1 < $downloaded_file
    else
        echo "ERROR: can not download $file from $url"
        exit 1
    fi
done
home_directory="/home/squidmon/"
patch_file="${home_directory}scripts/awstats/awstats.patch"
patch -d $unziped_file -p1 < $patch_file

# Move new awstats version to awstats_new directory
awstats_new_directory="${home_directory}etc/awstats_new/"
rm -fr $awstats_new_directory
mv $unziped_file $awstats_new_directory

# Rename configuration file
configuration_file="${awstats_new_directory}wwwroot/cgi-bin/awstats.model.conf"
mv $configuration_file ${configuration_file/"model"/"template"}

# Add AwstatsParser and .last_client_update
awstats_directory=${awstats_new_directory/"_new"/""}
cp -r ${awstats_directory}AwstatsParser $awstats_new_directory
cp ${awstats_directory}.last_client_update $awstats_new_directory
