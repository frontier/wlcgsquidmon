#!/bin/bash

source $HOME/.bashrc

main()
{
    here=$( cd $(dirname $0); pwd -P )
    conf="$(echo $here | sed 's/scripts/conf/')"
    etc="$(echo $here | sed 's/scripts/etc/')"
    etc="${etc}/cms"
    config_file="${conf}/cms/config.json"

    echo
    echo "========================================================="

    #cd ${etc}
    #record_files=$( grep -o '[^"]*\.csv' ${config_file} )
    #for file in ${record_files}; do
    #    cp -v $file $file.0 >&2
    #done

    echo "Failover Monitor starting at $(date)"
    echo
    #python ${here}/cms/hourly-monitor.py ${config_file}
    python3 ${here}/failovermon.py --config ${conf}/configCMS.json >> /home/squidmon/logs/failovermonCMS.log 2>&1
    python3 ${here}/failovermon.py --config ${conf}/configATLAS.json >> /home/squidmon/logs/failovermonATLAS.log 2>&1
    python3 ${here}/failovermon.py --config ${conf}/configCvmfs.json >> /home/squidmon/logs/failovermonCvmfs.log 2>&1
    python3 ${here}/failovermon.py --config ${conf}/configCernVM.json >> /home/squidmon/logs/failovermonCernVM.log 2>&1
    echo
    echo "Failover Monitor done at $(date)"

    script="/home/squidmon/scripts/distance-monitor/distance-monitor.py"
    conf="/home/squidmon/conf/distance-monitor/"
    logs="/home/squidmon/logs/distance-monitor/"

    echo "Distance Monitor starting at $(date)"
    echo
    python3 ${script} --config ${conf}config_CMS.json >> ${logs}CMS.log 2>&1
    python3 ${script} --config ${conf}config_ATLAS.json >> ${logs}ATLAS.log 2>&1
    python3 ${script} --config ${conf}config_Cvmfs.json >> ${logs}Cvmfs.log 2>&1
    echo
    echo "Distance Monitor done at $(date)"

}

main "$@"
