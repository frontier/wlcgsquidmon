"""
Prepare information for Failover monitoring and send emails if there are direct connections to
Frontier servers
"""


from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime
from time import time
import argparse
import csv
import json
import os
import smtplib
import socket
from netaddr import IPNetwork, IPAddress
import ipaddress
from tabulate import tabulate
import humanize
import maxminddb
import pytz


# awstats_information has awstats name as key and
# {"hostname": hostname1, "site_project": site_project1, "awstats_group": awstats_group1} as value
awstats_information = {}


def get_ip(hostname, dns_cache):
    """
    Get IP address from hostname
    """
    try:
        # Check if hostname is already an IP address
        ipaddress.ip_address(hostname)
        return hostname
    except:
        # Not an IP address, so use hostname to get address

        if hostname in dns_cache:
            return dns_cache[hostname]

        try:
            ip = socket.getaddrinfo(hostname, None)
            ip = sorted(ip, key=lambda x: 1 if x[0] == socket.AF_INET6 else 0)
            ip = ip[0][4][0]
        except:
            ip = None

        print("%s is not in DNS cache. Resolved to %s" % (hostname, ip))

        return ip


def get_dns_cache(file_name):
    """
    Get DNS cache
    """
    dns_cache = {}
    file_name_old = file_name + ".old"
    dns_cache = update_dns_cache(dns_cache, file_name_old)
    dns_cache = update_dns_cache(dns_cache, file_name)

    return dns_cache


def update_dns_cache(dns_cache, file_name):
    """
    Update DNS cache
    """
    if not file_exists(file_name):
        return dns_cache

    with open(file_name, "r") as f:
        for line in f:
            fields = line.split()
            if len(fields) < 3:
                continue

            ip = fields[1]
            host = fields[2].lower()
            if host == "*":
                host = ip
            dns_cache[host] = ip

    return dns_cache


def file_exists(file_name):
    """
    Check if file or directory exists
    """
    if not os.path.exists(os.path.expanduser(file_name)):
        print("ERROR: file or directory %s does not exist" % file_name)
        return False

    return True


def get_today_stats(file_name, timezone):
    """
    Get today's stats of one servers group
    """
    today_stats = []
    if file_exists(file_name):
        print("INFO: reading previous server group records from %s " % (file_name))
        with open(file_name, "r") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter="\t")
            for row in csv_reader:
                # Ignore lines that are empty
                if not row:
                    continue

                # If comment with Day has different day of the year than today - return today
                # stats list - an empty list
                # Ignore comments in general
                if row[0].startswith("#"):
                    if row[0] == "#Day":
                        if int(row[1]) != datetime.now(pytz.timezone(timezone)).timetuple().tm_yday:
                            return today_stats
                    continue
                row[1] = int(row[1])
                row[2] = int(row[2])
                today_stats.append(row)
    else:
        directory_name = file_name[:file_name.rfind("/")]
        if not file_exists(directory_name):
            os.makedirs(directory_name, 0o755)

    return today_stats


def get_current_stats(awstats, awstats_directory, timezone):
    """
    Get stats of one servers group
    """
    current_stats = {}
    today = datetime.now(pytz.timezone(timezone)).strftime("%m%Y%d")
    for awstats_name in awstats:
        print("INFO: processing server %s" % (awstats_name))
        awstats_file = get_awstats_file(awstats_name, awstats_directory, today)
        if not file_exists(awstats_file):
            continue

        dns_cache_file = awstats_file.replace("awstats" + today, "dnscachelastupdate")
        dns_cache = get_dns_cache(dns_cache_file)

        print("INFO: reading server %s records from %s" % (awstats_name, awstats_file))
        with open(awstats_file, "r") as f:
            found = False
            for line in f:
                if not found:
                    if line.startswith("BEGIN_VISITOR"):
                        found = True
                    continue

                if line.startswith("END_VISITOR"):
                    break

                fields = line.split()
                if len(fields) < 4:
                    continue

                host = fields[0]
                if host not in current_stats:
                    ip = get_ip(host, dns_cache)
                    if not ip:
                        ip = "NoIpFound"
                    current_stats[host] = [host, 0, 0, ip]

                current_stats[host][1] += int(fields[2])
                current_stats[host][2] += int(fields[3])

    return list(current_stats.values())


def get_revised_stats(current_stats,
                      today_stats,
                      timestamp,
                      gireader,
                      squid_ip_ranges,
                      squid_ips,
                      local_squids,
                      name_translation,
                      remove_sites,
                      remove_hosts,
                      group):
    """
    Get stats between today and current run of this script
    """
    print("INFO: getting stats between today and current run of this script")
    period = 3600 # 1 hour
    revised_stats = []
    for current_stats_element in current_stats:
        host = current_stats_element[0]
        if host in remove_hosts:
            continue

        hits = current_stats_element[1]
        bandwidth = current_stats_element[2]
        ip = current_stats_element[3]
        site = get_site(gireader, ip, host, name_translation, squid_ip_ranges)
        if site in remove_sites:
            continue

        for today_stats_element in today_stats:
            if host == today_stats_element[0]:
                hits = hits - today_stats_element[1]
                bandwidth = bandwidth - today_stats_element[2]
                break

        if hits == 0:
            continue

        revised_stats.append([timestamp,
                              group,
                              site,
                              host,
                              ip,
                              is_squid(ip, squid_ips, host, local_squids),
                              bandwidth,
                              bandwidth / period,
                              hits,
                              hits / period])

    return revised_stats


def get_timestamp_stats(file_name, lower_threshold):
    """
    Get stats of experiment servers groups that are not older then threshold
    """
    stats = []
    print("INFO: reading previous monitoring records from %s " % (file_name))
    with open(file_name, "r") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter="\t")
        for index, row in enumerate(csv_reader):
            # Skip header - first line
            if index == 0:
                continue

            # Ignore lines that are empty or start with #
            if not row or row[0].startswith("#"):
                continue

            # If first column value - timestamp - is lower than threshold then ignore this line
            row[0] = int(row[0])
            if row[0] < lower_threshold:
                continue

            stats.append(row)

    return stats


def update_file(file_name, data, *args):
    """
    Update CSV file
    """
    directory_name = file_name[:file_name.rfind("/")]
    if not file_exists(directory_name):
        os.makedirs(directory_name, 0o755)
    print("INFO: updating file %s" % (file_name))
    file_name_new = file_name + ".new"
    with open(file_name_new, "w") as csv_file:
        csv_writer = csv.writer(csv_file, delimiter="\t")
        for arg in args:
            csv_writer.writerow(arg)
        for element in data:
            csv_writer.writerow(element)

    os.rename(file_name_new, file_name)


def fill_awstats_information(nodes_mapping_file):
    """
    Create dictionary where awstats name as key and
    {"hostname": hostname1, "site_project": site_project1, "awstats_group": awstats_group1} as value
    """
    print("INFO: reading nodes mapping file information from %s" % (nodes_mapping_file))
    with open(nodes_mapping_file) as f:
        for line in f:
            values = line.split()
            site_project = values[0]
            hostname = values[1]
            awstats_name = values[2]
            if len(values) > 5:
                awstats_group = values[5]
            else:
                awstats_group = ""
            awstats_information[awstats_name] = {
                "hostname": hostname,
                "site_project": site_project,
                "awstats_group": awstats_group
            }

    return len(awstats_information) > 0


def create_monitoring_config(config_data, directory, groups):
    """
    Create config for monitoring
    """
    monitoring_config = {
        "experiment": config_data["experiment"],
        "groups": {key: value.get("name", key) for key, value in groups.items()},
        "history": config_data["history"],
        "emails": {"periodicity": config_data["emails"]["periodicity"]}
    }
    file_name = directory + "config.json"
    print("INFO: creating monitoring config %s" % (file_name))
    file_name_new = file_name + ".new"
    with open(file_name_new, "w") as json_file:
        json.dump(monitoring_config, json_file)
    os.rename(file_name_new, file_name)


def create_symbolic_links(source_directory, destination_directory, files):
    """
    Create symbolic links
    """
    source_directory = source_directory
    if not file_exists(source_directory):
        print("ERROR: symbolic links can not be created from source %s" % (source_directory))
        return

    destination_directory = destination_directory
    for f in files:
        source_file = source_directory + f
        if not file_exists(source_file):
            print("ERROR: symbolic link can not be created from source %s" % (source_file))
            continue

        destination_file = destination_directory + f
        if os.path.islink(destination_file):
            if os.readlink(destination_file) != source_file:
                print("INFO: creating symbolic link %s -> %s" % (destination_file, source_file))
                destination_file_new = destination_file + ".new"
                os.symlink(source_file, destination_file_new)
                os.rename(destination_file_new, destination_file)
        else:
            print("INFO: creating symbolic link %s -> %s" % (destination_file, source_file))
            os.symlink(source_file, destination_file)


def read_from_config(config, config_name, attribute, default_value=None):
    """
    Read a value from config, return default value or raise exception if value does not exist
    """
    value = config.get(attribute)
    if not value:
        if default_value is None:
            print('ERROR: configuration file %s is missing "%s"' % (config_name, attribute))
            raise Exception('Configuration file %s is missing "%s"' % (config_name, attribute))

        return default_value

    return value


def get_records_list(sites_dictionary, revised_stats, rate_threshold):
    """
    Include stats to records list if site has hits rate over the threshold
    """
    print("INFO: getting stats where site has hits rate over the threshold")
    records_list = []
    for site, hits_rate in sites_dictionary.items():
        if hits_rate < rate_threshold:
            continue

        for element in revised_stats:
            if site == element[2]:
                records_list.append(element)

    return records_list


def get_squid_ips(grid_squids_file):
    """
    Get squid IPs from grid squids file
    """
    with open(grid_squids_file) as json_file:
        try:
            squids_data = json.load(json_file)
        except Exception as ex:
            print("ERROR: error reading %s: %s" % (grid_squids_file, ex))
            raise Exception("Error reading %s: %s" % (grid_squids_file, ex))
    squid_ips = set()
    for key, value in squids_data.items():
        for ip in value["ips"] + value.get("otherips", []):
            if ip.startswith("["):
                squid_ips.add(ip.split("]")[0][1:])
            elif ip.count(":") == 1:
                squid_ips.add(ip.split(":")[0])
            else:
                squid_ips.add(ip)

    return squid_ips


def get_shoal_squids_ips(shoal_squids_file):
    """
    Get squid IPs from shoal squids file
    """
    with open(shoal_squids_file) as json_file:
        try:
            squids_data = json.load(json_file)
        except Exception as ex:
            print("ERROR: error reading %s: %s" % (shoal_squids_file, ex))
            raise Exception("Error reading %s: %s" % (shoal_squids_file, ex))
    squid_ips = {x["public_ip"] for x in squids_data.values()}

    return squid_ips


def get_squid_ip_ranges_and_name_translation(worker_proxies_file, experiment, gireader):
    """
    Get squid IP ranges from worker proxies file
    """
    with open(worker_proxies_file) as json_file:
        try:
            squids_data = json.load(json_file)
        except Exception as ex:
            print("ERROR: error reading %s: %s" % (worker_proxies_file, ex))
            raise Exception("Error reading %s: %s" % (worker_proxies_file, ex))

    key_general_name = "names"
    key_cms_name = "cmsnames"
    key_name = key_general_name
    if experiment == "CMS":
        key_name = key_cms_name
    squid_ip_ranges = {}
    name_translation = {}
    for key, value in squids_data.items():
        if "ip" in value:
            organization = get_organization(value["ip"], gireader)
            if organization is not None:
                organization = organization.replace(" ", "")
                if organization not in name_translation:
                    name_translation[organization] = ""

                name_translation[organization] += ",".join(value.get(key_name, ""))
                if key_name == key_general_name:
                    altnames = ",".join(value.get("altnames", ""))
                    if altnames:
                        name_translation[organization] += "," + altnames
                if name_translation[organization] == "":
                    if key_name == key_cms_name:
                        name_translation[organization] += ",".join(value.get(key_general_name))

        if "proxies" not in value:
            continue

        for proxy in value["proxies"]:
            for proxy_key, proxy_value in proxy.items():
                if proxy_key == "ipranges":
                    names = get_names(key_name, proxy, value)
                    if not names:
                        if key_name == key_cms_name:
                            names = get_names(key_general_name, proxy, value)
                        if not names:
                            continue

                    squid = proxy.get("default", [])
                    if not squid:
                        squid = key
                    else:
                       squid = squid[0]
                    if squid not in squid_ip_ranges:
                        squid_ip_ranges[squid] = {"Site": names,
                                                  "IPranges": proxy_value}
                    else:
                        for ip in proxy_value:
                            squid_ip_ranges[squid]["IPranges"].append(ip)

    return squid_ip_ranges, name_translation


def get_organization(addr, gireader):
    """
    Get organization from IP address
    """
    ip = addr
    if IPAddress(addr).version == 6:
        try:
            hostname = socket.gethostbyaddr(addr)[0]
            ip = socket.gethostbyname(hostname)
        except:
            pass

    organization = None
    gir = gireader.get(ip)
    if gir is not None:
        organization = gir["organization"]

    return organization


def get_names(key, proxy, squid):
    """
    Get site names from worker proxies file
    """
    site = ""
    if key in proxy:
        site = ",".join(proxy[key])
    elif key in squid:
        site = ",".join(squid[key])

    return site


def get_sites_dictionary(data, backup):
    """"
    Create dictionary where site as key and hits rate as value
    """
    sites_dictionary = {}
    for element in data:
        if not backup and element[5]:
            continue

        site = element[2]
        sites_dictionary.setdefault(site, 0)
        sites_dictionary[site] += element[9]

    return sites_dictionary


def get_awstats_file(awstats_name, awstats_directory, today):
    """
    Find awstats file according to awstats name and date
    """
    parent_name = awstats_name
    awstats_child = awstats_information.get(awstats_name)
    if awstats_child:
        awstats_parent = awstats_child
        if '-' in awstats_name:
            possible_parent_name = awstats_name[:awstats_name.rfind("-")]
            possible_awstats_parent = awstats_information.get(possible_parent_name)
            if possible_awstats_parent:
                if awstats_name.endswith("-0"):
                    parent_name = possible_parent_name
                awstats_parent = possible_awstats_parent
    else:
        for key in awstats_information:
            if awstats_information[key]["awstats_group"] == awstats_name:
                awstats_parent = awstats_information[key]
                break
        else:
            print("ERROR: can not get awstats information of %s" % (awstats_name))
            return ""

    awstats_file = "%s%s/%s/awstats%s.%s.txt" % (awstats_directory,
                                                 awstats_parent["site_project"],
                                                 awstats_name,
                                                 today,
                                                 parent_name)

    return awstats_file


def get_site(gireader, ip, host, name_translation, squid_ip_ranges):
    """
    Get site name
    """
    site = None
    if ip != "NoIpFound":
        gir = gireader.get(ip)
        if gir and "organization" in gir:
            site = gir["organization"].replace(" ", "")

    if not site:
        if host.endswith(".cern.ch"):
            site = "EuropeanOrganizationforNuclearResearch"
        else:
            site = "Unknown"

    if ip == "NoIpFound":
        return site

    translated_site = name_translation.get(site, site)
    if site == translated_site:
        return site

    site_found = False
    site_from_ip_ranges = None
    for squid in squid_ip_ranges.values():
        if "," in translated_site or squid["Site"] == translated_site:
            for ip_range in squid["IPranges"]:
                if "0.0.0.0/" in ip_range:
                    continue

                site_found = True
                if IPAddress(ip) in IPNetwork(ip_range):
                    if site_from_ip_ranges is None:
                        site_from_ip_ranges = squid["Site"]
                    else:
                        site_from_ip_ranges += "," + squid["Site"]

    if site_from_ip_ranges is not None:
        return site_from_ip_ranges

    if site_found and "," not in translated_site:
        return site


    return translated_site


def is_squid(ip, squid_ips, host, local_squids):
    """
    Check if IP or host is in squid list
    """
    return ip in squid_ips or host in local_squids


def get_bandwidth_stats(current_stats, squid_ips, local_squids, group):
    """
    Get bandwidth stats of experiment servers groups that are at least of 1GB
    """
    print("INFO: getting bandwidth stats")
    bandwidth_stats = []
    for element in current_stats:
        bandwidth = element[2]
        if bandwidth < 1000000000: # 1GB
            continue

        host = element[0]
        ip = element[3]
        bandwidth_stats.append([is_squid(ip, squid_ips, host, local_squids),
                                bandwidth,
                                host,
                                group,
                                ip,
                                element[1]])

    return bandwidth_stats


def update_groups_dictionary(timestamp_stats, timestamp_now, groups_dictionary):
    """
    Find sites that failed in two last hours
    """
    timestamp_last_hour = timestamp_now - 4000
    for element in timestamp_stats:
        timestamp = element[0]
        group = element[1]
        site = element[2]
        group_dict = groups_dictionary[group]
        if timestamp > timestamp_last_hour and site in group_dict["sites_of_last_hour"]:
            group_dict["sites_of_last_two_hours"].add(site)


def get_site_summary_by_group(records_list,
                              groups_dictionary,
                              timestamp_now,
                              site_summary_short,
                              site_summary_long):
    """
    Get site summary by group and by site
    """
    site_summary_by_group = {}
    for element in records_list:
        group = element[1]
        site = element[2]
        if site in groups_dictionary[group]["sites_of_last_two_hours"]:
            time_diff = timestamp_now - element[0]
            hits = int(element[8])
            if time_diff < site_summary_long * 3600:
                group_dict = site_summary_by_group.setdefault(group, {})
                site_dict = group_dict.setdefault(site, {"hits1h": 0, "hits24h": 0})
                site_dict["hits24h"] += hits
                if time_diff < site_summary_short * 3600:
                    site_dict["hits1h"] += hits

    return site_summary_by_group


def get_site_summary(site_summary_by_group):
    """
    Get site summary list from dictionary
    """
    print("INFO: getting site summary stats")
    summary = []
    for group, sites_dict in site_summary_by_group.items():
        for site, hits in sites_dict.items():
            summary.append([site, group, hits["hits1h"], hits["hits24h"]])

    return sorted(summary)


def get_records_without_squids(records_list, backups):
    """
    Exclude squids from records
    """
    records_without_squids = []
    for element in records_list:
        if element[1] not in backups and str(element[5]) == "True":
            continue

        row = element[:5] + element[6:]
        records_without_squids.append(row)

    return records_without_squids


def get_sites_emails_to_send(groups_dictionary, email_records, email_lower_threshold):
    """
    Get sites that emails should be sent
    """
    sites_failed = {y for x in groups_dictionary.values() for y in x["sites_of_last_two_hours"]}
    emails_sent = {x[2] for x in email_records if x[0] > email_lower_threshold}

    return list(sites_failed - emails_sent)


def get_sites_dictionary_for_emails(sites_emails_to_send, last_hour_records):
    """
    Collect information for summary and aggregated tables that are sent in emails
    """
    sites_dictionary = {}
    for element in last_hour_records:
        site = element[2]
        if site not in sites_emails_to_send:
            continue

        group = element[1]
        squid = element[5]
        bandwidth = element[6]
        hits = element[8]
        site_dict = sites_dictionary.setdefault(site,
                                                {"summary_table": [], "aggregated_table": {}})
        site_agg = site_dict["aggregated_table"].setdefault((squid, group), [0, 0])
        site_dict["summary_table"].append([squid,
                                           group,
                                           element[3],
                                           element[4],
                                           hits,
                                           humanize.naturalsize(bandwidth, True, format="%.2f")])
        site_agg[0] += hits
        site_agg[1] += bandwidth

    return sites_dictionary


def send_emails(template_file,
                sites_dictionary,
                sites_emails_to_send,
                email_records,
                timestamp_now,
                operator_email,
                groups,
                period,
                span,
                experiment):
    """
    Send emails
    """
    if not sites_emails_to_send:
        return

    template_file = open(template_file, "r")
    template = template_file.read()
    template_file.close()

    smtp = smtplib.SMTP("localhost")

    summary_table_header = ["IsSquid", "GCode", "Host", "Ip", "Hits", "Bandwidth"]
    aggregated_table_header = summary_table_header[:2] + summary_table_header[4:]
    for site in sites_emails_to_send:
        groups_set = set()
        aggregated_table = []
        for key, value in sites_dictionary[site]["aggregated_table"].items():
            group = key[1]
            aggregated_table.append([key[0],
                                     group,
                                     value[0],
                                     humanize.naturalsize(value[1], True, format="%.2f")])
            groups_set.add(group)

        group_table = []
        for group in groups_set:
            group_value = groups[group]
            group_table.append([group_value["name"], group, group_value["rate_threshold"]])

        template_values = {"now": datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"),
                           "server_groups": tabulate(group_table,
                                                     headers=["Group",
                                                              "GCode",
                                                              "RateThreshold [*]"],
                                                     tablefmt="plain"),
                           "support_email": operator_email,
                           "period": period,
                           "record_span": span,
                           "experiment": experiment,
                           "site_name": site,
                           "aggregated_table": tabulate(aggregated_table,
                                                        headers=aggregated_table_header,
                                                        tablefmt="plain"),
                           "summary_table": tabulate(sites_dictionary[site]["summary_table"],
                                                     headers=summary_table_header,
                                                     tablefmt="plain")}
        text = template.format(**template_values)

        message = MIMEMultipart()
        message["To"] = operator_email
        message["Subject"] = "Direct Connections to Frontier servers from " + site
        message.add_header("Reply-to", operator_email)
        message.attach(MIMEText(text, "plain"))
        smtp.sendmail("squidmon@mail.cern.ch", [operator_email], message.as_string())

        email_records.append([timestamp_now, operator_email, site])
        print("INFO: email was sent to %s site" % (site))
    smtp.quit()


def is_backup(name):
    """
    Check if group is real server (frontier server or cvmfs stratum 1) or backup proxies
    """

    return "backup" in name.lower()


def update_groups(groups, stratum1_config):
    """
    Update groups by merging dictionaries
    """
    with open(os.path.expanduser(stratum1_config)) as json_file:
        data = json.load(json_file)

    for name, group in groups.items():
        if name not in data:
            data[name] = group
        else:
            data[name].update(group)

    return data


def main():
    """
    Prepare information for Frontier failover monitoring
    """
    print("INFO: started at " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", required=True, help="Get information from configuration file")
    parser.add_argument("--test",
                        action="store_true",
                        help="Add output files to current directory and do not send emails")
    args = parser.parse_args()
    test = args.test
    config_file = args.config

    if not file_exists(config_file):
        return

    with open(config_file) as json_file:
        try:
            config_data = json.load(json_file)
        except Exception as ex:
            print("ERROR: error reading %s: %s" % (config_file, ex))
            return

    if test:
        etc_directory = "etc/"
        www_directory = "www/"
        emailing = False
    else:
        etc_directory = os.path.expanduser(read_from_config(config_data,
                                                            config_file,
                                                            "etc_directory"))
        www_directory = os.path.expanduser(read_from_config(config_data,
                                                            config_file,
                                                            "www_directory"))
        emailing = True

    wwwsrc_directory = os.path.expanduser(read_from_config(config_data,
                                                           config_file,
                                                           "wwwsrc_directory"))
    awstats_directory = os.path.expanduser(read_from_config(config_data,
                                                            config_file,
                                                            "awstats_directory"))
    nodes_mapping_file = os.path.expanduser(read_from_config(config_data,
                                                             config_file,
                                                             "nodes_mapping_file"))
    shoal_squids_file = os.path.expanduser(read_from_config(config_data,
                                                            config_file,
                                                            "shoal_squids"))
    worker_proxies_file = os.path.expanduser(read_from_config(config_data,
                                                              config_file,
                                                              "worker_proxies"))
    grid_squids_file = os.path.expanduser(read_from_config(config_data, config_file, "squid_list"))
    geo_db = os.path.expanduser(read_from_config(config_data, config_file, "geo_db"))
    symbolic_links = read_from_config(config_data, config_file, "symbolic_links")
    experiment = read_from_config(config_data, config_file, "experiment")
    groups = read_from_config(config_data, config_file, "groups")
    history = read_from_config(config_data, config_file, "history")
    span = history.get("span", 72)
    period = history.get("period", 60)
    local_squids = read_from_config(config_data, config_file, "local_squids", [])
    exceptions = read_from_config(config_data, config_file, "exceptions")
    remove_sites = exceptions.get("remove_sites", [])
    remove_hosts = exceptions.get("remove_hosts", [])
    site_summary_long = read_from_config(config_data, config_file, "site_summary_long")
    site_summary_short = read_from_config(config_data, config_file, "site_summary_short")
    emails = read_from_config(config_data, config_file, "emails")
    email_periodicity = emails.get("periodicity", 24)
    operator_email = emails.get("operator_email")
    template_file = os.path.expanduser(emails.get("template_file", ""))
    stratum1_config = read_from_config(config_data, config_file, "stratum1_config", "")

    if not operator_email:
        print(('ERROR: configuration file %s is missing "operator_email" in emails. '
               'Emails can not be sent.') % (config_file))
        emailing = False

    if not file_exists(template_file):
        print("ERROR: emails can not be sent.")
        emailing = False

    if not file_exists(nodes_mapping_file):
        return

    if not fill_awstats_information(nodes_mapping_file):
        print("ERROR: nodes mapping file %s is empty" % (nodes_mapping_file))
        return

    if not file_exists(grid_squids_file):
        return

    if not file_exists(worker_proxies_file):
        return

    squid_ips = get_squid_ips(grid_squids_file)

    if file_exists(shoal_squids_file):
        squid_ips.update(get_shoal_squids_ips(shoal_squids_file))

    try:
        gireader = maxminddb.open_database(geo_db)
    except Exception as ex:
        print("ERROR: can not open database %s: %s" % (geo_db, ex))
        return

    squid_ip_ranges, name_translation = get_squid_ip_ranges_and_name_translation(worker_proxies_file,
                                                                                 experiment,
                                                                                 gireader)

    failover_etc_directory = etc_directory + "failover" + experiment + "/"
    failover_www_directory = www_directory + "failover" + experiment + "/"
    email_file = failover_www_directory + "email-record.tsv"
    site_summary_file = failover_www_directory + "site_summary.txt"
    records_file = failover_www_directory + "failover-record.tsv"
    records_no_squids_file = failover_www_directory + "failover-record-nosquid.tsv"
    bandwidth_stats_1GB_file = failover_www_directory + "bandwidth-record-1GB.tsv"
    bandwidth_stats_10GB_file = failover_www_directory + "bandwidth-record-10GB.tsv"

    if stratum1_config and file_exists(stratum1_config):
        groups = update_groups(groups, stratum1_config)

    timestamp_now = int(time())
    records_list = []
    bandwidth_stats = []
    backups = []
    groups_dictionary = {}
    records_headers = ["Timestamp",
                       "Group",
                       "Sites",
                       "Host",
                       "Ip",
                       "IsSquid",
                       "Bandwidth",
                       "BandwidthRate",
                       "Hits",
                       "HitsRate"]
    for group_key, group_value in groups.items():
        print("INFO: processing group %s" % (group_key))
        awstats = group_value.get("awstats")
        if not awstats:
            print('ERROR: configuration file %s is missing "awstats" in group %s' %
                  (config_file, group_key))
            continue

        timezone = group_value.get("timezone")
        if not timezone:
            print('ERROR: configuration file %s is missing "timezone" in group %s' %
                  (config_file, group_key))
            continue

        name = group_value.get("name")
        if not name:
            print('ERROR: configuration file %s is missing "name" in group %s' %
                  (config_file, group_key))
            continue

        rate_threshold = group_value.setdefault("rate_threshold", 5.56)

        backup = is_backup(name)
        if backup:
            backups.append(group_key)

        file_today_stats = failover_etc_directory + "today_stats_" + group_key + ".tsv"
        today_stats = get_today_stats(file_today_stats, timezone)
        current_stats = get_current_stats(awstats, awstats_directory, timezone)
        revised_stats = get_revised_stats(current_stats,
                                          today_stats,
                                          timestamp_now,
                                          gireader,
                                          squid_ip_ranges,
                                          squid_ips,
                                          local_squids,
                                          name_translation,
                                          remove_sites,
                                          remove_hosts,
                                          group_key)
        sites_dictionary = get_sites_dictionary(revised_stats, backup)
        group_records_list = get_records_list(sites_dictionary, revised_stats, rate_threshold)
        records_list += group_records_list
        groups_dictionary[group_key] = {"sites_of_last_hour": {x[2] for x in group_records_list},
                                        "sites_of_last_two_hours": set()}
        bandwidth_stats += get_bandwidth_stats(current_stats, squid_ips, local_squids, group_key)
        update_file(file_today_stats,
                    current_stats,
                    ["#Day", datetime.now(pytz.timezone(timezone)).timetuple().tm_yday],
                    ["#Host", "Hits", "Bandwidth", "Ip"])

    last_hour_records = list(records_list)
    timestamp_stats = []
    if file_exists(records_file):
        lower_threshold = timestamp_now - span * 3600
        timestamp_stats = get_timestamp_stats(records_file, lower_threshold)
        records_list += timestamp_stats

    update_file(records_file, records_list, records_headers)

    records_without_squids = get_records_without_squids(records_list, backups)
    update_file(records_no_squids_file,
                records_without_squids,
                records_headers[:5] + records_headers[6:])

    update_groups_dictionary(timestamp_stats, timestamp_now, groups_dictionary)
    site_summary_by_group = get_site_summary_by_group(records_list,
                                                      groups_dictionary,
                                                      timestamp_now,
                                                      site_summary_short,
                                                      site_summary_long)
    site_summary = get_site_summary(site_summary_by_group)
    update_file(site_summary_file, site_summary, ["Sites", "Group", "Hits1h", "Hits24h"])

    bandwidth_stats = sorted(bandwidth_stats, key=lambda x: (x[0], -x[1]))
    bandwidth_stats_10GB = [x for x in bandwidth_stats if x[1] > 10000000000]
    for element in bandwidth_stats:
        element[1] = "{0:.2e}".format(element[1])
    bandwidth_headers = ["IsSquid", "Bandwidth", "Host", "Group", "Ip", "Hits"]
    update_file(bandwidth_stats_1GB_file, bandwidth_stats, bandwidth_headers)
    update_file(bandwidth_stats_10GB_file, bandwidth_stats_10GB, bandwidth_headers)

    if emailing:
        if file_exists(email_file):
            email_records = get_timestamp_stats(email_file, lower_threshold)
        else:
            email_records = []
        email_lower_threshold = timestamp_now - email_periodicity * 3600
        sites_emails_to_send = get_sites_emails_to_send(groups_dictionary,
                                                        email_records,
                                                        email_lower_threshold)
        sites_dictionary_for_emails = get_sites_dictionary_for_emails(sites_emails_to_send,
                                                                      last_hour_records)
        send_emails(template_file,
                    sites_dictionary_for_emails,
                    sites_emails_to_send,
                    email_records,
                    timestamp_now,
                    operator_email,
                    groups,
                    period,
                    span,
                    experiment)
        update_file(email_file, email_records, ["Timestamp", "Addresses", "Sites"])

    create_monitoring_config(config_data, failover_www_directory, groups)
    create_symbolic_links(wwwsrc_directory, failover_www_directory, symbolic_links)
    print("INFO: finished at " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


if __name__ == "__main__":
    main()
