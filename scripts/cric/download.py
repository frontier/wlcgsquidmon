#! /usr/bin/python3
import os
import json
import requests
import datetime

print("CRIC download script started: "+str(datetime.datetime.now()))

cricdata = {}
cricdata['cric_atlas_site_info'] = 'https://atlas-cric.cern.ch/api/atlas/site/query/?json'
cricdata['cric_atlas_downtimes'] = 'https://atlas-cric.cern.ch/api/core/downtime/query/?json&preset=sites'
cricdata['cric_atlas_queuestatus'] = 'https://atlas-cric.cern.ch/api/atlas/pandaqueuestatus/query/?json'
cricdata['cric_atlas_endpointstatus'] = 'https://atlas-cric.cern.ch/api/atlas/ddmendpointstatus/query/?json'

for l in cricdata.keys():
  url = cricdata[l]
  print(str(datetime.datetime.now()) + ' downloading from ' + url)
  try:
    r = requests.get(url, timeout=30, cert=('/home/squidmon/certs/wlcg-squid-monitor-cert.pem', '/home/squidmon/certs/wlcg-squid-monitor-key.pem'), verify='/etc/ssl/certs/CERN-bundle.pem')
  except requests.exceptions.ReadTimeout:
    print("ERROR: socket timed out - URL %s" % url)
    exit(1)
  j = r.json()
  with open('/home/squidmon/www/'+l+'.json', 'w') as cf:
    json.dump(j, cf)
  print(str(datetime.datetime.now())+' CRIC response stored in /home/squidmon/www/'+l+'.json')

print('CRIC download script finished: '+str(datetime.datetime.now()))

if os.path.exists('/home/squidmon/'):
  #extra output file for crash monitoring tool
  etc_grid_service_dir = "/home/squidmon/etc/grid-services/"
  if not os.path.exists(etc_grid_service_dir):
    os.makedirs(etc_grid_service_dir)
  f = open(etc_grid_service_dir + "download.out", "w")
  f.write("scripts/cric/download.py execution finished successfully!")
  f.close()
