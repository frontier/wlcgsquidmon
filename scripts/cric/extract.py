#! /usr/bin/python3
import os
import json
import pprint
import datetime

print('CRIC information extraction script started: '+str(datetime.datetime.now()))

if os.path.exists('/home/squidmon/'):
  cric_in = '/home/squidmon/www/cric_atlas_site_info.json'
  cric_out = '/home/squidmon/www/cric.json'
else:
  cric_in = 'cric_site_info.json'
  cric_out = 'cric.json'

with open(cric_in) as cf:
    cjson = json.load(cf)
#pprint.pprint(cjson)

sites = []
for s in cjson.keys():
  tempsites = {}
  tempsites['name'] = cjson[s]['name']
  tempsites['rc_site'] = cjson[s]['rc_site']
  tempsites['tier_level'] = cjson[s]['tier_level']
  tempsites['state'] = cjson[s]['state']
  tempsites['cloud'] = cjson[s]['cloud']
  tempsites['fsconf'] = cjson[s]['fsconf']
  sites.append(tempsites)
#pprint.pprint(sites)

with open(cric_out, 'w') as outfile:
  json.dump(sites, outfile)

print(str(datetime.datetime.now())+' extracted info stored in '+cric_out)
print('CRIC information extraction script finished: '+str(datetime.datetime.now()))

if os.path.exists('/home/squidmon/'):
  #extra output file for crash monitoring tool
  etc_grid_service_dir = "/home/squidmon/etc/grid-services/"
  if not os.path.exists(etc_grid_service_dir):
    os.makedirs(etc_grid_service_dir)
  f = open(etc_grid_service_dir + "extract.out", "w")
  f.write("scripts/cric/extract.py execution finished successfully!")
  f.close()
