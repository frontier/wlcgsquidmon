#!/bin/bash

date
echo 'running snmp probes'
cd $(dirname $0);
# Run MRTG probes
#./cms.job
#./cvmfs.job
# the rest copy from the "all" snmp collector, so give plenty of
#  time for them to finish
sleep 1m
date
echo 'running copy probes'
#python3 /home/squidmon/scripts/all/mrtg_plots.py --sub cmstesting
python3 /home/squidmon/scripts/all/mrtg_plots.py --sub cms
python3 /home/squidmon/scripts/all/mrtg_plots.py --sub cmssummary
#python3 /home/squidmon/scripts/all/mrtg_plots.py --sub cvmfstesting
python3 /home/squidmon/scripts/all/mrtg_plots.py --sub cvmfs
python3 /home/squidmon/scripts/all/mrtg_plots.py --sub juno
python3 /home/squidmon/scripts/mrtg/send2ES.py

# The ATLAS job can take a long time, so run it in the background.
#./atlas.job &

