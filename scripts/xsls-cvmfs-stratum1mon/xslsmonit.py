#! /usr/bin/python3
import os
import csv
import json
# import subprocess
from datetime import datetime, timezone
import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time
import pprint


def standard_degradation_email(stratum, s1data, newstatus, sfile, crittim):
  ##########################################
  # how to avoid publication of fluctuation - I have:
  # -newstatus - current state
  # -laststatus - previous state
  # -pubstatus - the most recent status announced by email before laststatus happened, i.e. it represents the trend before the laststatus
  # degradation is not fluctuation if the current and previous are the same but the trend stratumrted by pubstatus is different
  ##########################
  # statuses combinations and appropriate script actions:
  # pubstatus-laststatus-status
  # ---------------------------
  # available-available-available
  # -nothing happens
  # -written into the status file: pubstatus=available,laststatus=available
  # available-available-degraded
  # -starting, i.e. status is degraded but everything else is available - only the printed message in else statement
  # -written into the status file: pubstatus=available,laststatus=degraded
  # available-degraded-available - degradation fluctuation
  # -nothing happens
  # -written into the status file: pubstatus=available,laststatus=available
  # available-degraded-degraded - degradation continues
  # -email is triggered
  # -written into the status file: pubstatus=degraded,laststatus=degraded
  # degraded-degraded-degraded  degradation continues
  # -nothing happens
  # -written into the status file: pubstatus=degraded,laststatus=degraded
  # degraded-degraded-available - degradation is fixed
  # -only the printed message in else statement
  # -written into the status file: pubstatus=degraded,laststatus=available
  # degraded-available-degraded - degradation returns
  # -nothing happens
  # -written into the status file: pubstatus=degraded,laststatus=degraded
  # degraded-available-available - degradation fixed
  # -email is triggered
  # -written into the status file: pubstatus=available,laststatus=available
  ##########################
  # long degradation VS fluctuation
  # DDDAA - email is sent during DD phase, i.e. pubstatus is D
  # AADAA - email is not sent, i.e. pubstatus is A
  emailtext = ''
  email = s1data[stratum]['emails']
  if NOMAIL:
    email = ''
  pubstatus = s1data[stratum]['pubstatus']
  laststatus = s1data[stratum]['laststatus']
  print('pubstatus: ' + pubstatus + ', laststatus: ' + laststatus)
  if pubstatus != newstatus:  # i.e. do nothing for stable situation (available-available-available, degraded-degraded-degraded) or  fluctuation (available-degraded-available, degraded-available-degraded)
    if pubstatus == "":  # in case of empty status - this should really happen only when the script runs for a first time - setting published status to current status
      pubstatus = newstatus
      print("Published status is empty\n")
    elif laststatus != newstatus:  # starting degradation or just fluctuation (available-available-degraded, degraded-degraded-available)
      print("Status changed from " + pubstatus + " to " + newstatus + " but delaying notice since last check it was " + laststatus + "\n")
    else:  # i.e. getting to (available-degraded-degraded) or from (degraded-available-available) degradation
      if email != '':
        print("Sending notice to " + email + " of change from " + pubstatus + " to " + newstatus + "\n")
        emailtext += str(monitinfo) + "\n"
        emailtext += "\n"
        emailtext += 'https://monit-grafana.cern.ch/d/plTtqczZz/sls-details?orgId=17&var-services=cvmfs_stratum1mon_' + s + '\n'
        emailtext += URL + '\n'
        # try:
        #   process = subprocess.Popen(['mail', '-s', stratum+' status changed from '+pubstatus+' to '+newstatus, email],stdin=subprocess.PIPE)
        #   process.communicate(emailtext.encode('utf-8'))
        # except Exception as error:
        #   print('Email sending was affected by the following error: '+str(error))
        sender = 'squidmon@mail.cern.ch'  # From
        receivers = email  # To
        # copies = ''  # CC
        msg = MIMEMultipart('alternative')  # Using “alternative” indicates that the email message contains multiple representations of the same content, but in different formats. For example, you might have the same email message in both plain text and HTML.
        msg['Subject'] = stratum + ' status changed from ' + pubstatus + ' to ' + newstatus
        msg['To'] = receivers
        msg.attach(MIMEText(emailtext, 'plain'))
        try:
          smtpObj = smtplib.SMTP('localhost')
          # smtpObj.set_debuglevel(True)
          # smtpObj.sendmail(sender, [receivers] + [copies], msg.as_string())
          smtpObj.sendmail(sender, [receivers], msg.as_string())
          smtpObj.quit()
          print("\nSuccessfully sent email to: " + msg['To'])
          # print(" CC'ed to: "+msg['CC'])
          time.sleep(1)
        except:
          print("\nFailed to send email to: " + msg['To'])

      pubstatus = newstatus  # email is sent, so setting the pubstatus to current status
  if not os.path.exists('/home/squidmon/'):
    print(emailtext)
  writer = csv.writer(sfile)
  # current status become laststatus and last status become pubstatus
  writer.writerow(['PUB' + s + '=' + pubstatus])
  writer.writerow(['LAST' + s + '=' + newstatus])
  writer.writerow(['CRITTIME' + s + '=' + str(crittim)])


def persistent_degradation_email(stratum, s1data, newstatus, ctime):
  sendingInterval = 24 * 3600
  emailtext = ''
  email = s1data[stratum]['emails']
  if NOMAIL:
    email = ''
  reportStates = ['CRITICAL', 'NORESPONSE']
  if newstatus in reportStates:  # if the most recent state is one of those we want to report, fill the crit time - otherwise, reset it
    if len(ctime) == 0:  # the crit time is str (empty str if undefined) - first degradation fills it
      ct = int(datetime.now(tz=timezone.utc).timestamp())
    else:  # if crit time is defined
      ct = ctime
      if int(datetime.now(tz=timezone.utc).timestamp()) - int(ctime) > sendingInterval:
        if email != '':
          ct = int(datetime.now(tz=timezone.utc).timestamp())  # send email every 24 h, i.e. if the email is sent, reset the crit time to current time
          print("Sending notice to " + email + " about persistent " + newstatus + " status of " + stratum + "\n")
          emailtext += str(monitinfo) + "\n"
          emailtext += "\n"
          emailtext += 'https://monit-grafana.cern.ch/d/plTtqczZz/sls-details?orgId=17&var-services=cvmfs_stratum1mon_' + s + '\n'
          emailtext += URL + '\n'
          # try:
          #   process = subprocess.Popen(['mail', '-s', stratum+' status is still '+newstatus, email],stdin=subprocess.PIPE)
          #   process.communicate(emailtext.encode('utf-8'))
          # except Exception as error:
          #   print('Email sending was affected by the following error: '+str(error))
          sender = 'squidmon@mail.cern.ch'  # From
          receivers = email  # To
          # copies = ''  # CC
          msg = MIMEMultipart('alternative')  # Using “alternative” indicates that the email message contains multiple representations of the same content, but in different formats. For example, you might have the same email message in both plain text and HTML.
          msg['Subject'] = stratum + ' status is still ' + newstatus
          msg['To'] = receivers
          msg.attach(MIMEText(emailtext, 'plain'))
          try:
            smtpObj = smtplib.SMTP('localhost')
            # smtpObj.set_debuglevel(True)
            # smtpObj.sendmail(sender, [receivers] + [copies], msg.as_string())
            smtpObj.sendmail(sender, [receivers], msg.as_string())
            smtpObj.quit()
            print("\nSuccessfully sent email to: " + msg['To'])
            # print(" CC'ed to: "+msg['CC'])
            time.sleep(1)
          except:
            print("\nFailed to send email to: " + msg['To'])
  else:  # if the state is not amongst those which we want to report, reset the crit time
    ct = ''
  if not os.path.exists('/home/squidmon/'):
    print(emailtext)
  return ct


def monit(serviceid, servicestatus, info, desc, emails):
  report = {}
  report['producer'] = 'cvmfs_stratum1'
  report['type'] = 'availability'
  report['serviceid'] = serviceid.lower()
  report['service_status'] = servicestatus
  report['availabilityinfo'] = info
  report['availabilitydesc'] = desc
  report['contact'] = emails
  report['webpage'] = 'https://github.com/cvmfs-contrib/cvmfs-servermon/blob/master/README.md'
  return report


print("xslsmonit script started: " + str(datetime.now(tz=timezone.utc)) + ' UTC')

NOMAIL = False
if not os.path.exists('/home/squidmon/'):
  CONFDIR = os.getcwd() + "/conf"
  CONF = CONFDIR + '/xsls-cvmfs-stratum1mon-test.conf'
  JSONURL = ""
  STATDIR = os.getcwd() + "/out/test"
else:
  CONFDIR = "/home/squidmon/conf"
  CONF = CONFDIR + '/xsls-cvmfs-stratum1mon.conf'
  JSONURL = "http://monit-metrics:10012/"
  STATDIR = "/home/squidmon/etc/xsls"

STAT = STATDIR + '/cvmfs-stratum1mon.status2'

s1data = {}
with open(CONF, 'r') as s1conf:
  reader = csv.reader(s1conf, delimiter=' ')
  for row in reader:
    if row[0][0] != '#':
      site = row[0]
      s1data[site] = {}
      s1data[site]['pubstatus'] = ''
      s1data[site]['laststatus'] = ''
      s1data[site]['crittime'] = ''
      s1data[site]['host'] = row[1]
      if len(row) >= 3:
        cloudalias = row[2]
      else:
        cloudalias = '-'
      s1data[site]['cloudalias'] = cloudalias
      if len(row) >= 4:
        servicename = row[3]
      else:
        servicename = 'local'
      s1data[site]['servicename'] = servicename
      if len(row) >= 5:
        emails = row[4]
      else:
        emails = 'cvmfs-stratum-alarm@cern.ch'
      s1data[site]['emails'] = emails

if os.path.exists(STAT):  # if the file exists
  if os.stat(STAT).st_size != 0:  # if the file is not empty, read it
    with open(STAT, 'r') as statusfile:
      statusreader = csv.reader(statusfile, delimiter='=')
      for line in statusreader:
        if 'PUB' in line[0]:
          if line[0].replace('PUB', '').replace('_', '-') in list(s1data):
            s1data[line[0].replace('PUB', '').replace('_', '-')]['pubstatus'] = line[1]
        if 'LAST' in line[0]:
          if line[0].replace('LAST', '').replace('_', '-') in list(s1data):
            s1data[line[0].replace('LAST', '').replace('_', '-')]['laststatus'] = line[1]
        if 'CRITTIME' in line[0]:
          if line[0].replace('CRITTIME', '').replace('_', '-') in list(s1data):
            s1data[line[0].replace('CRITTIME', '').replace('_', '-')]['crittime'] = line[1]
  else:  # if the file exists but is empty
    print('Warning: stat file exists but is empty - using empty statuses')
    for a in s1data.keys():
      s1data[a]['pubstatus'] = ''
      s1data[a]['laststatus'] = ''
      s1data[a]['crittime'] = ''
else:  # if the stat file does not exist
  print('Warning: stat file does not exist - using empty statuses')
  for a in s1data.keys():
    s1data[a]['pubstatus'] = ''
    s1data[a]['laststatus'] = ''
    s1data[a]['crittime'] = ''
# pprint.pprint(s1data)

with open(STAT, 'w') as sfile:
  for s in sorted(s1data.keys()):
    print('Checking ' + s + ' at ' + datetime.now(tz=timezone.utc).strftime("%Y-%m-%dT%H:%M:%S"))
    if s1data[s]['servicename'] != 'local' or s1data[s]['servicename'] != '-':
      URL = 'http://' + s1data[s]['host'] + '/cvmfsmon/api/v1.0/all&format=details&server=' + s1data[s]['servicename']
    else:
      URL = 'http://' + s1data[s]['host'] + '/cvmfsmon/api/v1.0/all&format=details'
    status = 'available'
    monitinfo = ''
    monitdesc = 'Status according to ' + URL
    try:
      r = requests.get(URL, timeout=60)
      response = json.loads(r.content)
      monitinfo = 'OK'
      emailstatus = 'OK'
      if 'CRITICAL' in response.keys():
        status = 'unavailable'
        emailstatus = 'CRITICAL'
        monitinfo = '"' + emailstatus + '": ' + json.dumps(response[emailstatus])
      if 'WARNING' in response.keys() and 'CRITICAL' not in response.keys():
        status = 'degraded'
        emailstatus = 'WARNING'
        monitinfo = '"' + emailstatus + '": ' + json.dumps(response[emailstatus])
    except (requests.exceptions.RequestException, requests.exceptions.ConnectionError, ValueError) as e:
      print("Error: " + str(e))
      status = 'unavailable'
      monitinfo = 'NORESPONSE'
      emailstatus = 'NORESPONSE'
    if s1data[s]['cloudalias'] != '-':
      CloudURL = 'http://' + s1data[s]['cloudalias'] + '/cvmfs/info/v1/meta.json'
      monitdesc += ' and ' + CloudURL
      try:
        r = requests.get(CloudURL, timeout=30)
      except:
        if monitinfo == 'OK':
          status = 'degraded'
          emailstatus = 'WARNING'
          monitinfo = '"' + emailstatus + '": '
        else:
          monitinfo += ', '
        monitinfo += 'Failed to read ' + CloudURL
    monitreport = monit('cvmfs_stratum1mon_' + s, status, monitinfo, monitdesc, s1data[s]['emails'])
    if JSONURL != '':
      response = requests.post(JSONURL, data=json.dumps(monitreport), headers={"Content-Type": "application/json; charset=UTF-8"})
      print('uploading ' + status + ' for cvmfs_stratum1mon_' + s)
      if response.status_code != 200:
        print('upload for cvmfs_stratum1mon_' + s + ' failed with: ' + response.text)
    else:
      pprint.pprint(monitreport)
    ctime = ''
    ctime = persistent_degradation_email(s, s1data, emailstatus, s1data[s]['crittime'])  # as the standard_degradation_email function updates the statuses, the check for permanent needs to run first
    standard_degradation_email(s, s1data, emailstatus, sfile, ctime)

print("xslsmonit script ended: " + str(datetime.now(tz=timezone.utc)) + ' UTC')
