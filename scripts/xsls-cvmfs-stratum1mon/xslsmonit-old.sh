#!/bin/bash
# Read CVMFS stratum 1's cvmfsmon status, upload it to xsls.cern.ch,
#  and send email about changed status
# Written by Dave Dykstra November 2016

if [ $# = 1 ] && [ "$1" = "-t" ]; then
    shift
    CONFDIR=../../conf
    XMLDIR=.
    STATDIR=.
    # NOTE: does not override emails in $CONFDIR/xsls-cvmfs-stratum1mon.conf
    EMAIL=$LOGNAME@cern.ch
else
    CONFDIR=/home/squidmon/conf
    XMLDIR=/home/squidmon/www/xsls-cvmfs-stratum1mon
    STATDIR=/home/squidmon/etc/xsls
    EMAIL=cvmfs-stratum-alarm@cern.ch
fi

if [ $# != 0 ]; then
    echo "Usage: $0 [-t]" >&2
    exit 1
fi

CONF=$CONFDIR/xsls-cvmfs-stratum1mon.conf
STAT=$STATDIR/cvmfs-stratum1mon.status

if [ ! -f $CONF ]; then
    echo "$CONF not found" >&2
    exit 2
fi

mkdir -p $XMLDIR $STATDIR

if [ -f $STAT ]; then
    eval `cat $STAT`
fi

MAILFILE=/tmp/xsls-cvmfs-stratum1$$
trap "rm -f $MAILFILE" 0

STATUSES=""
while read SITE HOST SERVICE EMAILS; do
    case $SITE in
	\#*) continue;;
    esac
    NOW=`date +%Y-%m-%dT%H:%M:%S`
    echo "Checking $SITE at $NOW"
    XML=$XMLDIR/$SITE.xml
    URL="http://$HOST/cvmfsmon/api/v1.0/all&format=details"
    TIMEOUT=10
    if [ -n "$SERVICE" ] && [ "$SERVICE" != "-" ]; then
	URL="$URL&server=$SERVICE"
	TIMEOUT=30
    fi
    DETAILS="`wget --timeout=$TIMEOUT --tries=1 -qO- "$URL"`"
    if [ -z "$DETAILS" ]; then
	# try once more no matter why it failed
	sleep 1
	NOW=`date +%Y-%m-%dT%H:%M:%S`
	echo "no response, trying $SITE again at $NOW"
	DETAILS="`wget --timeout=$TIMEOUT --tries=1 -qO- "$URL"`"
    fi
    if [ -z "$DETAILS" ]; then
	STATUS=NORESPONSE
	NOTOKDETAILS=""
    else
	# this extracts the lines that are not marked "OK"
	NOTOKDETAILS="`echo "$DETAILS"|sed 's/^{//;s/}$//'| \
	    awk '/"OK"/{notok=0}
		/"WARNING"/{notok=1}
		/"CRITICAL"/{notok=1}
		{if (notok) print}'`"
	# Extract the overall status.  Check for the first quoted item on
	#  the first line of NOTOKDETAILS first because those are higher
	#  priority than OK.  If that shows nothing, look at OKDETAILS.
	STATUS="`echo "$NOTOKDETAILS"|sed 's/[^"]*"//;s/".*//;q'`"
	if [ -z "$STATUS" ]; then
	    STATUS="`echo "$DETAILS"|sed 's/[^"]*"//;s/".*//;q'`"
	fi
	if [ -z "$STATUS" ]; then
	    # this should never happen, but just in case
	    STATUS=NONE
	fi
    fi
    STATNAME="${SITE//-/_}"
    XSLSNAME=cvmfs_stratum1mon_$SITE
    eval "PUBSTATUS=\$PUB$STATNAME"
    eval "LASTSTATUS=\$LAST$STATNAME"
    if [ -n "$LASTSTATUS" ] && [ "$PUBSTATUS" != "$STATUS" ]; then
	SHOWDETAILS=true
	if [ "$STATUS" != NORESPONSE ] && [ "$LASTSTATUS" != NORESPONSE ]; then
	    # If a transition between non-responses happen, we delay until
	    #  we see a change twice in a row.  Otherwise make switches
	    #  immediately.  If we delay it becomes a problem because
	    #  if you click on a degraded range in grafana it only shows
	    #  the details for the last upload, and we don't save the
	    #  details so we can't upload them the last time if we delay.
	    LASTSTATUS=$STATUS
	fi
	if [ -z "$PUBSTATUS" ]; then
	    # first time
	    PUBSTATUS=$STATUS
	    SHOWDETAILS=false
	elif [ "$LASTSTATUS" != "$STATUS" ]; then
	    echo "$SITE changed from $PUBSTATUS to $STATUS, skipping because previous was $LASTSTATUS"
	else
	    echo "$SITE changed from $PUBSTATUS to $STATUS"
	    (
	    echo "$SITE changed from $PUBSTATUS to $STATUS"
	    echo "    https://monit-grafana.cern.ch/d/000000855/overview-service-availability?orgId=1&var-isfe=All&var-regexp=%5e$XSLSNAME%24"
	    echo "    $URL"
	    echo "$NOTOKDETAILS"
	    ) >$MAILFILE

	    if [ -z "$EMAILS" ]; then
		EMAILS="$EMAIL"
	    fi
	    echo "Mailing $SITE=$STATUS to $EMAILS"
	    mail -s "Stratum1mon $SITE=$STATUS" $EMAILS <$MAILFILE

	    PUBSTATUS=$STATUS
	fi
	if $SHOWDETAILS && [ "$STATUS" != NORESPONSE ] && [ "$STATUS" != OK ]; then
	    echo "$NOTOKDETAILS"
	fi
    fi 
    STATUSES="$STATUSES PUB$STATNAME=$PUBSTATUS LAST$STATNAME=$STATUS"

    if [ -z "$PUBSTATUS" ]; then
	# upload a good value the first time
	PUBSTATUS="$STATUS"
    fi
    XSLSSTATUS=unavailable
    if [ "$PUBSTATUS" = OK ]; then
	XSLSSTATUS=available
    elif [ "$PUBSTATUS" = WARNING ]; then
	XSLSSTATUS=degraded
    fi

    if [ "$STATUS" = WARNING ] || [ "$STATUS" = CRITICAL ]; then
	AVAILABILITYINFO="$NOTOKDETAILS"
	# replace ampersands, less thans and greater thans
	AVAILABILITYINFO="${AVAILABILITYINFO//&/&amp;}"
	AVAILABILITYINFO="${AVAILABILITYINFO//</&lt;}"
	AVAILABILITYINFO="${AVAILABILITYINFO//>/&gt;}"
	# insert a break after each line
	AVAILABILITYINFO="`echo "$AVAILABILITYINFO"|sed 's,$,<br />,'`"
    else
	# note using pubstatus here; can't use that for the 'if' above
	#   because the details are not saved from the past
	AVAILABILITYINFO="$PUBSTATUS"
    fi

    # the - before !EOF! removes leading tabs only, not spaces
    XMLURL="${URL//&/&amp;}"
    cat >$XML.new <<-!EOF!
	<?xml version="1.0" encoding="utf-8"?>
	<serviceupdate xmlns="http://sls.cern.ch/SLS/XML/update">
	  <id>$XSLSNAME</id>
	  <status>$XSLSSTATUS</status>
	  <timestamp>$NOW</timestamp>
	  <availabilitydesc>Status according to $XMLURL</availabilitydesc>
	  <availabilityinfo>$AVAILABILITYINFO</availabilityinfo>
	  <contact>$EMAIL</contact>
	  <webpage>https://github.com/cvmfs-contrib/cvmfs-servermon/blob/master/README.md</webpage>
	</serviceupdate>
	!EOF!
    if [ -s $XML.new ]; then
	mv $XML.new $XML
	echo "uploading $XSLSSTATUS to $XSLSNAME"
	curl -sF file=@$XML xsls.cern.ch
    fi

done <$CONF

printf '%s\n' $STATUSES >$STAT

echo Finished at `date +%Y-%m-%dT%H:%M:%S`
echo
