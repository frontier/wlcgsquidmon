#!/bin/bash

/usr/sbin/logrotate -s /home/squidmon/logs/logrotate.status /home/squidmon/conf/logrotate.conf

find /home/squidmon/data/awstats -mtime +365 ! -type d | xargs rm -f
find /home/squidmon/data/awstats -depth -type d | xargs rmdir 2> /dev/null
