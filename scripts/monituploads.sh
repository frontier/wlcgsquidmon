#!/bin/bash

# NOTE: This file is copied at frontiermon/scripts and wlcgsquidmon/scripts.
#       If you change either one, change both.

DIR=$HOME/data/awstats
cd $DIR

if [ $HOME == "/home/dbfrontier" ]; then
    monitoring_system="maxthreads"
else
    monitoring_system="awstats"
fi
subject="Failing $monitoring_system uploads"

declare -A emails
emails=( ["atlas"]=atlas-frontier-support@cern.ch ["cms"]=cms-frontier-alarm@cern.ch ["cvmfs"]=cvmfs-stratum-alarm@cern.ch ["juno"]=junodb-monitor@juno.ihep.ac.cn )

mapping_file=awstatsSiteProjectNodesMapping
url=http://wlcg-squid-monitor.cern.ch/$mapping_file
wget -q -O $mapping_file $url
trap "rm -f $mapping_file" 0

echo "$(date '+%Y-%m-%d %H:%M:%S') - [INFO] Starting monituploads script"
if [ -f "$mapping_file" ] ; then
    if [ -s "$mapping_file" ] ; then
        for key in ${!emails[@]}; do
            failed_directories=()
            for directory in *; do
                if [[ "$directory" =~ $key ]]; then
                    for subdirectory in $DIR/$directory/*; do
                        if [ "$subdirectory" != "$DIR/$directory/*" ] && \
                           [ -z "`find $subdirectory/ -mtime -2 -type f | head -1`" ] ; then
                            if awk -v dir=$directory -v subdir=${subdirectory##*/} '{if (($1 == dir) && (($3 == subdir) || ($6 == subdir))) exit 1}' $mapping_file; then
                                continue
                            fi
                            failed_directories+="\n$subdirectory/"
                        fi
                    done
                fi
            done
            if [ ${#failed_directories[@]} -ne 0 ]; then
                err_msg="There haven't been any uploads for 48 hours:$failed_directories"
                echo -e "$err_msg"  | mail -s "$subject" "${emails[$key]}"
                echo "$(date '+%Y-%m-%d %H:%M:%S') - [ERROR] $err_msg"
            fi
        done
    else
        echo "$mapping_file file is empty"
    fi
else
    echo "$mapping_file file is not retrieved from $url"
fi
