#!/usr/bin/python3

import json
import shutil
import os
import sys
from urllib import request
from urllib import error
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import xmltodict


def main():

    out_fname = '/home/squidmon/www/cms-lcg-sites.json'
    if len(sys.argv) > 1:
        if sys.argv[1] == '-t':
            out_fname = 'cms-lcg-sites.json'
        else:
            print('Usage: get_cms_lcg_sites.py [-t]', file=sys.stderr)
            sys.exit(1)

    data = get_request('https://cmssst.web.cern.ch/cmssst/vofeed/vofeed.xml')
    lcg_cms_map = get_lcg_cms_map(data)
    with open(out_fname + '.new', 'w') as out_file:
        json.dump(lcg_cms_map, out_file, indent=2)
    try:
        shutil.copyfile (out_fname, out_fname + '.old')
    except:
        pass
    os.rename (out_fname + '.new', out_fname)

    return 0


def get_request(url):

    rq = request.Request(url)
    try:
        xml_string = request.urlopen(rq, timeout=30).read()
    except error.URLError:
        print("ERROR: socket timed out - URL %s" % url)
        exit(1)
    data = xmltodict.parse(xml_string)

    return data


def get_lcg_cms_map (data):

    lcg_cms_map = []
    lcg_names = set()

    for atp_site in data['root']['atp_site']:
        lcg_name = atp_site['@name']
        if lcg_name and lcg_name not in lcg_names:
            for group in atp_site['group']:
                group_name = group['@name']
                if group['@type'] == 'CMS_Site' and group_name:
                    lcg_names.add(lcg_name)
                    lcg_cms_map.append({'lcg': lcg_name, 'cms': group_name})

    lcg_cms_map = sorted(lcg_cms_map, key=lambda map: map['lcg'] + ' ' + map['cms'])

    return lcg_cms_map


if __name__ == '__main__':
    sys.exit(main())
