#!/usr/bin/env python3

import json, os, sys, time, socket, xmltodict
import ssl
import http.client as httplib
import netaddr
from urllib import request
from urllib import error

home="/home/squidmon/"
file_out = home + "www/grid-squids.json"

def combine_ip_and_port(ip, port):
    if port == 'NONE':
      return ip
    else:
      if ip.find(':') >= 0:
        # this is an ipv6 address, surround by square brackets
        #  to distinguish it from the port
        ip = '[' + ip + ']'
      return ip + ':' + port

def main():

    if len(sys.argv) > 1:
        global file_out
        global home
        if sys.argv[1] == '-t':
            home = "../../"
            file_out = "grid-squids.json"
        else:
            print("Usage: get_squids.py [-t]", file=sys.stderr)
            sys.exit(1)

    exceptions_file = home + "conf/exceptions/monitoring.txt"

    print(time.strftime("%c"))
    gocdb_squids = get_squids_gocdb()
    print(len(gocdb_squids), "squids from GOCDB")
    oim_squids = get_squids_oim() 
    print(len(oim_squids), "squids from OIM")
    all = gocdb_squids.copy()
    all.update(oim_squids)
    # Get IP addresses for each service
    for squid in all:
        ips, otherips = get_host_addrs(squid)
        for i in range(len(ips)):
            ips[i] = combine_ip_and_port(ips[i], '3401')
        all[squid]['ips']= ips
        for i in range(len(otherips)):
            otherips[i] = combine_ip_and_port(otherips[i], '3401')
        if len(otherips) > 0:
            all[squid]['otherips']= otherips
    #        print all[squid]
    # Add exceptions
    all = vo_exceptions(all, exceptions_file)

    fd = open (file_out + '.new', 'w')
    json.dump (all, fd, indent=2, sort_keys=True)
    fd.write('\n')
    fd.close()
    try:
        os.remove (file_out + '.old')
    except:
        pass
    try:
        os.link (file_out, file_out + '.old')
    except:
        pass
    os.rename (file_out + '.new', file_out)
    

#################################################################################
#
# get_squids_gocdb - Gets a list of squid services declared in the GOCDB
#
#################################################################################

def get_squids_gocdb ():

    base_url = "https://goc.egi.eu/gocdbpi"
    api_req = 'public/?method=get_service_endpoint&service_type=org.squid-cache.Squid&scope=alice,atlas,cms,lhcb,EGI,wlcg,Local,tier1,tier2&scope_match=any'
    resources = get_request (base_url, api_req) 

    squids = {}
    for resource in resources['results']['SERVICE_ENDPOINT']:
        if resource['SERVICE_TYPE'] == 'org.squid-cache.Squid':
            host_fqdn = resource['HOSTNAME']
            if host_fqdn not in squids:
                keyvals = {'name': resource['SITENAME'], 'source': 'egi'}
                if resource['NODE_MONITORED'] != 'Y':
                    keyvals['monitored'] = False
                squids.update ({host_fqdn: keyvals})
            else:
                print("WARNING: Squid " + host_fqdn + " has been declared multiple times in GOCDB")

    api_req = "public/?method=get_site"
    resources = get_request_cert(base_url, api_req)

    removeSquids = []#list of squids to remove
    for key, value in squids.items():
        for resource in resources["results"]["SITE"]:
            if value["name"] == resource["@NAME"]:
                if resource['CERTIFICATION_STATUS'] == 'Suspended':
                    removeSquids.append(key)
                for siteipname in ["SITE_IP", "SITE_IPV6"]:
                    if siteipname not in resource:
                        continue
                    site_ips = resource[siteipname].replace(";", ",").split(",")
                    for site_ip in site_ips:
                        ip_ranges = []
                        try:
                            ip_ranges.append(str(netaddr.IPNetwork(site_ip)))
                        except:
                            try:
                                parts = site_ip.split("/")
                                cidrs = netaddr.iprange_to_cidrs(parts[0], parts[1])
                                for cidr in cidrs:
                                    ip_ranges.append(str(netaddr.IPNetwork(cidr)))
                                print("INFO: IP address ranges " + site_ip + " are converted to CIDR subnets " + str(ip_ranges))
                            except:
                                print("WARNING: Squid " + key + " IP address ranges (" + site_ip + ") are in incorrect format")
                                continue
                        for ip_range in ip_ranges:
                            if "ipranges" in value:
                                value["ipranges"].append(ip_range)
                            else:
                                value["ipranges"] = [ip_range]
                break

    squids2 = {}
    for i in squids.keys():
        if i not in removeSquids:#remove suspended squids
            squids2[i] = squids[i]
    return squids2

#################################################################################
#
# get_squids_oim - Gets a list of squid services declared in OIM
#
#################################################################################

def get_squids_oim ():

    base_url = "https://my.opensciencegrid.org"
    api_req = 'rgsummary/xml?all_resources=on&gridtype=on&gridtype_1=on&active=on&active_value=1'
    resources = get_request (base_url, api_req) 
#    print resources
    squids = {}
    for resource in resources['ResourceSummary']['ResourceGroup']:
	# The site name is the GroupName of the resource
        name = resource['GroupName']
        resource_list = resource['Resources']['Resource']
	# if there is only one resource listed it is just a dictionary rather than a list so convert it to a list
        if isinstance (resource_list, dict):
            resource_list = [resource_list]
        for host in resource_list:
            host_fqdn = host['FQDN']
#                print host['Services']['Service']
            service_list = host['Services']['Service']
	    # if there is only one service listed it is just a dictionary rather than a list so convert it to a list
            if isinstance (service_list, dict):
                service_list = [service_list]
            for service in service_list:
                if service['Name'] == 'Squid':
                    if host_fqdn not in squids:
                        keyvals = {'name': name, 'source': 'osg'}
                        if 'Details' in service and \
                                service['Details'] != None and \
                                'Monitored' in service['Details'] and \
                                service['Details']['Monitored'] == 'False':
                            keyvals['monitored'] = False
                        squids.update ({host_fqdn: keyvals})
                    else:
                        print("WARNING: Squid " + host_fqdn + " has been declared multiple times in OIM")

    return squids

#################################################################################
#
# get_request - Used to request .xml dump from GOCDB (protection level 1) and OIM
#
#################################################################################

def get_request (base_url, api):

    source_url = "{base}/{api}".format (base=base_url, api=api)

    rq = request.Request (source_url)
    try:
        xml_string = request.urlopen(rq, timeout=30, context=ssl._create_unverified_context()).read()
    except socket.timeout:
        print("ERROR: socket timed out - URL %s" % source_url)
        exit(1)
    data = xmltodict.parse (xml_string)

    return data

#################################################################################
#
# X509CertAuth and X509CertOpen - to authenticate via Grid Certificate
#
#################################################################################

class X509CertAuth(httplib.HTTPSConnection):

    def __init__(self, host, *args, **kwargs):
        key_file = "/home/squidmon/certs/wlcg-squid-monitor-key.pem"
        cert_file = "/home/squidmon/certs/wlcg-squid-monitor-cert.pem"

        if not os.path.exists(key_file):
            print("No certificate private key file found")
            exit(1)

        if not os.path.exists(cert_file):
            print("No certificate public key file found")
            exit(1)

        httplib.HTTPSConnection.__init__(self, host, key_file=key_file, cert_file=cert_file,
                                         context=ssl._create_unverified_context(), **kwargs)

class X509CertOpen(request.AbstractHTTPHandler):

    def default_open(self, req):
        return self.do_open(X509CertAuth, req)

#################################################################################
#
# get_request - Used to request .xml dump from GOCDB (protection level 2)
#
#################################################################################

def get_request_cert(base_url, api):

    source_url = "{base}/{api}".format (base=base_url, api=api)
    opener = request.build_opener(X509CertOpen())
    datareq = request.Request(source_url)
    try:
        response = opener.open(datareq, timeout=30).read()
    except error.URLError:
        print("ERROR: socket timed out - URL %s" % source_url)
        exit(1)
    data = xmltodict.parse(response)

    return data

#################################################################################
#
# vo_exceptions applies the exceptions to the squid configuration table
#
#################################################################################

def vo_exceptions(squid_list, overide_file):

  with open(overide_file) as f:
    for line in f:
      fields = line.split()
      # Add a squid alias to be monitored
      if line.startswith('ADD'):
        fields = line.split()
        if len(fields) != 4:
          print("ERROR: Incorrect number of arguments in: " + line)
        else:
          name = fields[2]
          if name in squid_list:
            if squid_list[name]['source'] == 'exception':
              print("WARNING: Squid " + name + " has been declared multiple times in " + os.path.basename(overide_file))
            del squid_list[name]
          ips = []
          otherips = []
          for entry in fields[3].split(','):
            colon = entry.find(':')
            if colon > 0:
              name = entry[0:colon]
              port = entry[colon+1:]
            else:
              # If entry has no ':' then it is only a port
              port = entry
            ipList, otheripList = get_host_addrs(name)
            for ip in ipList:
              ips.append(combine_ip_and_port(ip, port))
            for otherip in otheripList:
              otherips.append(combine_ip_and_port(otherip, port))

          squid_list.update({fields[2]: {'name': fields[1], 'source': 'exception', 'ips': ips}})
          if len(otherips) > 0:
            squid_list[fields[2]]['otherips'] = otherips
          if port == 'NONE':
            squid_list[fields[2]]['monitored'] = False
      # Remove a squid alias from being monitored
      if line.startswith('REMOVE'):
        fields = line.split()
        if len(fields) != 2:
          print("ERROR: Incorrect number of arguments in: " + line)
        else:
          if fields[1] in squid_list:
            del squid_list[fields[1]] 
          else:
            print("WARNING: Alias " + fields[1]  + " doesn't exist.")
#  print squid_list
  return squid_list

################################################################################
#
# Methods to get host names behind a DNS alias.
#
################################################################################

old_grid_squids = {}
read_old_grid_squids = False

# extract IP address from ipAndPort, possibly with square brackets around ip
def get_ip(ipAndPort):
    colon = ipAndPort.rfind(':')
    if colon > 0:
        ip = ipAndPort[:colon]
    else:
        ip = ipAndPort
    if ip[0] == '[':
        ip = ip[1:-1]
    return ip

def get_host_addrs (hostname):
  
    if is_a_valid_ip4 (hostname):
        return [hostname], []
    ips, otherips = get_dns_addresses(hostname)
    if len(ips) > 0:
        # Keep them sorted to make monitoring plots belong to the same
        #  address as much as possible.  It doesn't matter if they're
        #  in numerical or ascii order, just that they're stable.
        return sorted(list(ips)), sorted(list(otherips))

    global old_grid_squids, read_old_grid_squids
    if not read_old_grid_squids:
        read_old_grid_squids = True
        try:
            with open(file_out, 'r') as old_file:
                old_grid_squids = json.load(old_file)
        except:
            print("Failed to read " + file_out + ", continuing")
            pass

    if (hostname in old_grid_squids) and ('ips' in old_grid_squids[hostname]):
        print("Failure looking up " + hostname + ", continuing with old ips")
        ips = []
        otherips = []
        hostsquids = old_grid_squids[hostname]
        for ipAndPort in hostsquids['ips']:
            ips.append(get_ip(ipAndPort))
        if 'otherips' in hostsquids:
            for ipAndPort in hostsquids['otherips']:
                otherips.append(get_ip(ipAndPort))
        return ips, otherips

    print("Warning: DNS lookup failed for " + hostname + " and no old ips found")
    return ['0.0.0.0'], []

def is_a_valid_ip4 (address):
  
    parts = address.split('.')
    try:
        return len(parts) == 4 and all( (0 <= int(x) < 256) for x in parts )
    except:
        return False

def get_dns_addresses (hostname):
    ips = set()
    otherips = set()
  
    try:
        info = socket.getaddrinfo(hostname, 0, 0, socket.IPPROTO_TCP)
    except:
        return [], []

    for tuple in info:
        if (tuple[0] == socket.AF_INET):
            ips.add(tuple[4][0])
        elif (tuple[0] == socket.AF_INET6):
            otherips.add(tuple[4][0])
    if len(ips) == 0:
	# no IPv4, so use IPv6 as the primary ips
        ips = otherips
        otherips = set()
    return ips, otherips


if __name__ == "__main__":
    main()

