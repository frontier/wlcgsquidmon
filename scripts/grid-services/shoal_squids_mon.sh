#!/bin/bash

HOME=/home/squidmon
LOG=$HOME/logs
cd $HOME/scripts/grid-services/
python3 get_shoal_squids.py >> $LOG/get_shoal_squids.log 2>&1
./grid_service_mon.sh $HOME/www/shoal-squids-latest.json >> $LOG/grid_service_mon_shoal-squids-latest.log 2>&1

