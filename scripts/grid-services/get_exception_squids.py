#!/usr/bin/env python3

import json, os, sys
from optparse import OptionParser


def main():
    parser = OptionParser()
    parser.add_option("-t",
                      action="store_const",
                      const="",
                      default="/home/squidmon/",
                      dest="directory",
                      help="Write and read files from current directory")
    sub_choices = ("cvmfs", "juno")
    parser.add_option("--sub",
                      dest="sub",
                      help="Create squids description for specific sub type from monitoring exceptions file. Example choices are " + ', '.join(sub_choices) + ". (Required)")
    (options, args) = parser.parse_args()
    if not options.sub:
        print("--sub option missing", file=sys.stderr)
        parser.print_help(sys.stderr)
        sys.exit(1)
    sub = options.sub
    directory = options.directory
    file_out = sub + "-squids.json"
    exceptions_file = "monitoring.txt"
    grid_squids_file = "grid-squids.json"
    if directory:
        www_directory = "www/"
        exceptions_file = directory + "conf/exceptions/" + exceptions_file
        grid_squids_file = directory + www_directory + grid_squids_file
        file_out = directory + www_directory + file_out
    grid_squids = read_json_file(grid_squids_file)
    squids_dictionary = get_squids_dictionary(sub, exceptions_file, grid_squids, grid_squids_file)
    create_json(squids_dictionary, file_out)


def read_json_file(file_name):
    with open(file_name) as f:
        data = json.load(f)

    return data


def get_squids_dictionary(sub, exceptions_file, grid_squids, grid_squids_file):
    found_sub = False
    squids = {}
    with open(exceptions_file) as f:
        for line in f:
            if found_sub == False:
                if line.lower().startswith("# " + sub + " exceptions"):
                    found_sub = True
                    continue
                else:
                    continue
            if line.startswith("###########"):
                break
            fields = line.split()
            if fields:
                if fields[0] == "ADD":
                    host = fields[2]
                    if host in grid_squids:
                        squids[host] = grid_squids[host]
                        squids[host]["gridname"] = squids[host]["name"]
                    else:
                        print("WARNING: " + host + " squid is not found in " + grid_squids_file + " file.")
                elif fields[0] == "SITE":
                    site = fields[1]
                    if len(fields) > 2:
                        hosts = fields[2]
                        for host in hosts.split(','):
                            if host in grid_squids:
                                squids[host] = grid_squids[host]
                                squids[host]["gridname"] = squids[host]["name"]
                            else:
                                print("WARNING: " + host + " squid is not found in " + grid_squids_file + " file.")
                    else:
                        for host in grid_squids:
                            gridsquid = grid_squids[host]
                            if "name" in gridsquid and gridsquid["name"] == site:
                                squids[host] = gridsquid
                                squids[host]["gridname"] = site
                        if len(squids) == 0:
                            print("WARNING: " + site + " site is not found in " + grid_squids_file + " file.")

    return squids


def create_json(data, file_out):
    file_out_new = file_out + ".new"
    file_out_old = file_out + ".old"
    with open(file_out_new, "w") as f:
        json.dump(data, f, indent=2)
        f.write("\n")
    try:
        os.remove(file_out_old)
    except:
        pass
    try:
        os.link(file_out, file_out_old)
    except:
        pass
    os.rename(file_out_new, file_out)


if __name__ == "__main__":
    main()

