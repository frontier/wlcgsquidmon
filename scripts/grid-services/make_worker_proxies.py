#!/usr/bin/env python3

import os, sys, time, re, glob, socket, netaddr
import json, xmltodict
import maxminddb

agis_site_info_fname = '/home/squidmon/www/cric.json'
cms_lcg_sites_fname = '/home/squidmon/www/cms-lcg-sites.json'
cms_siteconf_dir = '/cvmfs/cms.cern.ch/SITECONF'
cms_siteconf_subpath = 'JobConfig/site-local-config.xml'
grid_squids_fname = '/home/squidmon/www/grid-squids.json'
site_squids_fname = '/home/squidmon/www/site-squids.json'
out_fname = '/home/squidmon/www/worker-proxies.json'
exceptions_fname = '/home/squidmon/conf/exceptions/workerview.json'
gireader = maxminddb.open_database('/home/squidmon/etc/wpad/GeoIP2-ISP.mmdb')

def logmsg(msg):
    print(msg)

def logsitelist(sitelist):
    sitelist.sort()
    linelen = 0
    line = ''
    for site in sitelist:
        site = str(site)
        if len(site) + linelen > 75:
            print(line + ',')
            linelen = 0
            line = ''
        elif linelen > 0:
            line += ', '
            linelen += 2
        line += site
        linelen += len(site)
    if linelen > 0:
        print(line)

old_worker_proxies = {}
read_old_worker_proxies = False
def load_old_worker_proxies():
    global old_worker_proxies, read_old_worker_proxies
    if not read_old_worker_proxies:
        read_old_worker_proxies = True
        try:
            with open(out_fname, 'r') as old_file:
                old_worker_proxies = json.load(old_file)
        except:
            logmsg('Failed to read ' + out_fname + ', continuing')
            pass

def getips(name):
    try:
        info = socket.getaddrinfo(name, 0, 0, 0, socket.IPPROTO_TCP)
        info.sort()
        iplist = set()
        # Check for IPv4 first; use IPv6 if no IPv4 found.
        for family in [socket.AF_INET, socket.AF_INET6]:
            for tuple in info:
                if (tuple[0] == family):
                    iplist.add(tuple[4][0])
            if len(iplist) > 0:
                return list(sorted(iplist))
    except:
        pass

    load_old_worker_proxies()

    if (name in old_worker_proxies) and ('ip' in old_worker_proxies[name]):
        logmsg('Failure looking up ' + name + ', continuing with old ip')
        return [str(old_worker_proxies[name]['ip'])]

    # don't need to print message here because there will be one later
    return None

def searchlist(orglist, target):
    for item in orglist:
        if str(target) == item[1]:
            return 1
    return 0

def getiporgs(name):
    iplist = getips(name)
    if iplist is None:
        return [[None, None]]

    org = None
    orglist = []
    for ip in iplist:
        gir = gireader.get(ip)
        if gir is not None:
            # an alternate is 'organization', see comment in wlcg-wpad
            org = gir['isp']
            if searchlist(orglist, org) == 0:
                orglist.append([ip, org])

    if len(orglist) < 1:
        return [[None, None]]

    return orglist

# update all ips of the given proxies in the worker proxies entry of index
#   idx, for both ipv4 & ipv6
def updateips(idx, entry, proxies):

    for proxy in proxies:
        colon = proxy.find(':')
        if colon > 0:
            proxy = proxy[0:colon]
        if proxy in entry['ips']:
            continue

        ips = []
        try:
            info = socket.getaddrinfo(proxy, 0, 0, 0, socket.IPPROTO_TCP)
            info.sort()
            for tuple in info:
                if (tuple[0] == socket.AF_INET) or (tuple[0] == socket.AF_INET6):
                    ips.append(tuple[4][0])
        except:
            load_old_worker_proxies

            if (idx in old_worker_proxies) and \
                    ('ips' in old_worker_proxies[idx]) and \
                    (proxy in old_worker_proxies[idx]['ips']):
                logmsg('Failure looking up ' + proxy + ', continuing with old ips')
                ips = old_worker_proxies[idx]['ips'][proxy]

        if ips == []:
            logmsg('Failure looking up ' + proxy + ' and no old ips available')
            continue

        entry['ips'][proxy] = sorted(ips)


def finalizenewfile(fname):
    try:
        os.remove (fname + '.old')
    except:
        pass
    try:
        os.link (fname, fname + '.old')
    except:
        pass
    os.rename (fname + '.new', fname)


def map_proxy_names(proxyinfo, namemaps, site):
    if site in namemaps:
        namemap = namemaps[site]
        for nametype in ['names', 'cmsnames']:
            if nametype in namemap:
                if nametype not in proxyinfo:
                    proxyinfo[nametype] = []
                proxyinfo[nametype].extend(namemap[nametype])


def main():

    if len(sys.argv) > 1:
        if sys.argv[1] == '-t':
            global out_fname
            global site_squids_fname
            global grid_squids_fname
            global exceptions_fname
            site_squids_fname = 'site-squids.json'
            if os.path.exists('grid-squids.json'):
                grid_squids_fname = 'grid-squids.json'
            out_fname = 'worker-proxies.json'
            exceptions_fname = os.path.dirname(sys.argv[0]) + '/../../conf/exceptions/workerview.json'
        else:
            print("Usage: make_worker_proxies.py [-t]", file=sys.stderr)
            sys.exit(1)

    print("Starting at " + time.strftime("%c"))

    # read in ATLAS AGIS squids
    with open(agis_site_info_fname, 'r') as agis_site_info_file:
        agis_site_info = json.load(agis_site_info_file)
    logmsg("Number of ATLAS AGIS sites: " + str(len(agis_site_info)))

    agis_site_squids = {}
    for site_info in agis_site_info:
        if 'fsconf' in site_info:
            fsconfdata = site_info['fsconf']
            if 'squid' in fsconfdata:
                site = site_info['name']
                if site not in agis_site_squids:
                    agis_site_squids[site] = {'source' : 'ATLAS'}
                if 'proxies' not in agis_site_squids[site]:
                    agis_site_squids[site]['proxies'] = []
                for fsquid in fsconfdata['squid']:
                    if len(fsquid) != 2:
                        logmsg('Skipping squids for AGIS site ' + site + ' because there are not 2 entries under first squid listed')
                        break
                    for squid in [fsquid[0]] + list(fsquid[1].keys()):
                        # note the value of the fsquid[1] keys can be ACTIVE or
                        #  DISABLED but that info appears to be not kept up to date
                        if squid.startswith('http://'):
                            squid = squid[7:]
                        # remove everything after a port number because
                        # some entries were seen to have garbage
                        squid = re.sub(re.compile(r'([^:]*:[0-9]*).*'), r'\1', squid)
                        agis_site_squids[site]['proxies'].append(squid)
    logmsg("Number of AGIS sites with squids: " + str(len(agis_site_squids)))

    # read in cms/lcg translation from siteDB
    with open(cms_lcg_sites_fname, 'r') as cms_lcg_sites_file:
        cms_lcg_sites = json.load(cms_lcg_sites_file)
    # one lcg site can map to multiple cms sites, but cms sites only
    #   map to one lcg site
    sitelists_lcg2cms = {}
    sites_cms2lcg = {}
    for cms_lcg_site in cms_lcg_sites:
        cms = cms_lcg_site['cms']
        lcg = cms_lcg_site['lcg']
        if lcg not in sitelists_lcg2cms:
            sitelists_lcg2cms[lcg] = []
        sitelists_lcg2cms[lcg].append(cms)
        sites_cms2lcg[cms] = lcg
    logmsg("Number of CMS sites in siteDB: " + str(len(sitelists_lcg2cms)))

    # read in the CMS SITECONF squids
    cms_site_squids = {}
    for cmssite in sites_cms2lcg:
        site = sites_cms2lcg[cmssite]
        fname = cms_siteconf_dir + '/' + cmssite + '/' + cms_siteconf_subpath
        if not os.path.exists(fname):
            continue
        with open(fname, 'r') as file:
            xml = file.read()
        if len(xml) == 0:
            continue
        try:
            data = xmltodict.parse(xml)
            sitedata = data['site-local-config']['site']
            frontierdata = sitedata['calib-data']['frontier-connect']
        except Exception as e:
            logmsg('Error parsing ' + fname + ', skipping: ' + str(e))
            continue
        if 'proxy' not in frontierdata:
            continue
        cms_site_squids[site] = {'source' : 'CMS'}
        if 'load' in frontierdata:
            loads = frontierdata['load']
            if isinstance (loads, dict):
                loads = [loads]
            for load in loads:
                if '@balance' not in load:
                    continue
                if load['@balance'] == 'proxies':
                    cms_site_squids[site]['loadbalance'] = 'proxies'
        proxies = frontierdata['proxy']
        if isinstance (proxies, dict):
            proxies = [proxies]
        for proxy in proxies:
            if '@url' not in proxy:
                continue
            if 'proxies' not in cms_site_squids[site]:
                cms_site_squids[site]['proxies'] = []
            squid = proxy['@url']
            if squid.startswith('http://'):
                squid = squid[7:]
            cms_site_squids[site]['proxies'].append(squid)

    logmsg("Number of CMS SITECONF sites with squids: " + str(len(cms_site_squids)))

    # combine ATLAS into CMS
    site_squids = cms_site_squids.copy()
    for site in agis_site_squids:
        if site in site_squids:
            # site exists for both ATLAS & CMS
            # add new ATLAS proxies after the CMS proxies
            for proxy in agis_site_squids[site]['proxies']:
                if proxy not in site_squids[site]['proxies']:
                    site_squids[site]['proxies'].append(proxy)
            site_squids[site]['source'] = 'CMS + ATLAS'
        else:
            site_squids[site] = agis_site_squids[site]

    # write out intermediate file listing all the sites with squids
    with open (site_squids_fname + '.new', 'w') as fd:
        json.dump (site_squids, fd, indent=2, sort_keys=True)
    finalizenewfile(site_squids_fname)

    # read in the registered squids
    with open(grid_squids_fname) as grid_squids_file:
        grid_squids = json.load(grid_squids_file)
    logmsg("Number of registered squids in GOCDB + OIM + exceptions: " + str(len(grid_squids)))
    grid_site_squids = {}
    for squid in grid_squids:
        if 'name' not in grid_squids[squid]:
            continue
        site = grid_squids[squid]['name']
        if site in grid_site_squids:
            grid_site_squids[site]['proxies'].append(squid)
        else:
            grid_site_squids[site] = {}
            grid_site_squids[site]['proxies'] = [squid]
            if 'ipranges' in grid_squids[squid]:
                ipranges = []
                for iprange in grid_squids[squid]['ipranges']:
                    try:
                        netaddr.IPNetwork(iprange)
                    except:
                        logmsg('Failure parsing IP range ' + iprange + ' for site ' + site)
                        continue
                    if not iprange.startswith('0.0.0.0'):
                        ipranges.append(iprange)
                if ipranges:
                    grid_site_squids[site]['ipranges'] = ipranges

    logmsg('')

    # combine the data for the worker view output file
    worker_proxies = {}
    orgsquids = {}
    numregmatched = 0
    numnotregistered = 0
    numreplacedbyregistered = 0
    numnoorg = 0
    numproxymatches = 0
    sitesnotregistered = []
    overlapsites = []
    nummultiorgsiteextraorgs = 0
    squidorgs = {}
    sitesdisabledduetomultigeoips = set()
    for site in site_squids:
        # note that these first several lines are duplicated in the next loop
        siteproxies = site_squids[site]['proxies']
        org = None
        ip = None
        orgsquid = None
        pub = True
        for proxy in siteproxies:
            colon = proxy.find(':')
            if colon > 0:
                proxy = proxy[0:colon]
            orglist = getiporgs(proxy)
            if len(orglist) > 1:
                sitesdisabledduetomultigeoips.add(site)
            # Just take first entry
            aip = orglist[0][0]
            aorg = orglist[0][1]
            if aorg is None:
                # there is at least one private address
                pub = False
            elif org is None:
                # the first public proxy represents the organization
                orgsquid = proxy
                ip = aip
                org = aorg

        pubproxies = siteproxies

        if site in grid_site_squids:
            numregmatched += 1
            if not pub:
                # there is at least one squid on a private network
                #  so replace them by registered squids
                numreplacedbyregistered += 1
                pubproxies = grid_site_squids[site]['proxies']
                # all registered squids are public so any one will do to
                #  be the representative orgsquid
                orgsquid = pubproxies[0]
                logmsg('Note: using ' + orgsquid + ' from GOCDB or OIM for site ' + site)
                orglist = getiporgs(orgsquid)
                # Just take first entry
                ip = orglist[0][0]
                org = orglist[0][1]
        else:
            numnotregistered += 1
            sitesnotregistered.append(site)
        if org is None:
            numnoorg += 1
            logmsg("No geo org found for site '" + site + "', skipping")
            continue

        source = site_squids[site]['source']
        if org in orgsquids:
            # add to existing entry
            orgsquid = orgsquids[org]
            if worker_proxies[orgsquid]['source'] != source:
                # combine the sources
                sources = worker_proxies[orgsquid]['source'].split(' + ')
                for s in source.split(' + '):
                    if s not in sources:
                        sources.append(s)
                worker_proxies[orgsquid]['source'] = ' + '.join(sources)
            defaultproxies = worker_proxies[orgsquid]['proxies'][0]['default']
            if defaultproxies[0] == siteproxies[0]:
                numproxymatches += 1
                worker_proxies[orgsquid]['names'].append(site)
            elif 'altnames' in worker_proxies[orgsquid]:
                overlapsites.append(site)
                worker_proxies[orgsquid]['altnames'].append(site)
            else:
                overlapsites += worker_proxies[orgsquid]['names']
                overlapsites.append(site)
                worker_proxies[orgsquid]['altnames'] = [site]
        else:
            # create a new orgsquids entry
            orgsquids[org] = orgsquid
            squidorgs[orgsquid] = org
            worker_proxies[orgsquid] =  { 'ip'      : ip,
                                          'ips'     : {},
                                          'names'   : [site],
                                          'proxies' : [{'default' : siteproxies}],
                                          'source'  : source
                                        }
            if 'loadbalance' in site_squids[site]:
                worker_proxies[orgsquid]['proxies'][0]['loadbalance'] = \
                    site_squids[site]['loadbalance']

        updateips(orgsquid, worker_proxies[orgsquid], pubproxies)

    # now look for alternative GeoIP orgs for the other proxies
    multiorgsites = []
    for site in site_squids:
        # note that these first several lines are duplicated from above
        siteproxies = site_squids[site]['proxies']
        orgsquid = siteproxies[0]
        colon = orgsquid.find(':')
        if colon > 0:
            orgsquid = orgsquid[0:colon]

        if orgsquid not in squidorgs:
            # this one didn't get mapped to a worker_proxies entry, skip it
            continue
        org = squidorgs[orgsquid]
        siteorgs = [org]
        firstorgsquid = orgsquids[org]
        idx = 0
        for orgsquid in siteproxies[1:]:
            idx += 1
            colon = orgsquid.find(':')
            if colon > 0:
                orgsquid = orgsquid[0:colon]
            orglist = getiporgs(orgsquid)
            # Just take first entry. Other entries get disabled elsewhere.
            ip = orglist[0][0]
            org = orglist[0][1]
            if org is None:
                # other squid doesn't map to an org, skip it
                continue
            if org in siteorgs:
                # same org as a previous one, skip it
                continue
            if org in orgsquids:
                # but it was already connected with a different site,
                #  so skip it
                continue
            nummultiorgsiteextraorgs += 1
            siteorgs.append(org)
            if site not in multiorgsites:
                multiorgsites.append(site)
            orgsquids[org] = orgsquid
            worker_proxies[orgsquid] = worker_proxies[firstorgsquid].copy()
            worker_proxies[orgsquid]['ip'] =  ip
            worker_proxies[orgsquid]['proxies'] =  \
                [{'default' : siteproxies[idx:]+siteproxies[0:idx]}]

    logmsg('')
    logmsg("Number of orgs added for multi-org sites: " + str(nummultiorgsiteextraorgs))

    # read in and apply the exceptions
    with open(exceptions_fname, 'r') as exceptions_file:
        exceptions_string = exceptions_file.read()
    # remove comments with a hash sign & following to the end of lines
    exceptions_string = re.sub(re.compile('#.*'), '', exceptions_string)
    exceptions = json.loads(exceptions_string)

    numexceptionsreplaced = 0
    numexceptionsaugmented = 0
    numexceptionsrenamed = 0
    numexceptionsnew = 0
    logmsg("Number of total orgs before exceptions: " + str(len(worker_proxies)))
    for squid in exceptions:
        source = 'exception'
        if 'namemaps' in exceptions[squid]:
            if squid not in worker_proxies:
                logmsg("No existing squid " + squid + " for exceptions namemaps to modify, skipping")
                continue
            numexceptionsaugmented += 1
            source = worker_proxies[squid]['source'] + ' + ' + source
            namemaps = exceptions[squid]['namemaps']
            worker_proxies[squid]['namemaps'] = namemaps
            worker_proxies[squid]['source'] = source
            for mapname in namemaps:
                if 'remove' in namemaps[mapname]:
                    for name in namemaps[mapname]['remove']:
                        for nametype in ['names', 'cmsnames', 'altnames']:
                            if nametype in worker_proxies[squid] and \
                                    name in worker_proxies[squid][nametype]:
                                worker_proxies[squid][nametype].remove(name)
            continue
        orglist = getiporgs(squid)
        # Just take first entry. Other entries get disabled elsewhere.
        ip = orglist[0][0]
        org = orglist[0][1]
        if org is None:
            if ip is None:
                missing = 'ip'
            else:
                missing = 'org'
            logmsg("No " + missing + " for " + squid + " in exceptions file, skipping")
            continue
        if org in orgsquids:
           if 'insertproxies' in exceptions[squid]:
                orgsquid = orgsquids[org]
                if orgsquid not in worker_proxies:
                    # this shouldn't ever happen
                    logmsg("orgsquid " + orgsquid + " for exceptions insertproxies not in worker proxies, skipping")
                    continue
                numexceptionsaugmented += 1
                source = worker_proxies[orgsquid]['source'] + ' + ' + source
                worker_proxy = worker_proxies[orgsquid]
                if 'proxies' not in worker_proxy:
                    worker_proxy['proxies'] = []
                worker_proxy['proxies'][:0] = exceptions[squid]['insertproxies']
                exceptions[squid] = worker_proxy
           else:
                numexceptionsreplaced += 1
                if orgsquids[org] != squid:
                    numexceptionsrenamed += 1
                    del worker_proxies[orgsquids[org]]
        else:
            numexceptionsnew += 1
        orgsquids[org] = squid
        if 'mapsto' in exceptions[squid]:
            mappedsquid = exceptions[squid]['mapsto']
            if mappedsquid in worker_proxies:
                worker_proxies[squid] = worker_proxies[mappedsquid].copy()
                worker_proxies[squid]['mapsto'] = mappedsquid
            else:
                logmsg('Error: ' + squid + ' mapsto ' + mappedsquid + ' failed to find a match')
                worker_proxies[squid] = exceptions[squid]
        else:
            worker_proxies[squid] = exceptions[squid]
        worker_proxies[squid]['source'] = source
        worker_proxies[squid]['ip'] = ip
        worker_proxies[squid]['ips'] = {}
        pubproxies = set()
        if 'proxies' in worker_proxies[squid]:
            for proxyentry in worker_proxies[squid]['proxies']:
                for item in proxyentry:
                    for proxy in proxyentry[item]:
                        colon = proxy.find(':')
                        if (colon > 0) and (proxy[colon+1].find(':') < 0):
                            # Only add the entries with exactly 1 colon
                            #  because it could be an ipv4 or ipv6 address
                            #  range.  Note that this way of distinguishing
                            #  them is quite a hack because it is just a
                            #  coincidence that only squid names have exactly
                            #  one colon, and that may change in the future.
                            pubproxies.add(proxy[0:colon])
        else:
            pubproxies.add(squid)
        updateips(squid, worker_proxies[squid], pubproxies)
    logmsg("Number of total orgs after exceptions: " + str(len(worker_proxies)))

    # look for overlapping sites to disable
    numsitesdisabled = 0
    numorgsenabled = 0
    sitesenabled = set()
    numsitesdisambiguated = 0
    for proxy in worker_proxies:
        if 'altnames' in worker_proxies[proxy]:
            sites = list(worker_proxies[proxy]['names'])
            sites += worker_proxies[proxy]['altnames']
            sites.sort()
            ipranges = {}
            # look for ipranges for matching site name
            for site in sites:
                if site in grid_site_squids and 'ipranges' in grid_site_squids[site]:
                    ipranges[site] = grid_site_squids[site]['ipranges']
            namemaps = {}
            if 'namemaps' in worker_proxies[proxy]:
                # For those that didn't find an iprange, look for ipranges
                #  for mapped site name, in case the name in GocDB is different
                #  than the ATLAS/CMS name.
                namemaps = worker_proxies[proxy]['namemaps']
                for mapsite in namemaps:
                    if mapsite in ipranges or mapsite not in grid_site_squids \
                            or 'ipranges' not in grid_site_squids[mapsite]:
                        continue
                    map = namemaps[mapsite]
                    if 'names' not in map:
                        continue
                    for site in map['names']:
                        if site not in sites:
                            continue
                        ipranges[site] = grid_site_squids[mapsite]['ipranges']
            worker_proxies[proxy]['proxies'] = []
            for site in ipranges:
                proxyinfo = {
                    'ipranges' : ipranges[site],
                    'names'    : [site],
                    'default'  : site_squids[site]['proxies']
                }
                if 'loadbalance' in site_squids[site]:
                    proxyinfo['loadbalance'] = site_squids[site]['loadbalance']
                map_proxy_names(proxyinfo, namemaps, site)
                worker_proxies[proxy]['proxies'].append(proxyinfo)
            all_sites_number = len(sites)
            names = set()
            for squid_proxy in worker_proxies[proxy]['proxies']:
                names.update(squid_proxy['names'])
            for name in names:
                if name in sites:
                    sites.remove(name)
            sites_number = len(sites)
            if sites_number <= 1:
                numsitesdisambiguated += all_sites_number
                default_site = set(sites) - set(ipranges)
                if len(default_site) != 0:
                    site = list(default_site)[0]
                    proxyinfo = {
                        'names'   : [site],
                        'default' : site_squids[site]['proxies']
                    }
                    map_proxy_names(proxyinfo, namemaps, site)
                    worker_proxies[proxy]['proxies'].append(proxyinfo)
            else:
                msg = 'Multiple site names share GeoIP org: '
                numsitesdisabled += sites_number
                worker_proxies[proxy]['disabled'] = msg + ', '.join(sites)
        else:
            for proxy_info in worker_proxies[proxy]['proxies']:
                if proxy_info.get('ipranges') is None:
                    sites = list(worker_proxies[proxy]['names'])
                    sites.sort()
                    ipranges = {}
                    # look for ipranges for matching site name
                    for site in sites:
                        if site in grid_site_squids and 'ipranges' in grid_site_squids[site]:
                            ipranges[site] =grid_site_squids[site]['ipranges']
                    if len(ipranges) != 0:
                        if len(sites) == 1:
                            worker_proxies[proxy]['proxies'][0]['ipranges'] = ipranges[site]
                            worker_proxies[proxy]['proxies'][0]['names'] = [site]
                        else:
                            for site in ipranges:
                                proxyinfo = {
                                    'ipranges' : ipranges[site],
                                    'names'    : [site],
                                    'default'  : site_squids[site]['proxies']
                                }
                                worker_proxies[proxy]['proxies'].append(proxyinfo)

            numorgsenabled += 1
            if 'names' in worker_proxies[proxy]:
                for name in worker_proxies[proxy]['names']:
                    if name not in sitesdisabledduetomultigeoips:
                        sitesenabled.add(name)

        if 'namemaps' in worker_proxies[proxy]:
            del worker_proxies[proxy]['namemaps']

        # Add cmsnames for CMS sites
        if 'CMS' in worker_proxies[proxy]['source']:
            sites = list(worker_proxies[proxy]['names'])
            if 'altnames' in worker_proxies[proxy]:
                sites += worker_proxies[proxy]['altnames']
            worker_proxies[proxy]['cmsnames'] = []
            for site in sites:
                if site in sitelists_lcg2cms:
                    worker_proxies[proxy]['cmsnames'].extend(sitelists_lcg2cms[site])
                    for entry in worker_proxies[proxy]['proxies']:
                        if 'names' in entry and entry['names'][0] == site:
                            entry['cmsnames'] = sitelists_lcg2cms[site]

        # Check if site has multiple GeoIP orgs
        sitecheck = list(worker_proxies[proxy]['names'])
        if 'altnames' in worker_proxies[proxy]:
            sitecheck += worker_proxies[proxy]['altnames']
        disabledsites = []
        for onesite in sitecheck:
            if onesite in sitesdisabledduetomultigeoips:
                disabledsites.append(onesite)
        if len(disabledsites) > 0:
            msg = "Proxy DNS alias for these sites uses IP addresses in two GeoIP organizations: "
            worker_proxies[proxy]['disabled'] = msg + ', '.join(disabledsites)

    # finally, write the output file
    with open(out_fname + '.new', 'w') as fd:
        json.dump (worker_proxies, fd, indent=2, sort_keys=True)
        fd.write('\n')
    finalizenewfile(out_fname)

    # and print the summary statistics
    logmsg('')
    logmsg('Sites with no squid registered in GOCDB or OIM:')
    logsitelist(sitesnotregistered)
    logmsg('')
    logmsg('Sites disabled because GeoIP org overlaps with other registered site:')
    logsitelist(overlapsites)
    logmsg('')
    logmsg('Sites disabled due to proxies using IPs from two GeoIP orgs:')
    logsitelist(list(sitesdisabledduetomultigeoips))
    logmsg('')
    logmsg('Sites expanded to more than one GeoIP org:')
    logsitelist(multiorgsites)
    logmsg('')

    logmsg('Number of squid site names that matched registered squid site names: ' + str(numregmatched))
    logmsg('Number of squid sites without registered squid: ' + str(numnotregistered))
    logmsg('Number of squids replaced by registered squid: ' + str(numreplacedbyregistered))
    logmsg('Number of sites with first squid matching another site\'s: ' + str(numproxymatches))
    logmsg('Number of squid sites without a GeoIP org: ' + str(numnoorg))
    logmsg('Number of squid sites disabled for overlapping GeoIP org: ' + str(numsitesdisabled))
    logmsg('Number of squid sites disabled due to proxies using IPs from two GeoIPs: ' + str(len(sitesdisabledduetomultigeoips)))
    logmsg('Number of squid sites enabled for overlapping GeoIP org: ' + str(numsitesdisambiguated))
    logmsg('Number of squid sites enabled: ' + str(len(sitesenabled)))
    logmsg('Number of exceptions mapped to new GeoIP org: ' + str(numexceptionsnew))
    logmsg('Number of exceptions replacing a GeoIP org: ' + str(numexceptionsreplaced))
    logmsg('Number of exceptions replacing a GeoIP org with different squid index: ' + str(numexceptionsrenamed))
    logmsg('Number of exceptions augmenting a GeoIP org: ' + str(numexceptionsaugmented))
    logmsg('Number of enabled GeoIP organizations: ' + str(numorgsenabled))

    print("Finished at " + time.strftime("%c"))
    print()

if __name__ == "__main__":
    main()
