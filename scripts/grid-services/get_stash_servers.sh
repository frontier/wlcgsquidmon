#!/bin/bash
# This file is used by a special api in the wlcg-wpad service.
# The api is used by stashcp.
date
set -ex
cd `dirname $0`/../../etc/wpad
wget -qO stashservers.whitelist.new http://oasis.opensciencegrid.org/cvmfs/stashservers/.cvmfswhitelist
if ! cmp stashservers.whitelist.new stashservers.whitelist >/dev/null; then
    cp stashservers.whitelist.new stashservers.whitelist
fi
