#!/usr/bin/env python3

import fnmatch, json, os
from optparse import OptionParser


def main():
    parser = OptionParser()
    parser.add_option("-t",
                      action="store_const",
                      const="",
                      default="/home/squidmon/",
                      dest="directory",
                      help="Write and read files from current directory")
    (options, args) = parser.parse_args()
    directory = options.directory
    file_out = "cms-squids.json"
    if directory:
        all_sites = read_json_file(directory + "www/grid-squids.json")
        cms_sites = read_json_file(directory + "www/cms-lcg-sites.json")
        exceptions_file = directory + "conf/exceptions/mrtgcmsexceptions.txt"
        file_out = directory + "www/" + file_out
    else:
        all_sites = read_json_file("grid-squids.json")
        cms_sites = read_json_file("cms-lcg-sites.json")
        exceptions_file = "mrtgcmsexceptions.txt"
    cms_dictionary = get_cms_squids_list(all_sites, cms_sites)
    cms_with_exceptions = make_exceptions(exceptions_file, all_sites, cms_dictionary)
    create_json(cms_with_exceptions, file_out)


def read_json_file(file_name):
    with open(file_name) as f:
        data = json.load(f)

    return data


def get_cms_squids_list(all_sites, cms_sites):
    cms_dictionary = {}
    lcg_set = set()
    for cms_site in cms_sites:
        lcg = cms_site["lcg"]
        cms = cms_site["cms"]
        if not (lcg in lcg_set):
            lcg_set.add(lcg)
            for key, value in all_sites.items():
                if "name" in value and value["name"] == lcg:
                    cms_dictionary[key] = dict(value)
                    cms_dictionary[key]["gridname"] = cms_dictionary[key]["name"]
                    cms_dictionary[key]["name"] = cms
        else:
            for key, value in cms_dictionary.items():
                if "gridname" in value and value["gridname"] == lcg:
                    cms_list = [cms, value["name"]]
                    cms_list.sort(reverse=False)
                    if cms == cms_list[0]:
                        value["name"] = cms

    return cms_dictionary


def make_exceptions(exceptions_file, all_sites, cms_dictionary):
    cms_with_exceptions = cms_dictionary
    with open(exceptions_file) as f:
        for line in f:
            if line.startswith("+") or line.startswith("-"):
                fields = line.split()
                fields_number = len(fields)
                first_sign = fields[0]
                if fields_number > 2:
                    if first_sign == "+":
                        for key, value in all_sites.items():
                            if "name" in value and fnmatch.fnmatch(value["name"], fields[1]):
                                if (fields_number == 3) or (fields_number == 4 and fnmatch.fnmatch(key, fields[3])):
                                    cms_with_exceptions[key] = dict(value)
                                    cms_with_exceptions[key]["gridname"] = cms_with_exceptions[key]["name"]
                                    cms_with_exceptions[key]["name"] = fields[2]
                    else:
                        for key in list(cms_with_exceptions.keys()):
                            if fnmatch.fnmatch(cms_with_exceptions[key]["gridname"], fields[1]):
                                if (fields_number == 3) or (fields_number == 4 and fnmatch.fnmatch(key, fields[3])):
                                    del cms_with_exceptions[key]

    return cms_with_exceptions


def create_json(cms_with_exceptions, file_out):
    file_out_new = file_out + ".new"
    file_out_old = file_out + ".old"
    with open(file_out_new, "w") as f:
        json.dump(cms_with_exceptions, f, indent=2)
        f.write("\n")
    try:
        os.remove(file_out_old)
    except:
        pass
    try:
        os.link(file_out, file_out_old)
    except:
        pass
    os.rename(file_out_new, file_out)


if __name__ == "__main__":
    main()
