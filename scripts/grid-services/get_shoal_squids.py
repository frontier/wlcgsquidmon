#!/usr/bin/env python3

import json, filecmp, os, shutil, sys, time
from urllib import request

print("Starting at %s" % time.strftime("%c"))

file_out_latest = "/home/squidmon/www/shoal-squids-latest.json"
if len(sys.argv) > 1:
    if sys.argv[1] == "-t":
        file_out_latest = "shoal-squids-latest.json"
    else:
        print("Usage: get_shoal_squids.py [-t]")
        sys.exit(1)

url = "http://shoal.heprc.uvic.ca/all"
try:
    response = request.urlopen(url, timeout=30)
except request.URLError as e:
    print("There was an error: %r" % e)
    sys.exit(1)
data = json.loads(response.read())

squids = {}
for key, value in data.items():
    hostname = value["hostname"]
    squids[hostname] = {}
    squids[hostname]["public_ip"] = value["public_ip"]
    private_ip = value["private_ip"]
    if private_ip is not None:
        squids[hostname]["private_ip"] = private_ip
    city = value["geo_data"]["city"]
    if city is not None:
        squids[hostname]["city"] = city
    squids[hostname]["country_code"] = value["geo_data"]["country_code"]

file_out_latest_new = open(file_out_latest + ".new", "w")
json.dump(squids, file_out_latest_new, indent=2, sort_keys=True)
file_out_latest_new.close()
try:
    os.remove(file_out_latest + ".old")
except:
    pass
try:
    os.link(file_out_latest, file_out_latest + ".old")
except:
    pass
os.rename (file_out_latest + ".new", file_out_latest)

file_out = file_out_latest.replace("-latest", "")
if os.path.isfile(file_out): 
    if not filecmp.cmp(file_out_latest, file_out):
        os.remove(file_out)
        os.link(file_out_latest, file_out)
else:
    shutil.copy(file_out_latest, file_out)

print("Finished at %s \n" % time.strftime("%c"))

