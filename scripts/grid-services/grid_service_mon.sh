#!/bin/bash
# This script should be run immediately after grid-service, because
# it checks if output of grid-service has been modified in the last
# three minutes.

MAILDEST="wlcg-squidmon-support@cern.ch"

if [ $# -ne 1 ]; then
    echo "One argument of file path should be passed."
    exit 1
fi

# $1 - status
# $2 - file path
function status {
    directory=/home/squidmon/etc/grid-services/
    mkdir -p $directory
    cd $directory
    STAT="${2##*/}"
    file=$STAT.status
    #if [ "$STAT" = shoal-squids-latest.json ]; then
    #    # temporary redirect while experiencing shoal problems
    #    MAILDEST="dwd@fnal.gov edita.kizinevic@cern.ch"
    #fi
    if [ ! -f "$file" ]; then
        echo "LAST=OK" >> $file
        echo "EMAIL=OK" >> $file
    fi
    LAST=`grep -oP '(?<=LAST=).*' $file`
    EMAIL=`grep -oP '(?<=EMAIL=).*' $file`
    if [ "$1" == "$LAST" ]; then
        if [ "$1" != "$EMAIL" ]; then
            if [ "$1" == "OK" ]; then
                echo | mail -s "$2 file generation is back to normal" $MAILDEST
            else
                echo | mail -s "$2 file generation has failed" $MAILDEST
            fi
            sed -i "s/EMAIL=\(.*\)/EMAIL=$1/" $file
        fi
    else
        sed -i "s/LAST=\(.*\)/LAST=$1/" $file
    fi 
}

if [ -f "$1" ]; then
    if [ -s "$1" ]; then
        if [ -z "`find $1 -mmin -3`" ]; then
            status "FAILED" $1
        else
            status "OK" $1
        fi
    else
        status "FAILED" $1
    fi
else
    status "FAILED" $1
fi
