#!/usr/bin/python3

# This script calls other 4 scripts when cronjob runs.
# March 20, 2017 --Amjad
# Modified March 28, 2017

import sys
import os
import subprocess

# NOTE: In general, every script called that generates something should
#  be followed by a call to grid_service_mon.sh verifying that a file
#  was modified, to make sure the script completed.  If necessary, have
#  the script just touch a file just before it exits and have
#  grid_service_mon.sh verify that.

subprocess.call('/home/squidmon/scripts/cric/download.py >> /home/squidmon/logs/cric.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/download.out >> /home/squidmon/logs/grid_service_mon_download.log 2>&1', shell=True) 

subprocess.call('/home/squidmon/scripts/cric/extract.py >> /home/squidmon/logs/cric.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/extract.out >> /home/squidmon/logs/grid_service_mon_extract.log 2>&1', shell=True) 

subprocess.call('/home/squidmon/scripts/grid-services/get_squids.py >> /home/squidmon/logs/get_grid_squids.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/www/grid-squids.json >> /home/squidmon/logs/grid_service_mon_grid_squids.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/grid-services/get_exception_squids.py --sub cvmfs >> /home/squidmon/logs/get_cvmfs_squids.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/www/cvmfs-squids.json >> /home/squidmon/logs/grid_service_mon_cvmfs.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/grid-services/get_exception_squids.py --sub juno >> /home/squidmon/logs/get_juno_squids.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/www/juno-squids.json >> /home/squidmon/logs/grid_service_mon_juno.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/all/makeConfig.py >> /home/squidmon/logs/all/makeConfig.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/makeConfig.all.out >> /home/squidmon/logs/grid_service_mon_makeConfig.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/all/makeConfig.py --sub cvmfs >> /home/squidmon/logs/all/makeConfig_cvmfs.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/makeConfig.cvmfs.out >> /home/squidmon/logs/grid_service_mon_makeConfig.cvmfs.log 2>&1', shell=True)

#subprocess.call('/home/squidmon/scripts/all/makeConfig.py --sub cvmfstesting >> /home/squidmon/logs/all/makeConfig_cvmfstesting.log 2>&1', shell=True)
#subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/makeConfig.cvmfstesting.out >> /home/squidmon/logs/grid_service_mon_makeConfig.cvmfstesting.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/all/makeConfig.py --sub juno >> /home/squidmon/logs/all/makeConfig_juno.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/makeConfig.juno.out >> /home/squidmon/logs/grid_service_mon_makeConfig.juno.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/grid-services/get_cms_lcg_sites.py >> /home/squidmon/logs/get_cms_lcg_sites.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/www/cms-lcg-sites.json >> /home/squidmon/logs/grid_service_mon_cms-lcg-sites.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/grid-services/get_cms_squids.py >> /home/squidmon/logs/get_cms_squids.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/www/cms-squids.json >> /home/squidmon/logs/grid_service_mon_cms-squids.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/all/makeConfig.py --sub cms >> /home/squidmon/logs/all/makeConfig_cms.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/makeConfig.cms.out >> /home/squidmon/logs/grid_service_mon_makeConfig.cms.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/all/makeConfig.py --sub cmssummary >> /home/squidmon/logs/all/makeConfig_cmssummary.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/makeConfig.cmssummary.out >> /home/squidmon/logs/grid_service_mon_makeConfig.cmssummary.log 2>&1', shell=True)

#subprocess.call('/home/squidmon/scripts/all/makeConfig.py --sub cmstesting >> /home/squidmon/logs/all/makeConfig_cmstesting.log 2>&1', shell=True)
#subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/makeConfig.cmstesting.out >> /home/squidmon/logs/grid_service_mon_makeConfig.cmstesting.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/grid-services/make_worker_proxies.py >> /home/squidmon/logs/make_worker_proxies.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/www/worker-proxies.json >> /home/squidmon/logs/grid_service_mon_worker-proxies.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/grid-services/get_stash_servers.sh >> /home/squidmon/logs/get_stash_servers.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/wpad/stashservers.whitelist.new >> /home/squidmon/logs/grid_service_mon_stashservers.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/mrtg/atlas2/SquidList.py >> /home/squidmon/logs/SquidList.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/www/snmpstats/mrtgatlas2/input.json >> /home/squidmon/logs/grid_service_mon_input.log 2>&1', shell=True)

subprocess.call('/home/squidmon/scripts/mrtg/atlas2/PageBuilder.py >> /home/squidmon/logs/PageBuilder.log 2>&1', shell=True)
subprocess.call('/home/squidmon/scripts/grid-services/grid_service_mon.sh /home/squidmon/etc/grid-services/PageBuilder.out >> /home/squidmon/logs/grid_service_mon_PageBuilder.log 2>&1', shell=True)


