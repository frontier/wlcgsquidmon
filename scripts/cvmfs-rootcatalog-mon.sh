#!/bin/bash
# Probe EGI & OSG primary stratum 1s for repositories with too-big
#   root catalogs
# Written by Dave Dykstra, 4/30/19
#

MAXSIZE=25

declare -A ALLREPOS
ALLREPOS[restricted.computecanada.ca]="exclude"

OUTFILE=/tmp/cvmfs-rootcatalog-mon.out.$$

for S1 in cvmfs-egi.gridpp.rl.ac.uk cvmfs-s1goc.opensciencegrid.org cvmfs-s1-beluga.computecanada.ca ; do
    URL="http://$S1:8000"
    for CVREPO in `curl -s $URL/cvmfs/info/v1/repositories.json|sed -n 's/.*"url"[^"]*"\([^"]*\)".*/\1/p'`; do
	REPO="${CVREPO##*/}"
	if [ -n "${ALLREPOS[$REPO]-}" ]; then
	    # been there, done that
	    continue
	fi
	CATALOG="`curl -s $URL$CVREPO/.cvmfspublished|cat -v|sed -n 's/^C//p'|head -1`"
	if [ -z "$CATALOG" ]; then
	    # silently continue, couldn't read .cvmfspublished
	    continue
	fi
	ALLREPOS[$REPO]="$CATALOG"
	DIR="`echo "$CATALOG"|cut -c1-2`"
	FILE="`echo "$CATALOG"|cut -c3-`"C
	SIZE="`curl -sI $URL$CVREPO/data/$DIR/$FILE|sed -n 's/^Content-Length: \([0-9]*\).*/\1/p'`"
	if [ -z "$SIZE" ]; then
	    echo "Cannot determine size of root catalog for $REPO on $S1" >&2
	    continue
	fi
	let SIZE/=1024*1024
	if [ "$SIZE" -gt $MAXSIZE ]; then
	    EXPIRES="`curl -s $URL$CVREPO/.cvmfswhitelist|cat -v|sed -n 's/^E//p'|head -1|cut -c1-8`"
	    # only report too-big size for unexpired repositories
	    if [ "$EXPIRES" -gt "`date +%Y%m%d`" ]; then
		echo $REPO root catalog on $S1 too big: $SIZE MB
	    fi
	fi
    done
done > $OUTFILE 2>&1

if [ -s $OUTFILE ]; then
    mail -s "Too-big cvmfs root catalogs" cvmfs-stratum-alarm@cern.ch < $OUTFILE
fi
rm -f $OUTFILE
