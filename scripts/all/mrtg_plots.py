#!/usr/bin/python3

import os, subprocess, re
from optparse import OptionParser

################################################################
#
# mrtg_plot.py creates graphs from squids data as well as mrtg
# log files associated with their creation
# 
################################################################

def main():

  parser = OptionParser()
  sub_choices = ("cms", "cmstesting", "all", "cmssummary", "cvmfs", "cvmfstesting")
  sub_choices = ("cms", "all", "cvmfs", "cmssummary")
  sub_default = "all"
  parser.add_option("--sub",
                    default=sub_default,
                    dest="sub",
                    help="Create MRTG page for specific sub. Example choices are %s. Default: %s" % (sub_choices, sub_default))
  (options, args) = parser.parse_args()
  sub = options.sub

  # export LANG=C required for MRTG to work
  os.environ["LANG"] = "C"

  # Get .cfg filenames from directory defined by MRTG_CFG_DIR environment and log directory
  etc_directory = "/home/squidmon/etc/"
  log_directory = "/home/squidmon/logs/"
  snmp_directory = "/home/squidmon/www/snmpstats/"
  cfgDir = etc_directory + sub
  mrtgLogDir = log_directory + sub
  snmpDir = snmp_directory + "mrtg" + sub
  cfgList =  os.listdir(cfgDir)

  if not os.path.exists(mrtgLogDir):
    os.makedirs(mrtgLogDir)

  # Get directories used from environment file
  mrtgBinDir = '/home/squidmon/scripts/mrtg/bin'
  mrtgBin = mrtgBinDir + '/mrtg'
  baseLogDir = '--logging ' +  mrtgLogDir

  mrtgSmoother = '/home/squidmon/scripts/all/mrtgSmoother'

  # Separate those ending in _N and do them first because the combined ones
  #   depend on them
  numberedCfgs = []
  plainCfgs = []
  r1 = re.compile(r'_[0-9]+$')
  for fname in cfgList:
    if fname.endswith('.cfg'):
      monname = fname.replace('.cfg', '')
      if r1.search(monname):
        numberedCfgs.append(monname)
      else:
        plainCfgs.append(monname)

  # Loop over .cfg files and run the mrtg command, numbered first
  for cfgs in [numberedCfgs, plainCfgs]:
    pipe = os.popen("parallel -j15",'w')
    for monname in cfgs:
      site_cfg = cfgDir + '/' + monname + '.cfg'
      logName = monname + '.log'
      # Directory name for .log files
      logDir = baseLogDir + '/' + logName
      cmd = mrtgBin + ' ' + site_cfg + ' ' + logDir
      smoothCmd = mrtgSmoother + ' ' + site_cfg + ' ' + snmpDir + '/' + monname
      print('echo ' + cmd + '; ' + cmd + '; echo ' + smoothCmd + '; ' + smoothCmd, file=pipe)
    pipe.close()


if __name__ == "__main__":
  main()
