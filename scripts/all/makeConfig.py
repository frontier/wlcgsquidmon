#!/usr/bin/python3


import string, json, socket, os, sys, subprocess, fileinput, re, shutil, time, filecmp
from optparse import OptionParser
from datetime import datetime


def main():
  print("Started at " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
  parser = OptionParser()
  parser.add_option("-t",
                    action="store_const",
                    const="",
                    default="/home/squidmon/",
                    dest="directory",
                    help="Write and read files from current directory")
  sub_choices = ("cms", "all", "cvmfs", "cmssummary")
  sub_default = "all"
  parser.add_option("--sub",
                    default=sub_default,
                    dest="sub",
                    help="Create MRTG page for specific sub. Example choices are %s. Default: %s" % (sub_choices, sub_default))
  (options, args) = parser.parse_args()
  directory = options.directory
  sub = options.sub
  squids_file = sub + "-squids.json"
  mrtg_directory = "mrtg" + sub + "/"
  menu_file = "menu" + sub + ".html"
  squids_deleted_file = sub + "-squids-deleted.txt"
  index_name = sub
  title = sub.upper()
  if sub == "cmssummary":
    squids_file = "cms-squids.json"
    title = "Summary For CMS"
    index_name = "indexsumcms"
  elif sub.startswith("cms") or sub == "cvmfs":
    index_name = "index" + sub
  elif sub == "all":
    squids_file = "grid-squids.json"
    squids_deleted_file = "grid-squids-deleted.txt"
    menu_deleted_file = "menualldeleted.html"
    title = "WLCG"
  if directory:
    script_dir = directory + "scripts/all/"
    www_dir = directory + "www/snmpstats/"
    etc_dir = directory + "etc/" + sub + "/"
    etc_grid_service_dir = directory + "etc/grid-services/"
    json_file = open(directory + "www/" + squids_file)
  else:
    script_dir = "./"
    www_dir = "www/"
    etc_dir = "etc/"
    etc_grid_service_dir = "etc/grid-services/"
    json_file = open(squids_file)
  if not os.path.exists(etc_dir):
    os.makedirs(etc_dir)
  if not os.path.exists(etc_grid_service_dir):
    os.makedirs(etc_grid_service_dir)

  squids_data = json.load(json_file)
  json_file.close()
  os.chdir(script_dir)
  menu_list = []
  if sub == "cmssummary":
    squids_egi_t1 = []
    squids_egi_t2 = []
    squids_egi_t3 = []
    squids_osg_t1 = []
    squids_osg_t2 = []
    squids_osg_t3 = []
    squids_exception = []

  # Create templates
  for alias in squids_data:
    if 'monitored' in squids_data[alias] and not squids_data[alias]['monitored']:
      continue
    ips = squids_data[alias]["ips"]
    site = squids_data[alias]["name"]
    site = site.replace(" ", "_")
    if sub != "all":
      gridname = squids_data[alias]["gridname"]
    else:
      gridname = None
    if sub == "cmssummary":
      source = squids_data[alias]["source"]
      exception_egi = False
      exception_osg = False
      if source == "exception":
        if site.startswith("T1_") or site.startswith("T2_") or site.startswith("T3_"):
          host_name_end = alias.split(".")[-1]
          if len(host_name_end) == 2 and not host_name_end.isdigit():
            exception_egi = True
          else:
            exception_osg = True
        else:
          squids_exception.append((gridname, alias, ips))
      if source == "egi" or exception_egi:
        if site.startswith("T1_"):
          squids_egi_t1.append((gridname, alias, ips))
        elif site.startswith("T2_"):
          squids_egi_t2.append((gridname, alias, ips))
        elif site.startswith("T3_"):
          squids_egi_t3.append((gridname, alias, ips))
      elif source == "osg" or exception_osg:
        if site.startswith("T1_"):
          squids_osg_t1.append((gridname, alias, ips))
        elif site.startswith("T2_"):
          squids_osg_t2.append((gridname, alias, ips))
        elif site.startswith("T3_"):
          squids_osg_t3.append((gridname, alias, ips))

    if sub != "cmssummary":
      make_template(site, alias, " & ".join(ips), www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname)
      make_index_template(site, alias, www_dir, mrtg_directory, sub)
      menu_list.append((site, alias))

      # If not only one squid create a template for each squid site and an index for the website
      if len(ips) > 1:
        n = 0
        for ip in ips:
          alias_with_number = alias + "_" + str(n)
          make_template(site, alias_with_number, ip, www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname)
          make_index_template(site, alias_with_number, www_dir, mrtg_directory, sub)
          menu_list.append((site, alias_with_number))
          n = n + 1

  # Create a summary page for cms
  if sub == "cmssummary":
     squids_global = squids_egi_t1 + squids_egi_t2 + squids_egi_t3 + squids_osg_t1 + squids_osg_t2 + squids_osg_t3 + squids_exception
     if squids_egi_t1:
       menu_list = make_summary_template("EGI-T1", www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_egi_t1, menu_list)
     if squids_egi_t2:
       menu_list = make_summary_template("EGI-T2", www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_egi_t2, menu_list)
     if squids_egi_t3:
       menu_list = make_summary_template("EGI-T3", www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_egi_t3, menu_list)
     if squids_osg_t1:
       menu_list = make_summary_template("OSG-T1", www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_osg_t1, menu_list)
     if squids_osg_t2:
       menu_list = make_summary_template("OSG-T2", www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_osg_t2, menu_list)
     if squids_osg_t3:
       menu_list = make_summary_template("OSG-T3", www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_osg_t3, menu_list)
     if squids_global:
       menu_list = make_summary_template("Global", www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_global, menu_list)

  # Get all squids from configuration files
  squids = set()
  for _, _, files in os.walk(etc_dir):
    for file in files:
      if file.endswith(".cfg"):
        squids.add(file[:-4])
  squids_deleted = set(squids)

  # Create menu and get lists of squids, which are deleted, and squids, which are not deleted
  menu_list.sort()
  if sub == "cms" or sub == "cmstesting":
    launchpads = [x for x in menu_list if x[0].startswith("Lpad_")]
    menu_list = [x for x in menu_list if x not in launchpads]
    launchpadsCMS = [x for x in launchpads if x[1].startswith("cmsfrontier.cern.ch")]
    launchpads = [x for x in launchpads if x not in launchpadsCMS]
    menu_list = launchpadsCMS + launchpads + menu_list
  menu_file_dir = www_dir + menu_file
  create_template("headerfile.txt", None, menu_file_dir, "a", False)
  for name, alias in menu_list:
    if alias == "":
      name_with_alias = name
    else:
      name_with_alias = name + "_" + alias
    d = {"DIR": mrtg_directory + name_with_alias, "SITE": name, "ALIAS": alias}
    if sub == "all":
      create_template("menu_template_for_all_sub.txt", d, menu_file_dir, "a", False)
    else:
      create_template("menu_template.txt", d, menu_file_dir, "a", False)
    if name_with_alias in squids:
      squids_deleted.remove(name_with_alias)
  create_template("footerfile.txt", None, menu_file_dir, "a")

  # Update file of deleted squids and create menu for deleted squids
  squids_existed = squids - squids_deleted
  squids_deleted_file_dir = directory + "www/" + squids_deleted_file
  if os.path.exists(squids_deleted_file_dir) and os.path.getsize(squids_deleted_file_dir) > 0:
    f = open(squids_deleted_file_dir)
    squids_deleted_from_file = set(line.rstrip("\n") for line in f)
    f.close()
  else:
    squids_deleted_from_file = set()
  for squid in squids_deleted_from_file.copy():
    if squid in squids_existed:
      squids_deleted_from_file.remove(squid)
  squids_deleted_from_file = sorted(squids_deleted_from_file.union(squids_deleted))
  year_ago = time.time() - 365 * 24 * 3600
  for root, directories, _ in os.walk(www_dir + mrtg_directory):
    for directory in directories:
      if directory in squids_deleted_from_file:
        proxy_obj_log = root + directory + "/proxy-obj.log"
        if not os.path.exists(proxy_obj_log):
          print("WARNING: " + proxy_obj_log + " file is not exsist.")
          continue
        f = open(proxy_obj_log)
        proxy_obj_log_data = [line.rstrip("\n") for line in f]
        f.close()
        if len(proxy_obj_log_data) > 1:
          no_data = True
          for data in proxy_obj_log_data[1:]:
            if year_ago < int(data.split()[0]):
              if data.endswith(" 0 0 0 0"):
                continue
              else:
                no_data = False
                break
            else:
              break
          if no_data:
            shutil.rmtree(os.path.join(root, directory))
            squids_deleted_from_file.remove(directory)
  file_out_new = squids_deleted_file_dir + ".new"
  file_out_old = squids_deleted_file_dir + ".old"
  if sub == "all":
    menu_file_deleted_dir = www_dir + menu_deleted_file
    create_template("headerfile.txt", None, menu_file_deleted_dir, "a", False)
  f = open(file_out_new, "w")
  if squids_deleted_from_file:
    for squid in squids_deleted_from_file:
      f.write(squid + "\n")
      if sub == "all":
        squid_splited = squid.rsplit('_', 1)
        if squid_splited[1].isdigit():
          squid_splited = squid.rsplit('_', 2)
          squid_splited[1] = squid_splited[1] + "_" + squid_splited[2]
        d = {"DIR": mrtg_directory + squid, "SITE": squid_splited[0], "ALIAS": squid_splited[1]}
        create_template("menu_template_for_all_sub.txt", d, menu_file_deleted_dir, "a", False)
  f.close()
  if sub == "all":
    create_template("footerfile.txt", None, menu_file_deleted_dir, "a")
  try:
    os.remove(file_out_old)
  except:
    pass
  try:
    os.link(squids_deleted_file_dir, file_out_old)
  except:
    pass
  os.rename(file_out_new, squids_deleted_file_dir)

  # Remove *.cfg* and *.ok files which are no longer needed
  if squids_deleted:
    for root, _, files in os.walk(etc_dir):
      for file in files:
        basename = file.rsplit(".", 1)[0]
        if basename in squids_deleted:
          file_path = os.path.join(root, file)
          if file.endswith(".cfg"):
            ip_address = ""
            with open(file_path, "r") as f:
              for line in f:
                if "Description" in line:
                  ip_address = line.strip().replace("<TR><TD>Description:</TD><TD>", "").replace("</TD></TR>", "")
                  break
            print(basename + " (" + ip_address + ") configuration file was deleted.")
          os.remove(file_path)

  # Create main pages
  if menu_list[0][1] == "":
    name_with_alias = menu_list[0][0]
  else:
    name_with_alias = menu_list[0][0] + "_" + menu_list[0][1]
  d = {"TITLE": title + " Squid Monitoring", "MENU": menu_file, "MAIN": mrtg_directory + name_with_alias + "/"}
  create_template("index_template.txt", d, www_dir + index_name + ".html", "w+")
  if sub == "all":
    if squids_deleted_from_file:
      d = {"TITLE": "WLCG Deleted Squid Monitoring", "MENU": menu_deleted_file, "MAIN": mrtg_directory + squids_deleted_from_file[0] + "/"}
      create_template("index_template.txt", d, www_dir + sub + "deleted.html", "w+")
    else:
      d = {"TITLE": "WLCG Deleted Squid Monitoring", "PARAGRAPH": "THERE ARE NO DELETED SQUIDS!"}
      create_template("no_information_template.txt", d, www_dir + sub + "deleted.html", "w+")

  f = open(etc_grid_service_dir + "makeConfig." + sub + ".out", "w")
  f.write("makeConfig.py script execution finished succesfully!")
  f.close()
  print("Finished at " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def create_template(template, values, configuration_file, mode, rename=True):
  filein = open(template, "r")
  result = filein.read()
  filein.close()
  if values is not None:
    src = string.Template(result)
    result = src.substitute(values)
  configuration_file_new = configuration_file + ".new"
  fileout = open(configuration_file_new, mode)
  fileout.write(result)
  fileout.close()
  if rename:
    if configuration_file.endswith(".cfg"):
      if os.path.isfile(configuration_file):
        if filecmp.cmp(configuration_file, configuration_file_new):
          os.remove(configuration_file_new)
          return
        print(values["SITE"] + " (" + values["DNSNAME"] + ") configuration file was changed.")
      else:
        print(values["SITE"] + " (" + values["DNSNAME"] + ") configuration file was added.")
    os.rename(configuration_file_new, configuration_file)


# make_template creates a webpage from the configuration information
def make_template(site, alias, squid, www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_list=None):
  if alias == "":
    site_with_alias = site
  else:
    site_with_alias = site + "_" + alias
  directory = www_dir + mrtg_directory + site_with_alias
  if not os.path.exists(directory):
    os.makedirs(directory)
  endTarget = ""
  if "&" not in squid:
    if sub != "all":
      if squids_list is None:
        endTarget = " " + www_dir + mrtg_directory.replace(sub, "all") + gridname  + "_" + alias + "`"
      else:
        endTarget = " " + www_dir + mrtg_directory.replace(sub, "all") + squids_list[0][0] + "_" + squids_list[0][1] + "`"
  else:
    if sub == "all":
      endTarget = " " + directory + "_*`"
    else:
      if squids_list is None:
        endTarget = " " + www_dir + mrtg_directory.replace(sub, "all") + gridname  + "_" + alias + "_*`"
      else:
        mrtg_all_directory = mrtg_directory.replace(sub, "all")
        for squid_tuple in squids_list:
          endTarget += " " + www_dir + mrtg_all_directory + squid_tuple[0]  + "_" + squid_tuple[1]
        endTarget += "`"
  if endTarget:
    startTarget = "`" + script_dir + "targetCopy " + directory + "/"
    hitTarget = startTarget + "proxy-hit" + endTarget
    objTarget = startTarget + "proxy-obj" + endTarget
    srvkbinoutTarget = startTarget + "proxy-srvkbinout" + endTarget
    cputimeTarget = startTarget + "proxy-cpu" + endTarget
    descriptorsTarget = startTarget + "proxy-descriptors" + endTarget
  else:
    hitTarget = "cacheProtoClientHttpRequests&cacheServerRequests:public@" + squid
    objTarget = "cacheNumObjCount&cacheMemUsage:public@" + squid
    srvkbinoutTarget = "cacheHttpOutKb&cacheServerInKb:public@" + squid
    cputimeTarget = "cacheCpuTime&cacheCpuTime:public@" + squid
    descriptorsTarget = "cacheCurrentUnusedFDescrCnt&cacheCurrentFileDescrCnt:public@" + squid
  if sub == "all":
    d = {"WORKDIR": directory, "SITE": site_with_alias, "HIT_TARGET": hitTarget, "OBJ_TARGET": objTarget, "SRVKBINOUT_TARGET": srvkbinoutTarget, "CPUTIME_TARGET": cputimeTarget, "DESCRIPTORS_TARGET": descriptorsTarget, "DNSNAME": squid}
    create_template("template_for_all_sub.txt", d, etc_dir + site_with_alias + ".cfg", "w+")
  else:
    d = {"WORKDIR": directory, "SITE": site_with_alias, "HIT_TARGET": hitTarget, "OBJ_TARGET": objTarget, "SRVKBINOUT_TARGET": srvkbinoutTarget, "DNSNAME": squid}
    create_template("template.txt", d, etc_dir + site_with_alias + ".cfg", "w+")


# make_index_template creates the index webpages for each site
def make_index_template(site, alias, www_dir, mrtg_directory, sub):
  if alias == "":
    site_with_alias = site
  else:
    site_with_alias = site + "_" + alias
  directory = www_dir + mrtg_directory + site_with_alias
  d = {"TITLE": site_with_alias + " individual site plots", "MENU": site_with_alias + ".html", "MAIN": ""}
  create_template("index_template.txt", d, directory + "/index.html", "w+")

  d = {"SITE": site, "ALIAS": alias}
  if sub == "all":
    create_template("index2_template_for_all_sub.txt", d, directory + "/" + site_with_alias + ".html", "w+")
  else:
    create_template("index2_template.txt", d, directory + "/" + site_with_alias + ".html", "w+")


# make_summary_template combines make_template and make_index_template functions for summary pages
def make_summary_template(site, www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_list, menu_list):
  make_template(site, "", " & ".join([y for x in squids_list for y in x[2]]), www_dir, etc_dir, script_dir, mrtg_directory, sub, gridname, squids_list)
  make_index_template(site, "", www_dir, mrtg_directory, sub)
  menu_list.append((site, ""))

  return menu_list


if __name__ == "__main__":
  main()
