#!/bin/bash
# This is an MRTG log data smoother that eliminates erroneous traffic spikes.
#   If the first data entry's fetches are greater than requests, then assume
#   it is an erroneous entry and zero it out and invalidate the counters.
#   MRTG has its own analagous builtin workaround with the "MaxBytes" config
#   option, but that has to be adjusted for different types of plots and isn't
#   as reliable.
# Assumes this is run after every time mrtg updates the data.
# Apply it only to those mrtg plots that read from SNMP, not those that
#   copy from other plots.
#
#  See https://oss.oetiker.ch/mrtg/doc/mrtg-logfile.en.html for description
#   of the fields in the .log files.
# Written by Dave Dykstra 7 November 2019

PROG=mrtgSmoother

usage()
{
    echo "Usage: $PROG mrtg_cfg_file mrtg_data_dir" >&2
    echo >&2
    exit 2
}

fatal()
{
    echo "$PROG: $*" >&2
    exit 1
}

if [ "$#" -ne 2 ]; then
    usage
fi

if [ ! -f "$1" ]; then
    fatal "$1 not found"
fi

for LOGTYPE in hit srvkbinout obj cpu descriptors; do
    LOGFILE=$2/proxy-$LOGTYPE.log
    if [ ! -f $LOGFILE ]; then
        if [ "$LOGTYPE" == "cpu" ] || [ "$LOGTYPE" == "descriptors" ]; then
            continue
        else
	    fatal "$LOGFILE does not exist"
        fi
    fi
    OLDFILE=$2/proxy-$LOGTYPE.old
    CONFCHANGED=0
    if [ -f $OLDFILE ]; then
	read OLDSTAMP OLDREQS OLDFETCHES REST <$OLDFILE
	echo "mrtgSmoother $OLDFILE old counters: $OLDSTAMP $OLDREQS $OLDFETCHES"
	if [ $1 -nt $OLDFILE ]; then
	    CONFCHANGED=1
	fi
    else
	OLDREQS=-1
	OLDFETCHES=-1
    fi
    if [ "$LOGTYPE" == "hit" ] || [ "$LOGTYPE" == "srvkbinout" ]; then
        HASREQFETCH=1
    else
        HASREQFETCH=0
    fi
    # use awk instead of bash commands here in order to efficiently copy
    #  the rest of the file after manipulating the first couple of lines
    awk -v LOGTYPE=$LOGTYPE -v OLDREQS=$OLDREQS -v OLDFETCHES=$OLDFETCHES -v CONFCHANGED=$CONFCHANGED -v HASREQFETCH=$HASREQFETCH '
	BEGIN {
	    # the first line has timestamp, reqs counter, fetch counter
	    getline
	    TS=$1
	    REQS=$2
	    FETCHES=$3
	    # Next get the first line of regular data with same timestamp,
	    #   avg elapsed reqs, avg elapsed fetches, max reqs, and max fetches
	    # Avg and max are always the same at this point.
	    getline
	    if ((REQS == "0") && (FETCHES == "0")) {
		# 0 counters from targetCopy means invalid.
		# Replace with -1 which MRTG uses for reset.
		print "mrtgSmoother '$LOGFILE' zero counters:",TS,REQS,FETCHES > "/dev/fd/3"
		print "mrtgSmoother '$LOGFILE' elapsed line:",$0 > "/dev/fd/3"
		print TS,-1,-1
		print $1,0,0,0,0
	    }
	    else if (CONFCHANGED == "1") {
		print "mrtgSmoother '$LOGFILE' cfg changed. counters were:",TS,REQS,FETCHES > "/dev/fd/3"
		print "mrtgSmoother '$LOGFILE' elapsed line was:",$0 > "/dev/fd/3"
		print TS,-1,-1
		print $1,0,0,0,0
	    }
	    else if ((HASREQFETCH) && ((OLDREQS > REQS) || (OLDFETCHES > FETCHES))) {
		print "mrtgSmoother '$LOGFILE' decreased counters:",TS,REQS,FETCHES > "/dev/fd/3"
		print "mrtgSmoother '$LOGFILE' elapsed line:",$0 > "/dev/fd/3"
		print TS,-1,-1
		print $1,0,0,0,0
	    }
	    else if ((HASREQFETCH) && ($3 > ($2+100))) {
		# bad line; the elapsed fetches are greater than elapsed reqs
		#  by a significant amount
		print "mrtgSmoother '$LOGFILE' bad counters:",TS,REQS,FETCHES > "/dev/fd/3"
		print "mrtgSmoother '$LOGFILE' bad line:",$0 > "/dev/fd/3"
		print TS,-1,-1
		print $1,0,0,0,0
	    }
	    else {
		# no adjustment needed
		print "mrtgSmoother '$LOGFILE' good counters:",TS,REQS,FETCHES > "/dev/fd/3"
		print TS,REQS,FETCHES
		print "mrtgSmoother '$LOGFILE' good line:",$0 > "/dev/fd/3"
		print
	    }
	    if (getline) {
		print "mrtgSmoother '$LOGFILE' next line:",$0 > "/dev/fd/3"
		print
	    }
	}
	# print the rest of the lines as-is
	{ print }
	' $LOGFILE 3>&1 >$LOGFILE.new
    diff -u $LOGFILE $LOGFILE.new
    mv $LOGFILE.new $LOGFILE
done
