#!/bin/bash
# Probe WPAD servers and send email about changed status
# Written by Dave Dykstra October 2018

TIMEOUT=10

if [ $# = 1 ] && [ "$1" = "-t" ]; then
    shift
    CONFDIR=../conf
    STATDIR=.
    EMAIL=$LOGNAME@cern.ch
else
    CONFDIR=/home/squidmon/conf
    STATDIR=/home/squidmon/etc/wpad
    EMAIL=wlcg-squidmon-support@cern.ch
fi

if [ $# != 0 ]; then
    echo "Usage: $0 [-t]" >&2
    exit 1
fi

CONF=$CONFDIR/wpad-mon.conf
STAT=$STATDIR/wpad-mon.status

if [ ! -f $CONF ]; then
    echo "$CONF not found" >&2
    exit 2
fi

mkdir -p $STATDIR

if [ -f $STAT ]; then
    eval `cat $STAT`
fi

MAILFILE=/tmp/wpad-mon$$
trap "rm -f $MAILFILE" 0

STATUSES=""
while read STATNAME URL HOST MATCH; do
    case $STATNAME in
	\#*) continue;;
    esac
    NOW=`date +%Y-%m-%dT%H:%M:%S`
    echo "Checking $STATNAME at $NOW"
    URL="http://$URL"
    CMD="curl -s -m $TIMEOUT -H \"Host: $HOST\" \"$URL\""
    RESPONSE="`eval $CMD|cat -v`"
    if [ -z "$RESPONSE" ]; then
	# try once more if there's no output including no stdout error messages
	sleep 1
	NOW=`date +%Y-%m-%dT%H:%M:%S`
	echo "no response, trying $STATNAME again at $NOW"
	RESPONSE="`eval $CMD`"
    fi
    if [ -z "$RESPONSE" ]; then
	STATUS=NORESPONSE
    elif echo "$RESPONSE"|grep -q "$MATCH"; then
	STATUS=OK
    else
	STATUS=NOMATCH
    fi
    eval "PUBSTATUS=\$PUB$STATNAME"
    eval "LASTSTATUS=\$LAST$STATNAME"
    if [ -n "$LASTSTATUS" ] && [ "$PUBSTATUS" != "$STATUS" ]; then
	SHOWRESPONSE=true
	if [ -z "$PUBSTATUS" ]; then
	    # first time
	    PUBSTATUS=$STATUS
	    SHOWRESPONSE=false
	elif [ "$LASTSTATUS" != "$STATUS" ]; then
	    echo "$STATNAME changed from $PUBSTATUS to $STATUS, skipping because previous was $LASTSTATUS"
	else
	    echo "$STATNAME changed from $PUBSTATUS to $STATUS"
	    if [ -n "$CHANGES" ]; then
		CHANGES="$CHANGES "
	    fi
	    CHANGES="$CHANGES$STATNAME=$STATUS"

	    (
	    echo "$STATNAME changed from $PUBSTATUS to $STATUS"
	    echo "    $CMD"
	    if [ "$STATUS" = OK ]; then
		echo "Matched"
		echo "  $MATCH"
	    elif [ "$STATUS" != NORESPONSE ]; then
		echo "Expected"
		echo "  $MATCH"
		echo "but got"
		echo "$RESPONSE"
	    fi
	    echo
	    ) >>$MAILFILE

	    PUBSTATUS=$STATUS
	fi
	if $SHOWRESPONSE && [ "$STATUS" != NORESPONSE ] && [ "$STATUS" != OK ]; then
	    echo "$RESPONSE"
	fi
    fi 
    STATUSES="$STATUSES PUB$STATNAME=$PUBSTATUS LAST$STATNAME=$STATUS"

    if [ -z "$PUBSTATUS" ]; then
	# upload a good value the first time
	PUBSTATUS="$STATUS"
    fi

done <$CONF

printf '%s\n' $STATUSES >$STAT

if [ -n "$CHANGES" ]; then
    echo "Mailing $CHANGES to $EMAIL"
    mail -s "WPAD mon $CHANGES" $EMAIL <$MAILFILE
fi

echo Finished at `date +%Y-%m-%dT%H:%M:%S`
echo
