#!/usr/bin/env python3

# NOTE: This file is copied at frontiermon/scripts/awstats-rsyncd and
#   wlcgsquidmon/scripts/awstats-client.  If you change either one,
#   change both.

import sys
import os
import re
import socket
import datetime
from urllib import request
from jinja2 import Environment, FileSystemLoader

HOME = os.environ["HOME"]
if HOME == "/home/dbfrontier":
    awstats_directory = "/home/dbfrontier/scripts/awstats-rsyncd"
else:
    awstats_directory = "/home/squidmon/scripts/awstats-client"
os.chdir(awstats_directory)

http_url = "http://wlcg-squid-monitor.cern.ch/awstatsSiteProjectNodesMapping"

j2_env = Environment(loader=FileSystemLoader(awstats_directory),trim_blocks=True)
allow_table = {}
current_time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
# the get_template "include" mechanism doesn't allow absolute or 
#  ".." paths, so depend on a symlink at 'etc' pointing to '../../etc'
rsyncd_dir = 'etc/'
tmp_conf = rsyncd_dir+'tmp_rsyncd.conf'


def nodes_url(url):

 rq = request.Request(url)
 try:
     data = request.urlopen(rq, timeout=30).readlines()
 except socket.timeout:
     print("ERROR: socket timed out - URL %s" % url)
     exit(1)

 return data

lines = sorted(nodes_url(http_url))
lines = [x.decode("utf-8") for x in lines]

def create_experiment():
 
 f = open(tmp_conf, 'w+')

 seen = set()
 host = set()


 for line in range(len(lines)-2):
  columns = lines[line+2].split()
  expr_field = "[awstats-" + columns[0] + "]"
  if expr_field in seen:
   continue
  else:
   ip_group = ' '.join(map(str, allow_table[columns[0]]))
   if ip_group != "":
    f.write(j2_env.get_template('awstats_rsyncd_combined_template.conf').render(
     expr=columns[0], ip=ip_group)+"\n\n")
    seen.add(expr_field)

 f.close()

 # make sure path exists
 with open(tmp_conf, 'r') as f:
  for line in f.readlines():
   words = line.split()
   if len(words) == 3 and words[0] == 'path' and words[1] == '=':
    dir = words[2]
    if not os.path.isdir(dir):
     os.mkdir(dir)


def get_experiment_ip():
 host = set()

 for line in range(len(lines)-2):
  columns = lines[line+2].split()
  if columns[0] in host:
   continue
  else:
   host.add(columns[0])
   allow_table.setdefault(columns[0], [])

 for line in range(len(lines)-2):
  columns = lines[line+2].split()
  hostips = gethostips(columns[1])
  if hostips != []:
   allow_table[columns[0]].extend(hostips)

 for index in allow_table:
   allow_table[index] = sorted(allow_table[index])


def gethostips(host, type=None):
    ips = set()
    if type:
        types = (type,)
    else:
        types = (socket.AF_INET,
                 socket.AF_INET6)
    for t in types:
        try: 
            res = socket.getaddrinfo(host, None,
                           t, socket.SOCK_STREAM)
        except socket.error:
            continue
        nips = set([x[4][0] for x in res])
        ips.update(nips)
    return list(ips)

def create_final_conf():
 
 f = open(rsyncd_dir+"rsyncd.conf", 'w+')

 f.write(j2_env.get_template('main_rsyncd.conf').render(ctime=current_time, tmpconf=tmp_conf))
 os.remove(tmp_conf)

 
get_experiment_ip()
create_experiment()
create_final_conf()

