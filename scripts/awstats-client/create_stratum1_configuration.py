#!/usr/bin/env python3

import json
import os
import sys


def main():
    if len(sys.argv) > 1:
        if sys.argv[1] == '-t':
            config_file = "config_stratum1.json"
        else:
            print("Usage: create_stratum1_configuration.py [-t]", file=sys.stderr)
            sys.exit(1)
    else:
        config_file = "/home/squidmon/etc/config_stratum1.json"

    mapping_file = "/home/squidmon/conf/awstats/SiteProjectNodesMapping"
    groups = {}

    with open(mapping_file) as f:
        for line in f:
            values = line.split()
            if values[3] == "stratum1":
                awstats_name = values[2]
                values_length = len(values)
                if values_length < 7:
                    continue

                awstats_group = values[5]
                if awstats_group == "-":
                    awstats_group = values[1]
                if awstats_group not in groups:
                    groups[awstats_group] = {
                        "name": "%s Stratum 1" % values[0].replace("cvmfs", "").upper(),
                        "awstats": [],
                        "timezone": values[6]
                    }
                groups[awstats_group]["awstats"].append(awstats_name)

    with open(config_file + ".new", "w") as f:
        f.write(json.dumps(groups, indent=4))

    try:
        os.remove(config_file + ".old")
    except:
        pass
    try:
        os.link(config_file, config_file + ".old")
    except:
        pass
    os.rename(config_file + ".new", config_file)


if __name__ == "__main__":
    main()
