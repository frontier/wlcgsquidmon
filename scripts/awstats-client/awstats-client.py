#!/usr/bin/env python3

import sys
import re
import os
import shutil
from jinja2 import Environment, FileSystemLoader

vos = set(['atlas', 'cms', 'cvmfs', 'juno'])

## Config file template & directory to put config files
config_folder = '/home/squidmon/etc/awstats/wwwroot/cgi-bin/'
src_folder = '/home/squidmon/scripts/awstats-client/'
www_folder = '/home/squidmon/www/awstats/'
etc_folder =  '/home/squidmon/etc/'
data_folder =  '/home/squidmon/data/awstats/'
awstat_template = "awstats.template.conf"
if len(sys.argv) > 1:
  if sys.argv[1] == '-t':
    if not os.path.exists('testout'):
      os.mkdir('testout')
    config_folder = 'testout/'
    src_folder = ''
    www_folder = 'testout/'
    etc_folder = 'testout/'
    data_folder = 'testout/'
data_dirs = {}
node = open('/home/squidmon/conf/awstats/SiteProjectNodesMapping', 'r')
lines = node.readlines()
node.close()


THIS_DIR = os.path.dirname(os.path.abspath(__file__))

##Create configuration file for all listed machine in SiteProjectNodesMapping

def create_conf_file(conf_name):
  try:
    open(config_folder+conf_name, 'a').close()
    shutil.copy(src_folder+awstat_template, config_folder+conf_name)
    return conf_name
  except Exception as e:
    print("Error occurred while creating config file: " + str(e))
    sys.exit(0)

##Modify configuration file for machines individually

def modify_conf_file(awstat_conf, dns_alias, data_directory):

  f = open(src_folder+awstat_template, 'r')
  f2 = open(config_folder+awstat_conf, 'w')

  lines = f.readlines()


  for line in lines:
    if line.startswith("SiteDomain"):
      if '-0' in dns_alias:
        f2.write(line.replace("@@@SITENAME@@@", dns_alias[:-2]))
      else:
        f2.write(line.replace("@@@SITENAME@@@", dns_alias))
      continue
    if line.startswith("DirData"):
      f2.write(line.replace("@@@DataDir@@@",data_folder+data_directory+"/"+dns_alias))
    else:
      f2.write(line)

  f.close()
  f2.close()

##Modify group awstats machine configuration file

def modify_conf_group_file(conf_group, experiment, aw_group):

  f = open(src_folder+awstat_template, 'r')
  f2 = open(config_folder+conf_group, 'w')

  lines = f.readlines()

  for line in lines:
    if line.startswith("SiteDomain"):
      f2.write(line.replace("@@@SITENAME@@@", aw_group))
      continue
    if line.startswith("DirData"):
      f2.write(line.replace("@@@DataDir@@@",data_folder+experiment+"/"+aw_group))
    else:
      f2.write(line)

  f.close()
  f2.close()

# create awstats config files and modify group config files
for line in range(len(lines)-2):
  columns = lines[line+2].split()
  if len(columns) == 5:
    columns.append('-')
  machine_alias = columns[2]
  conf = machine_alias
  if machine_alias[-2] == '-' or machine_alias.endswith('-httpd') :
    # Probably one of the services on multi-service machine, ends in -N
    parent = conf[:machine_alias.rfind('-')]
    if parent in data_dirs:
      # Use data dir of the parent alias, without -N
      data_dir = data_dirs[parent]
    else:
      # This happens if they're out of order or if isn't really multi-service
      # (which can be because a manually chosen alias ends in -N)
      data_dir = columns[0]
      data_dirs[conf] = data_dir # in case this is a manually chosen -N alias
    if machine_alias[-1] == '0':
      conf = parent
  else:
    data_dir = columns[0]
    data_dirs[conf] = data_dir

  conf_file = "awstats."+conf+".conf"

  create_conf_file(conf_file)
  modify_conf_file(conf_file, machine_alias, data_dir)

  # make sure data_dir is made
  parent_dir = os.path.dirname(data_folder+data_dir)
  if not os.path.exists(data_folder+data_dir):
    os.mkdir(data_folder+data_dir)
  
  if columns[5] != "-":
    conf_file_group = "awstats."+columns[5]+".conf"
    create_conf_file(conf_file_group)
    modify_conf_group_file(conf_file_group, columns[0], columns[5])


def awstats_page_creation():

  fs = {}
  for vo in vos:
    fs[vo] = open('temp' + vo + '.html', 'w+')
  j2_env = Environment(loader=FileSystemLoader(THIS_DIR),trim_blocks=True)

  seen = set()

  for line in range(len(lines)-2):
    columns = lines[line+2].split()
    if len(columns) == 5:
      columns.append('-')

    match_expr2 = columns[0]
    f = None
    for vo in vos:
      if vo in match_expr2:
        f = fs[vo]
    if f == None:
      continue

    alias = columns[2]
    if '-0' in alias:
      alias = alias[:-2]
    if alias in seen:
      continue
    seen.add(alias)
    match_group1 = columns[5]
    if match_group1 == "-":
      f.write(j2_env.get_template('single_entry_template.html').render(
        aws=alias, dns=columns[1], mode=columns[4])+'\n')
    else:
      if match_group1 not in seen:
        f.write(j2_env.get_template('individual_combined_template.html').render(
          aws=columns[5], dns=columns[5], mode="combined"))
        seen.add(match_group1)
      f.write(j2_env.get_template('individual_combined_template.html').render(
        aws=alias, dns=columns[1], mode=columns[4]))

  for vo in vos:
    fs[vo].close()

def create_final_doc():
  fs = {}
  for vo in vos:
    fs[vo] = open(www_folder + vo + '.html', 'w+')
    os.fchmod(fs[vo].fileno(), 0o755)

  j_env = Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)
  for vo in vos:
    fs[vo].write(j_env.get_template('main_template.html').render(vo=vo))
    fs[vo].close()
    os.remove('temp' + vo + '.html')


def create_awmerge():

  f = open(etc_folder + 'awmerge_lines.sh', 'w+')
  
  awmerge_start = "\ndomerge"
  seen = set()
  for line in range(len(lines)-2):
    columns = lines[line+2].split()

    if (len(columns) == 5):
      columns.append('-')
     
    match_expr1 = columns[5]
    match_expr2 = columns[0]
    gotvo = None
    for vo in vos:
      if vo in match_expr2:
        gotvo = vo
        break
    if gotvo == None:
      continue

    match_group1 = columns[5]
    if match_group1 == '-':
      continue
    match_group2 = columns[5]
    if line+3 < len(lines):
      col_compare = lines[line+3].split()
      if (len(col_compare) == 5):
        col_compare.append('')
      match_group3 = col_compare[5]
    else:
      match_group3 = ''
    if match_group1 == match_group2:
      alias = columns[2]
      aparent = alias[:alias.rfind('-')]
      if match_group1 in seen:
        if aparent in data_dirs:
          data_dir = data_dirs[aparent]
        else:
          data_dir = columns[0]
        f.write(data_dir+'/'+alias+' ')
      else:
        seen.add(match_group1)
        destination_dir = match_expr2
        if aparent in data_dirs:
          match_expr2 = data_dirs[aparent]
        f.write(awmerge_start+' '+destination_dir+'/'+match_group1+' '+match_expr2+'/'+columns[2]+' ') 

  f.write('\n')
  f.close()


awstats_page_creation()
create_final_doc()
create_awmerge()
