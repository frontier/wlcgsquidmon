#! /usr/bin/python
import os
import json
import datetime
from bs4 import BeautifulSoup

now = datetime.datetime.now()

print "SquidVersionATLAS.py running at",now

wwwinfiles = os.path.join('/home/squidmon/www/snmpstats/mrtgall/')
sitelist = os.path.join('/home/squidmon/www/snmpstats/mrtgatlas2/')
wwwoutfiles = os.path.join('/home/squidmon/www/snmpstats/version/ATLAS/')

with open(os.path.join(sitelist, 'input.json')) as f1:
  input_json = json.load(f1)
  print "input JSON opened"

squid2list = []
squid3list = []
def versioncheck(name):
  squiddetails = []
  try:
    datafile = file(name)
    strfromfolder = str(name)[len(wwwinfiles)+1:].split('/')[0]
    for line in datafile:
      if 'time' in line:
        html = BeautifulSoup(line, 'html.parser')
        hyperlink = html.find_all('strong')
    if str(hyperlink[0])[15:29]=="frontier-squid":#if the package version contains "frontier-squid"
      if str(hyperlink[0])[30:31]=="3":#if it is squid3
        #print str(hyperlink[0])[30:-10]
        if len(strfromfolder.split('_'))==3:#site with more squids
          squiddetails.append(strfromfolder.split('_')[0])
          squiddetails.append(strfromfolder.split('_')[1]+"_"+strfromfolder.split('_')[2])
          squiddetails.append(str(hyperlink[0])[15:-10])
          squid3list.append(squiddetails)
        elif len(strfromfolder.split('_'))==2:#site with one squid
          squiddetails.append(strfromfolder.split('_')[0])
          squiddetails.append(strfromfolder.split('_')[1])
          squiddetails.append(str(hyperlink[0])[15:-10])
          squid3list.append(squiddetails)
      elif str(hyperlink[0])[30:31]=="2":#if it is squid2
        #print str(hyperlink[0])[30:-10]
        if len(strfromfolder.split('_'))==3:#site with more squids
          squiddetails.append(strfromfolder.split('_')[0])
          squiddetails.append(strfromfolder.split('_')[1]+"_"+strfromfolder.split('_')[2])
          squiddetails.append(str(hyperlink[0])[15:-10])
          squid2list.append(squiddetails)
        elif len(strfromfolder.split('_'))==2:#site with one squid
          squiddetails.append(strfromfolder.split('_')[0])
          squiddetails.append(strfromfolder.split('_')[1])
          squiddetails.append(str(hyperlink[0])[15:-10])
          squid2list.append(squiddetails)
    else:
      if str(hyperlink[0])[15:16]=="3":#if it is squid3
        #print str(hyperlink[0])[15:-10]
        if len(strfromfolder.split('_'))==3:#site with more squids
          squiddetails.append(strfromfolder.split('_')[0])
          squiddetails.append(strfromfolder.split('_')[1]+"_"+strfromfolder.split('_')[2])
          squiddetails.append(str(hyperlink[0])[15:-10])
          squid3list.append(squiddetails)
        elif len(strfromfolder.split('_'))==2:#site with one squid
          squiddetails.append(strfromfolder.split('_')[0])
          squiddetails.append(strfromfolder.split('_')[1])
          squiddetails.append(str(hyperlink[0])[15:-10])
          squid3list.append(squiddetails)
      elif str(hyperlink[0])[15:16]=="2":#if it is squid2
        #print str(hyperlink[0])[15:-10]
        if len(strfromfolder.split('_'))==3:#site with more squids
          squiddetails.append(strfromfolder.split('_')[0])
          squiddetails.append(strfromfolder.split('_')[1]+"_"+strfromfolder.split('_')[2])
          squiddetails.append(str(hyperlink[0])[15:-10])
          squid2list.append(squiddetails)
        elif len(strfromfolder.split('_'))==2:#site with one squid
          squiddetails.append(strfromfolder.split('_')[0])
          squiddetails.append(strfromfolder.split('_')[1])
          squiddetails.append(str(hyperlink[0])[15:-10])
          squid2list.append(squiddetails)
    return str(hyperlink[0])[15:-10]
  except:
    return "NotAvailable"

#############################Common part#############################
html = '<!DOCTYPE html>'
html += '<html lang="en">\n'
html += '<head>\n'
html += '<meta charset="utf-8">\n'
html += '<title>ATLAS squid version monitoring</title>\n'
html += '<style>\n'
#general style
html += '#body {\n'
html += 'margin: 0px;\n'
html += 'padding: 0px;\n'
html += '}\n'
html += '#main {\n'
html += 'overflow: auto;\n'#If overflow is clipped, a scroll-bar should be added to see the rest of the content
html += '}\n'
html += '#content {\n'
html += 'float: left;\n'
html += '}\n'
#unordered list in navigation bar
html += 'ul {\n'
html += 'list-style-type: none;\n'
html += 'margin: 0;\n'
html += 'padding: 0;\n'
html += 'overflow: hidden;\n'#to prevent li elements from going outside of the list.
html += 'background-color: #f1f1f1;\n'
html += '}\n'
#list items
html += 'li {\n'
html += 'float: left;\n'#use float to get block elements to slide next to each other
#html += 'vertical-align:middle;\n'
#html += 'line-height: 38px"\n'
html += '}\n'
#list items anchor
html += 'li a {\n'
html += 'display: block;\n'#Displaying the links as block elements makes the whole link area clickable
html += 'color: #000000;\n'#black letters
html += 'background-color: #f1f1f1;\n'#grey background
html += 'padding: 8px 16px;\n'#Since block elements take up the full width available, they cannot float next to each other.
html += 'text-decoration: none;\n'
html += '}\n'
#active page
html += 'li a.active {\n'
html += 'background-color: #00cc00;\n'
html += 'color: white;\n'
html += '}\n'
#if hovering above it, change color
html += 'li a:hover {\n'
html += 'background-color: #555555;\n'
html += 'color: white;\n'
html += '}\n'
#changing line colors in tables
html += 'tr:nth-child(even) {\n'
html += 'background-color: #dddddd;\n'
html += '}\n'
html += '</style>\n'
html += '</head>\n'
#body
html += '<body>\n'
#############################All squids#############################
allsquids = html
allsquids += '<header>\n'
allsquids += '<nav class="menu">\n'
allsquids += '<ul>\n'
allsquids += '<li style="vertical-align:middle; float:left; line-height: 32px">Listing: </li>\n'
allsquids += '<li><a class="active" href="squidversionATLAS.html">All</a></li>\n'
allsquids += '<li><a href="squidversionATLAS2.html">Squid2</a></li>\n'
allsquids += '<li><a href="squidversionATLAS3.html">Squid3</a></li>\n'
allsquids += '<li style="vertical-align:middle; float:right; line-height: 32px">Last Update: '+now.strftime("%Y-%m-%d %H:%M")+' (CERN time)</li>\n'
allsquids += '</ul>\n'
allsquids += '</nav>\n'
allsquids += '</header>\n'
#content
allsquids += '<table>\n'
allsquids += '<tr><th>site</th><th>endpoint/node</th><th>squid version</th></tr>\n'

#mrtgall folders are called sitename_endpoint
for j in input_json:
  for i in j[1]['endpoint']:
    for squidname, nmachines in i.iteritems():
      folder=j[1]['rcname']+"_"+squidname
      allsquids +='<tr> \
        <td>'+j[0]+'</td> \
        <td>'+squidname+'</td> \
        <td>'+versioncheck(wwwinfiles+'/'+folder+'/'+'proxy-hit.html')+'</td>  \
        </tr>\n'
      if nmachines>1:
        for c in range(0, nmachines):
          allsquids +='<tr> \
            <td>'+j[0]+'</td> \
            <td>'+squidname+'_'+str(c)+'</td> \
            <td>'+versioncheck(wwwinfiles+'/'+folder+'_'+str(c)+'/'+'proxy-hit.html')+'</td>  \
            </tr>\n'

allsquids += '</table>\n</body>\n</html>\n'

fs = open(os.path.join(wwwoutfiles, 'squidversionATLAS.html'),'w')
fs.write(allsquids)
fs.close()
print "Page with all ATLAS squids written"

#############################Squid 2#############################
#print "squid2:",squid2list
squid2 = html
squid2 += '<header>\n'
squid2 += '<nav class="menu">\n'
squid2 += '<ul>\n'
squid2 += '<li style="vertical-align:middle; float:left; line-height: 32px">Listing: </li>\n'
squid2 += '<li><a href="squidversionATLAS.html">All</a></li>\n'
squid2 += '<li><a class="active" href="squidversionATLAS2.html">Squid2</a></li>\n'
squid2 += '<li><a href="squidversionATLAS3.html">Squid3</a></li>\n'
squid2 += '<li style="vertical-align:middle; float:right; line-height: 32px">Last Update: '+now.strftime("%Y-%m-%d %H:%M")+' (CERN time)</li>\n'
squid2 += '</ul>\n'
squid2 += '</nav>\n'
squid2 += '</header>\n'
#content
squid2 += '<table>\n'
squid2 += '<tr><th>site</th><th>endpoint/node</th><th>squid version</th></tr>\n'

for l in squid2list:
  squid2 +='<tr> \
    <td>'+l[0]+'</td> \
    <td>'+l[1]+'</td> \
    <td>'+l[2]+'</td>  \
    </tr>\n'

squid2 += '</table>\n</body>\n</html>\n'

fs = open(os.path.join(wwwoutfiles, 'squidversionATLAS2.html'),'w')
fs.write(squid2)
fs.close()
print "Page with all ATLAS squid 2 written"

#############################Squid 3#############################
#print "squid3:",squid3list
squid3 = html
squid3 += '<header>\n'
squid3 += '<nav class="menu">\n'
squid3 += '<ul>\n'
squid3 += '<li style="vertical-align:middle; float:left; line-height: 32px">Listing: </li>\n'
squid3 += '<li><a href="squidversionATLAS.html">All</a></li>\n'
squid3 += '<li><a href="squidversionATLAS2.html">Squid2</a></li>\n'
squid3 += '<li><a class="active" href="squidversionATLAS3.html">Squid3</a></li>\n'
squid3 += '<li style="vertical-align:middle; float:right; line-height: 32px">Last Update: '+now.strftime("%Y-%m-%d %H:%M")+' (CERN time)</li>\n'
squid3 += '</ul>\n'
squid3 += '</nav>\n'
squid3 += '</header>\n'
#content
squid3 += '<table>\n'
squid3 += '<tr><th>site</th><th>endpoint/node</th><th>squid version</th></tr>\n'

for l in squid3list:
  squid3 +='<tr> \
    <td>'+l[0]+'</td> \
    <td>'+l[1]+'</td> \
    <td>'+l[2]+'</td>  \
    </tr>\n'

squid3 += '</table>\n</body>\n</html>\n'

fs = open(os.path.join(wwwoutfiles, 'squidversionATLAS3.html'),'w')
fs.write(squid3)
fs.close()
print "Page with all ATLAS squid 3 written"
print ""
