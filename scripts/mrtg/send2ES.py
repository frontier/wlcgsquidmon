#! /usr/bin/python3
import os
import json
from datetime import datetime, timezone
import requests
import pprint

now = datetime.now(tz=timezone.utc)

print('Elastic Search extraction script started: '+str(now)+' UTC')

if os.path.exists('/home/squidmon/'):
  data = '/home/squidmon/www/snmpstats/mrtgall/'
  cms = '/home/squidmon/www/cms-squids.json'
  atlas = '/cvmfs/atlas.cern.ch/repo/sw/local/etc/cric_sites.json'
else:
  data = 'mrtgall/'
  cms = 'cms-squids.json'
  atlas = 'cric_sites.json'

with open(atlas) as fa:
  atlas_dict = json.load(fa)

atlas_list = []
for a in list(atlas_dict):
  if 'squid' in list(atlas_dict[a]['fsconf']):
    for i in atlas_dict[a]['fsconf']['squid']:
      atlas_list.append(i[0].split('/')[2].split(':')[0])
      for j in i[1]:
        atlas_list.append(j.split('/')[2].split(':')[0])
#print(atlas_list)

with open(cms) as fc:
  cms_dict = json.load(fc)

cms_list = []
for c in list(cms_dict):
  #print(c)
  cms_list.append(c)
#print(cms_list)


logs = ['proxy-cpu', 'proxy-descriptors', 'proxy-hit', 'proxy-obj', 'proxy-srvkbinout']
names = {}
names['proxy-cpu'] = {'in': 'Cpu time','out': ''}
names['proxy-descriptors'] = {'in': 'Available' ,'out': 'Used'}
names['proxy-hit'] = {'in': 'HTTP reqs','out': 'HTTP fetches'}
names['proxy-obj'] = {'in': 'Obj num','out': ''}
names['proxy-srvkbinout'] = {'in': 'Total','out': 'Fetches'}
send2ES = []
for i in sorted(os.listdir(data)):
  experiment = []
  tempdict = {}
  if os.path.isdir(data+i):
    nameList = i.split('_')
    if nameList[-1].isnumeric():#if the last character is number, i.e. if it is one of aliased machines
      squid = nameList[-2]+'_'+nameList[-1]
      site = i.replace(squid,'')[:-1]
    else:
      squid = nameList[-1]
      site = i.replace(squid,'')[:-1]
    if squid in atlas_list:
      experiment.append('atlas')
    if squid in cms_list:
      experiment.append('cms')
    if len(experiment) == 0:
      experiment.append('other')
    tempdict['experiment'] = experiment
    tempdict['site'] = site
    tempdict['squid'] = squid
    for l in logs:
      ppath = data+i+'/'+l+'.log'
      #print(ppath)
      with open(ppath) as f:
        lines = f.readlines()
        values = lines[3].strip('\n').split(' ')
        tempdict['timestamp'] = int(values[0])
        #https://linux.die.net/man/1/mrtg-logfile
        tempdict[names[l]['in']] = int(values[1])
        if len(names[l]['out']) > 0:
          tempdict[names[l]['out']] = int(values[2])
    #print(tempdict)
    send2ES.append(tempdict)
#pprint.pprint(send2ES)
#print(send2ES)

if os.path.exists('/home/squidmon/'):
  newHeaders = {'Content-type': 'application/json', 'Accept': 'text/plain'}
  response = requests.post('https://squid.atlas-ml.org', json={'token': 'sqKHBKJB45n', 'data':send2ES}, headers=newHeaders, verify=False)
  print("Status code: ", response.status_code)

print('Elastic Search extraction script finished: '+str(datetime.now(tz=timezone.utc))+' UTC')
