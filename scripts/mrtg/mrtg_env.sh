#!/bin/bash

export LANG=C
export TZ=UTC

export MRTG_BASE_DIR=/home/squidmon/scripts/mrtg
export MRTG_BIN_DIR=${MRTG_BASE_DIR}/bin
export MRTG_CFG_DIR=/home/squidmon/etc/mrtg
export MRTG_LOG_DIR=/home/squidmon/logs/mrtg

export MRTG_ATLAS_DIR=${MRTG_BASE_DIR}/atlas

export SQUID_STATS_DIR=/home/squidmon/www/snmpstats

