#! /usr/bin/python3
import os
import json
from datetime import datetime, timedelta, timezone
from bs4 import BeautifulSoup
import glob

def tableLine(site,squid,path):
  """
  return '<tr> \
          <td><a href="'+wwwoutlinks+'/'+'indexatlas2site'+site+'.html'+'">'+site+'</a></td> \
          <td>'+squid+'</td> \
          <td><a href="'+path+'/'+'proxy-hit.html'+'"><img src="'+path+'/'+'proxy-hit-day.png" width="300" style=border-style: none></a></td>  \
          <td><a href="'+path+'/'+'proxy-srvkbinout.html'+'"><img src="'+path+'/'+'proxy-srvkbinout-day.png" width="300" style=border-style: none></a></td> \
          <td><a href="'+path+'/'+'proxy-obj.html'+'"><img src="'+path+'/'+'proxy-obj-day.png" width="300" style=border-style: none>  </a> </td> \
          <td><a href="'+path+'/'+'proxy-cpu.html'+'"><img src="'+path+'/'+'proxy-cpu-day.png" width="300" style=border-style: none>  </a> </td> \
          <td><a href="'+path+'/'+'proxy-descriptors.html'+'"><img src="'+path+'/'+'proxy-descriptors-day.png" width="300" style=border-style: none>  </a> </td> \
          </tr>\n'
  """
  tableLineString = indent[2]+'<tr>\n'
  tableLineString += indent[3]+'<td><a href="'+wwwoutlinks+'/'+'indexatlas2site'+site+'.html'+'">'+site+'</a></td>\n'
  tableLineString += indent[3]+'<td>'+squid+'</td>\n'
  tableLineString += indent[3]+'<td><a href="'+path+'/'+'proxy-hit.html'+'"><img src="'+path+'/'+'proxy-hit-day.png" width="300" style=border-style: none></a></td>\n'
  tableLineString += indent[3]+'<td><a href="'+path+'/'+'proxy-srvkbinout.html'+'"><img src="'+path+'/'+'proxy-srvkbinout-day.png" width="300" style=border-style: none></a></td>\n'
  tableLineString += indent[3]+'<td><a href="'+path+'/'+'proxy-obj.html'+'"><img src="'+path+'/'+'proxy-obj-day.png" width="300" style=border-style: none></a></td>\n'
  tableLineString += indent[3]+'<td><a href="'+path+'/'+'proxy-cpu.html'+'"><img src="'+path+'/'+'proxy-cpu-day.png" width="300" style=border-style: none></a></td>\n'
  tableLineString += indent[3]+'<td><a href="'+path+'/'+'proxy-descriptors.html'+'"><img src="'+path+'/'+'proxy-descriptors-day.png" width="300" style=border-style: none></a></td>\n'
  tableLineString += indent[2]+'</tr>\n'
  return tableLineString

now = datetime.now(tz=timezone.utc)
timeFormat = '%Y-%m-%d %H:%M'

print('ATLAS MRTG page building script started: '+str(datetime.now(tz=timezone.utc))+' UTC')

basepath = os.path.dirname(os.path.abspath(__file__))
if os.path.exists('/home/squidmon/'):
  wwwpathfiles = '/home/squidmon/www/snmpstats/mrtgall'
  wwwpathlinks = 'http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgall'
  wwwoutfiles = '/home/squidmon/www/snmpstats/mrtgatlas2'
  wwwoutlinks = 'http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgatlas2'
else:
  wwwpathfiles = os.path.join(basepath, 'mrtgall')
  wwwpathlinks = os.path.join(basepath, 'mrtgall')
  wwwoutfiles = os.path.join(basepath, 'www')
  wwwoutlinks = os.path.join(basepath, 'www')

#First, copy the existing page and set some elements red. So, if the creation of the new version crashes, it would be noticeable.
input_files=glob.glob(wwwoutfiles+'/*.html')
for file_name in input_files:
  with open(file_name, 'r') as of:
    htmltext = BeautifulSoup(of, "html.parser")
    li = htmltext.find_all('li')
    for l in li:
      if 'Start of the last update' in l.string:
        #if the last update is older than 4 hour - otherwise, when the page is opened while the script is running, it is red even though nothing failed
        if now - timedelta(hours=4) > datetime.strptime(l.string.split('=')[1], timeFormat).replace(tzinfo=timezone.utc):
          l['style'] = 'vertical-align:middle; float:right; line-height: 32px;background-color: red;'
  with open(file_name, 'w') as fs:
    fs.write(str(htmltext))
    
indent = []
stringg = ''
for r in range(0,20):
  stringg += '  '
  indent.append(stringg)

with open(os.path.join(wwwoutfiles, 'input.json')) as f1:
  input_json = json.load(f1)

#############################Common part#############################
html = '<!DOCTYPE html>'
html += '<html lang="en">\n'
html += indent[0]+'<head>\n'
html += indent[1]+'<meta charset="utf-8">\n'
html += indent[1]+'<title>ATLAS MRTG monitoring</title>\n'
html += indent[1]+'<style>\n'
#general style
html += indent[2]+'#body {\n'
html += indent[3]+'margin: 0px;\n'
html += indent[3]+'padding: 0px;\n'
html += indent[2]+'}\n'
html += indent[2]+'#main {\n'
html += indent[3]+'overflow: auto;\n'#If overflow is clipped, a scroll-bar should be added to see the rest of the content
html += indent[2]+'}\n'
html += indent[2]+'#content {\n'
html += indent[3]+'float: left;\n'
html += indent[2]+'}\n'
#unordered list in navigation bar
html += indent[2]+'ul {\n'
html += indent[3]+'list-style-type: none;\n'
html += indent[3]+'margin: 0;\n'
html += indent[3]+'padding: 0;\n'
html += indent[3]+'overflow: hidden;\n'#to prevent li elements from going outside of the list.
html += indent[3]+'background-color: #f1f1f1;\n'
html += indent[2]+'}\n'
#list items
html += indent[2]+'li {\n'
html += indent[3]+'float: left;\n'#use float to get block elements to slide next to each other
html += indent[2]+'}\n'
#list items anchor
html += indent[2]+'li a {\n'
html += indent[3]+'display: block;\n'#Displaying the links as block elements makes the whole link area clickable
html += indent[3]+'color: #000000;\n'#black letters
html += indent[3]+'background-color: #f1f1f1;\n'#grey background
html += indent[3]+'padding: 8px 16px;\n'#Since block elements take up the full width available, they cannot float next to each other.
html += indent[3]+'text-decoration: none;\n'
html += indent[2]+'}\n'
#active page
html += indent[2]+'li a.active {\n'
html += indent[3]+'background-color: #00cc00;\n'
html += indent[3]+'color: white;\n'
html += indent[2]+'}\n'
#if hovering above it, change color
html += indent[2]+'li a:hover {\n'
html += indent[3]+'background-color: #555555;\n'
html += indent[3]+'color: white;\n'
html += indent[2]+'}\n'
#changing line colors in tables
html += indent[2]+'tr:nth-child(even) {\n'
html += indent[3]+'background-color: #dddddd;\n'
html += indent[2]+'}\n'
html += indent[1]+'</style>\n'
html += indent[0]+'</head>\n'
#body
html += indent[0]+'<body>\n'
html += indent[1]+'Note: Clicking on the plot gives detail page. Page updates: plots every 5 minutes, list of sites every 3 hours\n'

#############################All squids#############################

allsquids = html
allsquids += indent[1]+'<header>\n'
allsquids += indent[2]+'<nav class="menu">\n'
allsquids += indent[3]+'<ul>\n'
allsquids += indent[4]+'<li style="vertical-align:middle; float:left; line-height: 32px">Listing: </li>\n'
allsquids += indent[4]+'<li><a class="active" href="indexatlas2.html">All</a></li>\n'
allsquids += indent[4]+'<li><a href="indexatlas2tier1.html">Tier</a></li>\n'
allsquids += indent[4]+'<li><a href="indexatlas2cloudCERN.html">Cloud</a></li>\n'
allsquids += indent[4]+'<li><a href="indexatlas2siteCERN-PROD.html">Site</a></li>\n'
allsquids += indent[4]+'<li style="vertical-align:middle; float:right; line-height: 32px">Start of the last update (UTC)='+now.strftime(timeFormat)+'</li>\n'
allsquids += indent[3]+'</ul>\n'
allsquids += indent[2]+'</nav>\n'
allsquids += indent[1]+'</header>\n'
#content
allsquids += indent[1]+'<table>\n'
allsquids += indent[2]+'<tr><th>site</th><th>endpoint/node</th><th>Request/Fetch</th><th>In/Out</th><th>Object count</th><th>CPU time</th><th>file descriptor usage</th></tr>\n'

#mrtgall folders are called sitename_endpoint
for j in input_json:
  for i in j[1]['endpoint']:
    for squidname in i.keys():
      folder=j[1]['rcname']+"_"+squidname
      allsquids += tableLine(j[0],squidname,wwwpathlinks+'/'+folder)
      if i[squidname]>1:
        for c in range(0, i[squidname]):
          allsquids += tableLine(j[0],squidname+'_'+str(c),wwwpathlinks+'/'+folder+'_'+str(c)) 

allsquids += indent[1]+'</table>\n'
allsquids += indent[0]+'</body>\n</html>\n'

fs = open(os.path.join(wwwoutfiles, 'indexatlas2.html'),'w')
fs.write(allsquids)
fs.close()
print('Page with all ATLAS squids written')

#############################Sites#############################

sitelists = []
for j in input_json:
  sitelists.append(j[0])

for sitelabel in sitelists:
  sites = html
  sites += indent[1]+'<header>\n'
  sites += indent[2]+'<nav class="menu">\n'
  sites += indent[3]+'<ul>\n'
  sites += indent[4]+'<li style="vertical-align:middle; float:left; line-height: 32px">Listing: </li>\n'
  sites += indent[4]+'<li><a href="indexatlas2.html">All</a></li>\n'
  sites += indent[4]+'<li><a href="indexatlas2tier1.html">Tier</a></li>\n'
  sites += indent[4]+'<li><a href="indexatlas2cloudCERN.html">Cloud</a></li>\n'
  sites += indent[4]+'<li><a  class="active" href="indexatlas2siteCERN-PROD.html">Site</a></li>\n'
  sites += indent[4]+'<li style="vertical-align:middle; float:right; line-height: 32px">Start of the last update (UTC)='+now.strftime(timeFormat)+'</li>\n'
  sites += indent[3]+'</ul>\n'
  sites += indent[2]+'</nav>\n'
  sites += indent[2]+'<nav class="menu">\n'
  sites += indent[3]+'<ul>\n'
  sites += indent[4]+'<li style="vertical-align:middle; float:left; line-height: 32px">Site: </li>\n'
  for s in sitelists:
    if s == sitelabel:
      sites += indent[4]+'<li><a class="active" href="indexatlas2site'+s+'.html">'+s+'</a></li>\n'
    else:
      sites += indent[4]+'<li><a href="indexatlas2site'+s+'.html">'+s+'</a></li>\n'
  sites += indent[3]+'</ul>\n'
  sites += indent[2]+'</nav>\n'
  sites += indent[1]+'</header>\n'
  #content
  sites += indent[1]+'<table>\n'
  sites += indent[2]+'<tr><th>site</th><th>endpoint/node</th><th>Request/Fetch</th><th>In/Out</th><th>Object count</th><th>CPU time</th><th>file descriptor usage</th></tr>\n'
  for j in input_json:
    for i in j[1]['endpoint']:
      for squidname in i.keys():
        folder=j[1]['rcname']+"_"+squidname
        if j[0] == sitelabel:
          if i[squidname] == 1:
            sites += tableLine(j[0],squidname,wwwpathlinks+'/'+folder)
          else:
            for c in range(0, i[squidname]):
              sites += tableLine(j[0],squidname+'_'+str(c),wwwpathlinks+'/'+folder+'_'+str(c))
  sites += indent[1]+'</table>\n'
  sites += indent[0]+'</body>\n</html>\n'
  fs = open(os.path.join(wwwoutfiles, 'indexatlas2site'+sitelabel+'.html'),'w')
  fs.write(sites)
  fs.close()
  print('Page with '+sitelabel+' squid(s) written')
  

#############################Tiers#############################

for tierlabel in range(0,4):
  tiers = html
  tiers += indent[1]+'<header>\n'
  tiers += indent[2]+'<nav class="menu">\n'
  tiers += indent[3]+'<ul>\n'
  tiers += indent[4]+'<li style="vertical-align:middle; float:left; line-height: 32px">Listing: </li>\n'
  tiers += indent[4]+'<li><a href="indexatlas2.html">All</a></li>\n'
  tiers += indent[4]+'<li><a class="active" href="indexatlas2tier1.html">Tier</a></li>\n'
  tiers += indent[4]+'<li><a href="indexatlas2cloudCERN.html">Cloud</a></li>\n'
  tiers += indent[4]+'<li><a href="indexatlas2siteCERN-PROD.html">Site</a></li>\n'
  tiers += indent[4]+'<li style="vertical-align:middle; float:right; line-height: 32px">Start of the last update (UTC)='+now.strftime(timeFormat)+'</li>'
  tiers += indent[3]+'</ul>\n'
  tiers += indent[2]+'</nav>\n'
  tiers += indent[2]+'<nav class="menu">\n'
  tiers += indent[3]+'<ul>\n'
  tiers += indent[4]+'<li style="vertical-align:middle; float:left; line-height: 32px">Tier: </li>\n'
  for t in range(0,4):
    if tierlabel == t:
      tiers += indent[4]+'<li><a class="active" href="indexatlas2tier'+str(t)+'.html">T'+str(t)+'</a></li>\n'
    else:
      tiers += indent[4]+'<li><a href="indexatlas2tier'+str(t)+'.html">T'+str(t)+'</a></li>\n'
  tiers += indent[3]+'</ul>\n'
  tiers += indent[2]+'</nav>\n'
  tiers += indent[1]+'</header>\n'
  tiers += indent[1]+'<table>\n'
  tiers += indent[2]+'<tr><th>site</th><th>endpoint/node</th><th>Request/Fetch</th><th>In/Out</th><th>Object count</th><th>CPU time</th><th>file descriptor usage</th></tr>\n'
  for j in input_json:
    for i in j[1]['endpoint']:
      for squidname in i.keys():
        folder=j[1]['rcname']+"_"+squidname
        if j[1]['tier'] == tierlabel:
          if i[squidname] == 1:
            tiers += tableLine(j[0],squidname,wwwpathlinks+'/'+folder)
          else:
            for c in range(0, i[squidname]):
              tiers += tableLine(j[0],squidname+'_'+str(c),wwwpathlinks+'/'+folder+'_'+str(c))
  tiers += indent[1]+'</table>\n'
  tiers += indent[0]+'</body>\n</html>\n'

  ft = open(os.path.join(wwwoutfiles, 'indexatlas2tier'+str(tierlabel)+'.html'),'w')
  ft.write(tiers)
  ft.close()
  print('Page with all tier '+str(tierlabel)+' squids written')

#############################Clouds#############################
cloudlists = []

for j in input_json:
  cloudlists.append(j[1]['cloud'])
cloudlist = sorted(list(set(cloudlists)))

for cloudlabel in cloudlist:
  clouds = html
  clouds += indent[1]+'<header>\n'
  clouds += indent[2]+'<nav class="menu">\n'
  clouds += indent[3]+'<ul>\n'
  clouds += indent[4]+'<li style="vertical-align:middle; float:left; line-height: 32px">Listing: </li>\n'
  clouds += indent[4]+'<li><a href="indexatlas2.html">All</a></li>\n'
  clouds += indent[4]+'<li><a href="indexatlas2tier1.html">Tier</a></li>\n'
  clouds += indent[4]+'<li><a class="active" href="indexatlas2cloudCERN.html">Cloud</a></li>\n'
  clouds += indent[4]+'<li><a href="indexatlas2siteCERN-PROD.html">Site</a></li>\n'
  clouds += indent[4]+'<li style="vertical-align:middle; float:right; line-height: 32px">Start of the last update (UTC)='+now.strftime(timeFormat)+'</li>'
  clouds += indent[3]+'</ul>\n'
  clouds += indent[2]+'</nav>\n'
  clouds += indent[2]+'<nav class="menu">\n'
  clouds += indent[3]+'<ul>\n'
  clouds += indent[4]+'<li style="vertical-align:middle; float:left; line-height: 32px">Cloud: </li>\n'
  for c in cloudlist:
    if cloudlabel == c:
      clouds += indent[4]+'<li><a class="active" href="indexatlas2cloud'+c+'.html">'+c+'</a></li>\n'
    else:
      clouds += indent[4]+'<li><a href="indexatlas2cloud'+c+'.html">'+c+'</a></li>\n'
  clouds += indent[3]+'</ul>\n'
  clouds += indent[2]+'</nav>\n'
  clouds += indent[1]+'</header>\n'
  clouds += indent[1]+'<table>\n'
  clouds += indent[2]+'<tr><th>site</th><th>endpoint/node</th><th>Request/Fetch</th><th>In/Out</th><th>Object count</th><th>CPU time</th><th>file descriptor usage</th></tr>\n'
  for j in input_json:
    for i in j[1]['endpoint']:
      for squidname in i.keys():
        folder=j[1]['rcname']+"_"+squidname
        if j[1]['cloud'] == cloudlabel:
          if i[squidname] == 1:
            clouds += tableLine(j[0],squidname,wwwpathlinks+'/'+folder)
          else:
            for c in range(0, i[squidname]):
              clouds += tableLine(j[0],squidname+'_'+str(c),wwwpathlinks+'/'+folder+'_'+str(c))
  clouds += indent[1]+'</table>\n'
  clouds += indent[0]+'</body>\n</html>\n'

  fc = open(os.path.join(wwwoutfiles, 'indexatlas2cloud'+cloudlabel+'.html'),'w')
  fc.write(clouds)
  fc.close()
  print('Page with all '+cloudlabel+' cloud squids written')

print('ATLAS MRTG page building script finished: '+str(datetime.now(tz=timezone.utc))+' UTC')

if os.path.exists('/home/squidmon/'):
  #extra output file for crash monitoring tool
  etc_grid_service_dir = "/home/squidmon/etc/grid-services/"
  if not os.path.exists(etc_grid_service_dir):
    os.makedirs(etc_grid_service_dir)
  f = open(etc_grid_service_dir + "PageBuilder.out", "w")
  f.write("PageBuilder.py script execution finished succesfully!")
  f.close()
