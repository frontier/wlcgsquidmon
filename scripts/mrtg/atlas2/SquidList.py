#! /usr/bin/python3
import os
import json
from datetime import datetime, timezone
import pprint

now = datetime.now(tz=timezone.utc)

print('MRTG: ATLAS squid listing script started: '+str(now)+' UTC')

if os.path.exists('/home/squidmon/'):
  wwwout = '/home/squidmon/www/snmpstats/mrtgatlas2'
  allpath = '/home/squidmon/www/'
  excpath = '/home/squidmon/conf/exceptions/'
else:
  wwwout = 'www/'
  allpath = 'JSONs'
  excpath = ''
  
#CRIC list
#from /home/squidmon/www/cric.json
with open(os.path.join(allpath,'cric.json')) as f1:#list of dictionaries
  cric_dump = json.load(f1)
#pprint.pprint(cric_dump)

#GOCBD+OIM list
#from /home/squidmon/www/grid-squids.json
with open(os.path.join(allpath,'grid-squids.json')) as f2:#dictionary of dictionaries: {squidname:{ipranges:, ips:, name:, otherips:, source:}}
  all_dump = json.load(f2)
#pprint.pprint(all_dump)
allSites = {}#content of grid-squids.json as per-site structure (instead of per-squid)
for squidname in all_dump.keys():#
  if all_dump[squidname]['name'] not in allSites.keys():#is the site is not in the new dict
    allSites[all_dump[squidname]['name']] = {}
    allSites[all_dump[squidname]['name']][squidname] = {}
    allSites[all_dump[squidname]['name']][squidname]['ips'] = all_dump[squidname]['ips']
    if 'monitored' in all_dump[squidname].keys():
      allSites[all_dump[squidname]['name']][squidname]['monitored'] = all_dump[squidname]['monitored']
  else:
    allSites[all_dump[squidname]['name']][squidname] = {}
    allSites[all_dump[squidname]['name']][squidname]['ips'] = all_dump[squidname]['ips']
    if 'monitored' in all_dump[squidname].keys():
      allSites[all_dump[squidname]['name']][squidname]['monitored'] = all_dump[squidname]['monitored']
#pprint.pprint(allSites)

#checking if the content of all_dump and allSites is the same
#siteCounter = 0
#for s in allSites.keys():
#  siteCounter += len(allSites[s])
#print(len(all_dump))
#print(siteCounter)

hit = {}#dictionary of dictionaries:{sitename:{cloud:, rcname:, tier:, endpoint:[{squidname:Nmachines}]}}
for cricsite in cric_dump:#cricsite is a dictionary with all info for a site
  site_properties = {}
  sitename = cricsite['name'] #site name
  rcsitename = cricsite['rc_site'] #this is the OIM/GOCDB site name (RegionalCenter sitename) (http://www.usatlas.bnl.gov/~caballer/files/agis_attributes)
  sitestatus = cricsite['state'] #site status
  squidinfo = []
  if len(cricsite['fsconf']) > 0 and 'squid' in cricsite['fsconf'].keys():
    squidinfo = cricsite['fsconf']['squid'] #part with squid informations
  
  squids = []
  nodes = []
  for i in squidinfo:
    squids.append(i[0][7:].split(":")[0]) #squids (with 'http://' and port removed)
    for j in i[1]:
      nodes.append({j[7:].split(":")[0]:i[1][j]}) #nodes of squid (with 'http://' and port removed)

  if sitestatus == 'ACTIVE':
    if sitename in allSites.keys():#if site name from CRIC is amongst sites in all page
      site_properties['tier'] = cricsite["tier_level"]
      site_properties['cloud'] = cricsite["cloud"]
      site_properties['rcname'] = rcsitename
      endpoints = []
      for s in squids:#looping over CRIC squids at given site
        endpointpair = {}
        if s in allSites[sitename].keys():#if squid name from all page matches squid name from CRIC
          if 'monitored' not in allSites[sitename][s].keys():#monitored: true does not appear in the grid-squids.json - this is to ignore squids for which there are no mrtg pages
            endpointpair[s] = len(allSites[sitename][s]["ips"])#dictionary showing squid name and number of its machines
            endpoints.append(endpointpair)
      for n in nodes:#looping over all nodes at given site
        for sq in n.keys():
          if n[sq] == 'ACTIVE':
            endpointpair = {}
            if sq in allSites[sitename].keys():#if squid name from all page matches squid name from CRIC
              if 'monitored' not in allSites[sitename][sq].keys():#monitored: true does not appear in the grid-squids.json - this is to ignore squids for which there are no mrtg pages
                if sq not in [list(k)[0] for k in endpoints]:#if the squid is already in the endpoints list, do not add it to avoid duplicates
                  endpointpair[sq] = len(allSites[sitename][sq]["ips"])#dictionary showing squid name and number of its machines
                  endpoints.append(endpointpair)
      if len(endpoints) > 0:#only sites that have some squids go into the output
        site_properties['endpoint'] = endpoints
        hit[sitename]=site_properties
    elif rcsitename in allSites.keys():#if rc site name from CRIC is amongst sites in all page
      site_properties['tier'] = cricsite["tier_level"]
      site_properties['cloud'] = cricsite["cloud"]
      site_properties['rcname'] = rcsitename
      endpoints = []
      for s in squids:#looping over CRIC squids at given site
        endpointpair = {}
        if s in allSites[rcsitename].keys():#if squid name from all page matches squid name from CRIC
          if 'monitored' not in allSites[rcsitename][s].keys():#monitored: true does not appear in the grid-squids.json - this is to ignore squids for which there are no mrtg pages
            endpointpair[s] = len(allSites[rcsitename][s]["ips"])#dictionary showing squid name and number of its machines
            endpoints.append(endpointpair)
      for n in nodes:#looping over all nodes at given site
        for sq in n.keys():
          if n[sq] == 'ACTIVE':
            endpointpair = {}
            if sq in allSites[rcsitename].keys():#if squid name from all page matches squid name from CRIC
              if 'monitored' not in allSites[rcsitename][sq].keys():#monitored: true does not appear in the grid-squids.json - this is to ignore squids for which there are no mrtg pages
                if sq not in [list(k)[0] for k in endpoints]:#if the squid is already in the endpoints list, do not add it to avoid duplicates
                  endpointpair[sq] = len(allSites[rcsitename][sq]["ips"])#dictionary showing squid name and number of its machines
                  endpoints.append(endpointpair)
      if len(endpoints) > 0:#only sites that have some squids go into the output
        site_properties['endpoint'] = endpoints
        hit[sitename]=site_properties 
#pprint.pprint(hit)

#exceptions file
exceptionsAdd = {}
exceptionsRemove = {}
#exception file info: add/remove;sitename;rc_sitename;squidnamelist;tier;cloud
with open(os.path.join(excpath,'mrtgatlas2exceptions.txt')) as f3:
  for x in f3.readlines():
    endpoints  = []
    site_properties = {}
    xx = x.strip('\n').split(';')
    if not xx[0].startswith("#"):#skip comment lines
      if xx[0] == '+':#if the line starts with +, add the line
        squidString = xx[3]
        squidList = []
        for r in squidString.split(','):
          if r in all_dump.keys():
            squidDict = {}
            squidDict[r] = len(all_dump[r]['ips'])
            squidList.append(squidDict)
          else:
            print('Trying to add '+r+' but it is not in grid-squids.json/all page')
        exceptionsAdd[xx[1]] = {'tier':xx[4], 'rcname':xx[2], 'endpoint':squidList, 'cloud':xx[5]}
      if xx[0] == '-':#if the line starts with -, remove the line
        exceptionsRemove[xx[1]] = xx[3].split(',')#just a site:squid list dict
#pprint.pprint(exceptionsAdd)
#pprint.pprint(exceptionsRemove)
        
        
#making final output structure
sitedict = {}
#removing squids/sites
for f in hit:#hit is dictionary of dictionaries:{sitename:{cloud:, rcname:, tier:, endpoint:[{squidname:Nmachines}]}}
  if f in exceptionsRemove.keys():#if the site name is on the exception list
    tempdict = {}
    templist = []
    for h in hit[f].keys():#copy all info except for endpoint
      if h == 'endpoint':
        for l in hit[f][h]:#run over all squid dicts defined in endpoint
          if list(l)[0] not in exceptionsRemove[f]:#include only squid dictionaries which are not in remove dict
            templist.append(l)
        tempdict['endpoint'] = templist
      else:
        tempdict[h] = hit[f][h]
    if len(tempdict['endpoint']) > 0:#if there are squids defined for the site then include it into the output
      sitedict[f] = tempdict
  else:#if the site is not on the exception list then just put it in the output
    sitedict[f] = hit[f]

#add squids/sites
for i in exceptionsAdd.keys():
  if i in list(sitedict):
    for h in sitedict[i].keys():
      if h == 'endpoint':
        for k in exceptionsAdd[i][h]:
          sitedict[i][h].append(k)#attach the new squid into endpoint list
  else:#if a new site is added, just put it in the dictionary
    sitedict[i] = exceptionsAdd[i]
#pprint.pprint(sitedict)

#sorting endpoints
sortedsitedict = {}
for s in sorted(sitedict.keys()):
  sortedsitedict[s] = {}
  for k in sitedict[s].keys():
    if k == 'endpoint':
      sortedsitedict[s][k] = sorted(sitedict[s][k], key = lambda x:x.keys())#sort the list of dictionaries by key (list(dict) returns list of keys)
    else:
      sortedsitedict[s][k] = sitedict[s][k]
#pprint.pprint(sortedsitedict)

print("sites/squids matched between GOCDB/OIM and CRIC:\n")
outList = []
for f in sorted(sortedsitedict, key=str.casefold):
  outList.append([f, sortedsitedict[f]])
print(outList)

with open(os.path.join(wwwout,'input.json'), 'w') as outfile:
  json.dump(outList, outfile)
print('MRTG: ATLAS squid listing script finished: '+str(datetime.now(tz=timezone.utc))+' UTC')
