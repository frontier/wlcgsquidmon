#! /usr/bin/python
import os
import json
from datetime import datetime, timedelta
import csv
import calendar
import urllib2
#import pprint

print "script started:", datetime.now()
#csvpath = 'csv/'
#csvlink = 'https://www.fzu.cz/~svatosm/csv/'
csvpath = '/home/squidmon/www/snmpstats/csv/'
csvlink = 'http://wlcg-squid-monitor.cern.ch/snmpstats/csv/'

indent = []
stringg = ''
for r in range(0,10):
  stringg += '  '
  indent.append(stringg)

times = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
h = int(datetime.utcnow().strftime("%H"))
#h = 19
timeline = times[h:]+times[:h]+[h]
now = int(calendar.timegm(datetime.utcnow().utctimetuple()))
#now = int(calendar.timegm(datetime.strptime("2018-12-08 19:20", "%Y-%m-%d %H:%M").utctimetuple()))

#downtime
#http://atlas-agis-api.cern.ch/request/downtime/query/list/?json&newapi=1
#/afs/cern.ch/user/a/atagadm/public/ddmblacklisting/cache/downtimes.json
downDict = {}

#with open("downtime.json") as f:
  #downtimes = json.load(f)

try:
  responseD = urllib2.urlopen("http://atlas-agis-api.cern.ch/request/downtime/query/list/?json&newapi=1")
  #responseD = urllib2.urlopen("file:///afs/cern.ch/user/a/atagadm/public/ddmblacklisting/cache/downtimes.json")
  #responseD = urllib2.urlopen("file:///home/svatos/cernbox/Computing/frontierOperations/squid/squids-SSBreplacement/downtimes.json")
  downtimes = json.load(responseD)
except:
  downtimes = {}

for downsites, downsdetails in downtimes.iteritems():
  for downdetail in downsdetails:
    if "org.squid-cache.Squid" in downdetail["affected_services"]:
      if int(calendar.timegm(datetime.strptime(downdetail["start_time"], "%Y-%m-%dT%H:%M:%S").utctimetuple())) < now:#after the beginning of the downtime
        if now < int(calendar.timegm(datetime.strptime(downdetail["end_time"], "%Y-%m-%dT%H:%M:%S").utctimetuple())):#before the end of the downtime
          downDict[downsites] = downdetail["info_url"]

sitesDict = {}#dict containing all and adcos lists
sitelistAll = []#list of all sites in ATLAS MRTG monitor
sitelistADCoS = []#ADCoS filtering - list of all sites in ATLAS MRTG monitor without T3s
try:
  response = urllib2.urlopen("http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgatlas2/input.json")
  #response = urllib2.urlopen("file:///home/squidmon/www/snmpstats/mrtgatlas2/input.json")
  siteNew = json.load(response)
except:
  siteNew = []
#with open(inputjson) as f:
  #siteNew = json.load(f)
for agis in siteNew:
  sitelistAll.append(agis[0])
  if agis[1]["tier"] !=3:
    sitelistADCoS.append(agis[0])
sitesDict['sitelistAll'] = sitelistAll
sitesDict['sitelistADCoS'] = sitelistADCoS

idata = {}#time and status of current situation
allDict = {}#time and status of history+current
todayDict = {}#time indexeds (index-in-day index-in-hour) and status of history(1 day)+current
offset1d = 24*3600
offsetLong = 30*24*3600

try:
  f = urllib2.urlopen("http://wlcg-squid-monitor.cern.ch/ssb/availabilityDetails.csv")
  #f = urllib2.urlopen("file:///home/squidmon/www/ssb/availabilityDetails.csv")
  #f = urllib2.urlopen("file:///home/svatos/cernbox/Computing/frontierOperations/squid/squids-SSBreplacement/availabilityDetails.csv")
  reader = csv.reader(f)
  for row in reader:
    site = {}
    siteDict = {}
    siteDict["status"] = row[2].split(' ')[0]
    siteDict["cacheStatus"] = row[3]
    siteDict["downNodes"] = row[4]
    site[row[0].split(" ")[0]+" "+row[0].split(" ")[1].split(":")[0]+":"+row[0].split(" ")[1].split(":")[1]] = siteDict
    idata[row[1]] = site
except:
  pass
#print idata

nowindex = ''
for site in sitelistAll:
  allDict[site] = {}
  todayDict[site] = {}
  #adding current data
  for sitename,sitedetails in idata.iteritems():
    if sitename == site:
      #print sitename,sitedetails[sitedetails.keys()[0]]
      allDict[sitename][sitedetails.keys()[0]] = sitedetails[sitedetails.keys()[0]]#there is only one value of the time in current
      if int(sitedetails.keys()[0].split(" ")[1].split(":")[1]) < 20:#to catch first run of current hour at 00
        nowindex = '1'
        todayDict[sitename][str(sitedetails.keys()[0].split(" ")[1].split(":")[0])+' '+nowindex] = sitedetails[sitedetails.keys()[0]]#there is only one value of the time in current
      elif int(sitedetails.keys()[0].split(" ")[1].split(":")[1]) < 45:#to catch second run of current hour at 25
        nowindex = '2'
        todayDict[sitename][str(sitedetails.keys()[0].split(" ")[1].split(":")[0])+' '+nowindex] = sitedetails[sitedetails.keys()[0]]#there is only one value of the time in current
      elif int(sitedetails.keys()[0].split(" ")[1].split(":")[1]) < 55:#to catch third run of current hour at 50
        nowindex = '3'
        todayDict[sitename][str(sitedetails.keys()[0].split(" ")[1].split(":")[0])+' '+nowindex] = sitedetails[sitedetails.keys()[0]]
  #extracting history data
  if os.path.exists(csvpath+site+'.csv'):
    with open(csvpath+site+'.csv', 'r') as sitehistoryr:
      reader = csv.reader(sitehistoryr)
      tempDict = {}
      for row in reader:
        if row[0] not in allDict[site].keys():#if time is not amongst those already recorded
          tempDict = {}
          tempDict["status"] = row[1]
          try:
            tempDict["cacheStatus"] = row[2]
          except:
            tempDict["cacheStatus"] = ''
          try:
            tempDict["downNodes"] = row[3]
          except:
            tempDict["downNodes"] = ''
          if now - int(calendar.timegm(datetime.strptime(row[0], "%Y-%m-%d %H:%M").utctimetuple()))<offset1d:#just one day of data to display
            if int(row[0].split(" ")[1].split(":")[1]) < 20:
              todayDict[site][str(row[0].split(" ")[1].split(":")[0])+" 1"] = tempDict
            elif int(row[0].split(" ")[1].split(":")[1]) < 45:
              todayDict[site][str(row[0].split(" ")[1].split(":")[0])+" 2"] = tempDict
            elif int(row[0].split(" ")[1].split(":")[1]) < 55:
              todayDict[site][str(row[0].split(" ")[1].split(":")[0])+" 3"] = tempDict
          allDict[site][row[0]] = tempDict
  #writing all back into the files
  with open(csvpath+site+'.csv', 'w') as sitehistoryw:
    writer = csv.writer(sitehistoryw)
    for outtime in sorted(allDict[site].keys()):
      if now - int(calendar.timegm(datetime.strptime(outtime.split(" ")[0], "%Y-%m-%d").utctimetuple()))<offsetLong:#store offsetLong of data in the file
        writer.writerow([outtime,allDict[site][outtime]['status'],allDict[site][outtime]['cacheStatus'],allDict[site][outtime]['downNodes']])
#print nowindex
#print allDict['AGLT2']
#print todayDict['AGLT2']
degradation = {}
for site,sitedet in allDict.iteritems():
  for timing,details in sitedet.iteritems():
    if now - int(calendar.timegm(datetime.strptime(timing, "%Y-%m-%d %H:%M").utctimetuple()))<offset1d:
      if len(details['downNodes']) > 0:
        if site in degradation.keys():
          if timing in degradation[site].keys():
            pass
          else:
            degradation[site][timing] = {}
            degradation[site][timing] = details['downNodes']
          pass
        else:
          degradation[site] = {}
          degradation[site][timing] = {}
          degradation[site][timing] = details['downNodes']
#print degradation
##############################################################
#############################HTML#############################
##############################################################

#####################################################################
#############################Common part#############################
#####################################################################
html = '<!DOCTYPE html>'
html += '<html lang="en">\n'
html += indent[0]+'<head>\n'
html += indent[1]+'<meta charset="utf-8">\n'
html += indent[1]+'<title>ATLAS site squid availability overview</title>\n'
html += indent[1]+'<meta name="viewport" content="width=device-width, initial-scale=1">\n'
html += indent[1]+'<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">\n'
html += indent[1]+'<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">\n'
html += indent[1]+'<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">\n'
html += indent[1]+'<style>\n'
#general style
html += indent[2]+'#body {\n'
html += indent[3]+'margin: 0px;\n'
html += indent[3]+'padding: 0px;\n'
html += indent[2]+'}\n'
html += indent[2]+'#main {\n'
html += indent[3]+'overflow: auto;\n'#If overflow is clipped, a scroll-bar should be added to see the rest of the content
html += indent[2]+'}\n'
html += indent[2]+'#content {\n'
html += indent[3]+'float: left;\n'
html += indent[2]+'}\n'
html += indent[2]+'.row:after {\n'
html += indent[3]+'content: "";\n'
html += indent[3]+'display: table;\n'
html += indent[3]+'clear: both;\n'
html += indent[2]+'}\n'
html += indent[2]+'.okbox{ \n'
html += indent[3]+'background-color: green;\n'
html += indent[3]+'width: 10px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.degbox{ \n'
html += indent[3]+'background-color: orange;\n'
html += indent[3]+'width: 10px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.downbox{ \n'
html += indent[3]+'background-color: red;\n'
html += indent[3]+'width: 10px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.nobox{ \n'
html += indent[3]+'background-color: white;\n'
html += indent[3]+'width: 10px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.gapbox{ \n'
html += indent[3]+'background-color: white;\n'
html += indent[3]+'width: 1px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.headbox{ \n'
html += indent[3]+'background-color: white;\n'
html += indent[3]+'width: 31px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'table, th, td {\n'
html += indent[3]+'border: 1px solid black;\n'
html += indent[3]+'border-collapse: collapse;\n'
html += indent[3]+'border-spacing: 10px 0px;\n'
html += indent[2]+'}\n'
html += indent[1]+'</style>\n'
html += indent[0]+'</head>\n'
#body
html += indent[0]+'<body>\n'

##############################################################
#############################All##############################
##############################################################

for filtering in ['sitelistADCoS','sitelistAll']:
  if filtering == 'sitelistADCoS':
    html += indent[1]+'<div id="adcos" class="w3-container tab">\n'
  elif filtering == 'sitelistAll':
    html += indent[1]+'<div id="all" class="w3-container tab" style="display:none">\n'
  #navbar
  html += indent[2]+'<div class="w3-bar w3-light-grey">\n'
  if filtering == 'sitelistADCoS':
    html += indent[3]+'<button class="w3-bar-item w3-button w3-indigo" onclick="openTab('+"'"+"adcos"+"'"+')">ADCoS filtering</button>\n'
    html += indent[3]+'<button class="w3-bar-item w3-button" onclick="openTab('+"'"+"all"+"'"+')">All</button>\n'
  elif filtering == 'sitelistAll':
    html += indent[3]+'<button class="w3-bar-item w3-button" onclick="openTab('+"'"+"adcos"+"'"+')">ADCoS filtering</button>\n'
    html += indent[3]+'<button class="w3-bar-item w3-button w3-indigo" onclick="openTab('+"'"+"all"+"'"+')">All</button>\n'
  html += indent[3]+'<button class="w3-bar-item w3-button w3-hover-light-grey w3-right" >Start of the last update: '+str(datetime.now())+' (local time)</button>\n'
  html += indent[2]+'</div>\n'
  html += indent[2]+'<h4 style="text-align:center">Availability overview:</h4>\n'
  html += indent[2]+'<table>\n'
  #table header
  html += '<tr><th>Site Name,history<th>downtime </th><th>problematic f/r ratio</th><th>Availability (UTC hours)</th></tr>\n'
  #html += indent[3]+'<tr><th>Site Name</th><th> History record </th><th> Squid currently in downtime? </th><th>Availability (UTC hours)</th></tr>\n'
  #row with hours
  html +=indent[3]+'<tr><td></td><td></td><td></td><td><span class="nobox"></span>'
  for hour in timeline:
    html +='<span class="headbox">'+str(hour).zfill(2)+'</span>'
  html +='</td></tr>\n'

  for site in sitesDict[filtering]:
    availstring = ''
    c = 0
    for hour in timeline:
      c += 1
      if c == 1:
        for hourindex in range(1, int(nowindex)+1):#the beginning of availability - empty offset in the beginning
          availstring += '<span class="nobox"></span>'
        for hourindex in range(int(nowindex)+1,4):
          try:
            if todayDict[site][str(hour).zfill(2)+" "+str(hourindex)]['status'] == "OK":
              availstring += '<span class="okbox"></span>'
            if todayDict[site][str(hour).zfill(2)+" "+str(hourindex)]['status'] == "degraded":
              availstring += '<span class="degbox"></span>'
            if todayDict[site][str(hour).zfill(2)+" "+str(hourindex)]['status'] == "down":
              availstring += '<span class="downbox"></span>'
          except KeyError:
            availstring += '<span class="nobox"></span>'
      elif c > 1 and c < len(timeline):#middle of the availability - everything is full
        for hourindex in range(1,4):
          try:
            if todayDict[site][str(hour).zfill(2)+" "+str(hourindex)]['status'] == "OK":
              availstring += '<span class="okbox"></span>'
            if todayDict[site][str(hour).zfill(2)+" "+str(hourindex)]['status'] == "degraded":
              availstring += '<span class="degbox"></span>'
            if todayDict[site][str(hour).zfill(2)+" "+str(hourindex)]['status'] == "down":
              availstring += '<span class="downbox"></span>'
          except KeyError:
            availstring += '<span class="nobox"></span>'
      elif c == len(timeline):#the end of availability - empty offset at the end
        for hourindex in range(1,int(nowindex)+1):
          try:
            if todayDict[site][str(hour).zfill(2)+" "+str(hourindex)]['status'] == "OK":
              availstring += '<span class="okbox"></span>'
            if todayDict[site][str(hour).zfill(2)+" "+str(hourindex)]['status'] == "degraded":
              availstring += '<span class="degbox"></span>'
            if todayDict[site][str(hour).zfill(2)+" "+str(hourindex)]['status'] == "down":
              availstring += '<span class="downbox"></span>'
          except KeyError:
            availstring += '<span class="nobox"></span>'
        for hourindex in range(int(nowindex)+1,4):#the beginning of availability - empty offset in the beginning
          availstring += '<span class="nobox"></span>'
      availstring += '<span class="gapbox"></span>'
    if site in downDict.keys():
      html +=indent[3]+'<tr><td><a href="http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgatlas2/indexatlas2site'+site+'.html">'+site+'</a>,<a href="'+csvlink+site+'.csv"><i class="fa fa-download"></i></a></td><td bgcolor="lightgray" align="center"><a href="'+downDict[site]+'">YES</a></td><td bgcolor="'+todayDict[site][idata[site].keys()[0].split(" ")[1].split(":")[0]+" "+nowindex]['cacheStatus']+'"></td><td>'+availstring+'</td></tr>\n'
    else:
      html +=indent[3]+'<tr><td><a href="http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgatlas2/indexatlas2site'+site+'.html">'+site+'</a><a href="'+csvlink+site+'.csv"><i class="fa fa-download"></i></a></td><td align="center">NO</td><td bgcolor="'+todayDict[site][idata[site].keys()[0].split(" ")[1].split(":")[0]+" "+nowindex]['cacheStatus']+'"></td><td>'+availstring+'</td></tr>\n'
  html += indent[2]+'</table>\n'

  html += indent[2]+'<div>\n'
  html += indent[3]+'<div style="float:left;width:30%;">\n'
  html += indent[4]+'<h4 style="text-align:center">Legend:</h4>\n'
  html += indent[4]+'<ul>\n'
  html += indent[5]+'<li>Site name</li>\n'
  html += indent[6]+'<ul><li>name of the site with link to its MRTG monitoring page </li></ul>\n'
  html += indent[5]+'<li>history</li>\n'
  html += indent[6]+'<ul><li>link history of states in form of csv file (for easier searching)</li></ul>\n'
  html += indent[5]+'<li>downtime</li>\n'
  html += indent[6]+'<ul><li>If the site\'s squid had a downtime declared on it in AGIS when the script ran. If there was a downtime, a GOCDB link is provided.</li></ul>\n'
  html += indent[5]+'<li>problematic f/r ration</li>\n'
  html += indent[6]+'<ul><li>If current fetch/request ratio of combined site\'s squid(s) is over 0.5, the field will turn red. This means squid has problem with caching as many requests are read from database instead of squids cache.</li></ul>\n'
  html += indent[5]+'<li>Availability overview</li>\n'
  html += indent[6]+'<ul><li>Shows colored field representing availability of sites squid(s). More info at SSB part of <a href="https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/ADCoS#Frontier">ADCoS twiki</a></li></ul>\n'
  html += indent[4]+'</ul>\n'
  html += indent[3]+'</div>\n'

  html += indent[3]+'<div style="float:left;width:60%;">\n'
  html += indent[4]+'<h4 style="text-align:center">Degradation summary:</h4>\n'
  html += indent[4]+'<ul>\n'
  if len(degradation) == 0:
    html += indent[5]+'<li><b>no service is degraded</b></li>\n'
  else:
    for site in sorted(degradation.keys()):
      if site in sitesDict[filtering]:
        html += indent[5]+'<li><b>'+site+'</b></li>\n'
        html += indent[5]+'<ul style="max-height: 40px;overflow-y: auto;">\n'
        for timestamp in reversed(sorted(degradation[site])):
          html += indent[6]+'<li><b>timestamp:</b>'+timestamp+'; <b>machine(s):</b>'+degradation[site][timestamp]+'</li>\n'
        html += indent[5]+'</ul>\n'
  html += indent[4]+'</ul>\n'
  html += indent[3]+'</div>\n'

  html += indent[2]+'</div>\n'
  html += indent[1]+'</div>\n'
  html += '\n'

##################################################################
#############################Finalize#############################
##################################################################
html += indent[1]+'<script>\n'
html += indent[2]+'function openTab(tabName) {\n'
html += indent[3]+'var i;\n'
html += indent[3]+'var x = document.getElementsByClassName("tab");\n'
html += indent[3]+'for (i = 0; i < x.length; i++) {\n'
html += indent[4]+'x[i].style.display = "none";\n'
html += indent[3]+'}\n'
html += indent[3]+'document.getElementById(tabName).style.display = "block";\n'
html += indent[2]+'}\n'
html += indent[1]+'</script>\n'
html += indent[0]+'</body>\n</html>\n'

#fs = open('SquidAvailability.html','w')
fs = open('/home/squidmon/www/snmpstats/SquidAvailability.html','w')
fs.write(html)
fs.close()
print "Page written"
print "script finished:", datetime.now()
