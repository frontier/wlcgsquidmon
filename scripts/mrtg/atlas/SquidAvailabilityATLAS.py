#! /usr/bin/python3
import os
import json
from datetime import datetime, timedelta, timezone
import csv
import glob
import matplotlib
matplotlib.use('Cairo')
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup
import pprint

def fplot(site, xlist, ylist):
  sitename = site.replace('/',',')#there is a site with slash in the name (SUNET/NORDUnet) which breaks storing files "sitename.png" files
  if ',' in sitename:
    for s in sitename.split(','):
      plt.clf()
      #plt.figure(figsize=(12,3))
      plt.ylabel('#hits')
      #plt.title(s,  x=0.5)#do not use plt.title on CC7 because of https://bugzilla.redhat.com/show_bug.cgi?id=1653300
      plt.xticks([])
      plt.bar(xlist, ylist)
      plt.gcf().text(0.4,0.91,s)
      plt.savefig(csvpath+s+'.png', dpi=300, bbox_inches='tight', pad_inches=0.15)
  else:
    plt.clf()
    #plt.figure(figsize=(12,3))
    plt.ylabel('#hits')
    #plt.title(sitename,  x=0.5)#do not use plt.title on CC7 because of https://bugzilla.redhat.com/show_bug.cgi?id=1653300
    plt.xticks([])
    plt.bar(xlist, ylist)
    plt.gcf().text(0.4,0.91,sitename)
    plt.savefig(csvpath+sitename+'.png', dpi=300, bbox_inches='tight', pad_inches=0.15)

def failoverf(sitename,hits):
  if int(hits) > 0:
    return '<div class="profile"><font style="color:white;font-weight:bold;"><span>F</span></font><div class="content"><div class="header"><img src="'+csvlink+sitename+'.png"></div></div></div>'
  else:
    return ''

def FRcolor(nindex):
  if nindex in todayDict[site].keys():
    r = float(todayDict[site][nindex]['maxFR'])
    #https://www.w3schools.com/colors/colors_names.asp
    color = ''
    if r > 0.75:
      color = 'Red'
    elif r > 0.5:
      color = 'Orange'
    elif r > 0.25:
      color = 'Yellow'
    else:
      color = 'White'
  else:
    color = 'White'
    r = 0.00
  return color+'">'+str("{0:.2f}".format(r))

def downtimecolor(sitename):
  if sitename in downDict.keys():
    return '<td bgcolor="lightgray" align="center"><a href="'+downDict[sitename]+'">YES</a></td>'
  else:
    return '<td align="center">NO</td>'

def availabilityBox(site, avstat):
  outbox = ''
  if avstat in todayDict[site].keys():
    if todayDict[site][avstat]['status'] == "OK":
      outbox = '--><span class="okbox">'+failoverf(site,todayDict[site][avstat]['failoverHits'])+'</span><!-- '
    elif todayDict[site][avstat]['status'] == "degraded":
      outbox = '--><span class="degbox">'+failoverf(site,todayDict[site][avstat]['failoverHits'])+'</span><!-- '
    elif todayDict[site][avstat]['status'] == "down":
      outbox = '--><span class="downbox">'+failoverf(site,todayDict[site][avstat]['failoverHits'])+'</span><!-- '
  else:
    outbox = '--><span class="nobox"></span><!-- '
  return outbox

print('ATLAS squid availability script started: '+str(datetime.now(tz=timezone.utc))+' UTC')
csvlink = 'http://wlcg-squid-monitor.cern.ch/snmpstats/csvATLAS/'

if os.path.exists('/home/squidmon/'):
  csvpath = '/home/squidmon/www/snmpstats/csvATLAS/'
  failoverfile = '/home/squidmon/www/failover/failoverATLAS/failover-record-nosquid.tsv'
  inputfile = '/home/squidmon/www/snmpstats/mrtgatlas2/input.json'
  squidstatusfile = '/home/squidmon/www/ssb/availabilityDetailsATLAS.csv'
  outfile = '/home/squidmon/www/snmpstats/SquidAvailabilityATLAS.html'
  downtimesfile = '/home/squidmon/www/cric_atlas_downtimes.json'
else:
  csvpath = 'csvATLAS/'
  failoverfile = 'failover-record-nosquid.tsv'
  inputfile = 'input.json'
  squidstatusfile = 'availabilityDetailsATLAS.csv'
  outfile = 'SquidAvailabilityATLAS.html'
  downtimesfile = 'cric_atlas_downtimes.json'

#First, copy the existing page and set some elements red. So, if the creation of the new version crashes, it would be noticeable.
if os.path.exists(outfile):
  with open(outfile) as of:
    htmltext = BeautifulSoup(of, "html.parser")
    buttons = htmltext.find_all('button')
    for b in buttons:
      if 'Start of the last update' in b.string:
        #if the last update is older than 1 hour - otherwise, when the page is opened while the script is running, it is red even though nothing failed
        if datetime.now(tz=timezone.utc) - timedelta(hours=1) > datetime.strptime(b.string.split('=')[1], "%Y-%m-%d %H:%M").replace(tzinfo=timezone.utc):
          b['class'] = 'w3-bar-item w3-button w3-red w3-right'
  fs = open(outfile,'w')
  fs.write(str(htmltext))
  fs.close()
  
indent = []
stringg = ''
for r in range(0,20):
  stringg += '  '
  indent.append(stringg)

times = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
h = int(datetime.now(tz=timezone.utc).strftime("%H"))
#h = 19
timeline = times[h:]+times[:h]+[h]
now = datetime.now(tz=timezone.utc)

#downtimes
downDict = {}
with open(downtimesfile) as fd:
  downtimes = json.load(fd)
for downsite in sorted(downtimes):
  for downdetail in downtimes[downsite]:
    if "org.squid-cache.Squid" in downdetail["affected_services"] or "Squid" in downdetail["affected_services"]:#GOCDB uses org.squid-cache.Squid, OSG uses Squid
      if datetime.strptime(downdetail["start_time"], "%Y-%m-%dT%H:%M:%S").replace(tzinfo=timezone.utc) < now:#after the beginning of the downtime
        if now < datetime.strptime(downdetail["end_time"], "%Y-%m-%dT%H:%M:%S").replace(tzinfo=timezone.utc):#before the end of the downtime
          downDict[downsite] = downdetail["info_url"]
#pprint.pprint(downDict)

#failover data
failoversites = {}
with open(failoverfile) as ff:
  reader = csv.reader(ff, delimiter='\t')
  for row in reader:
    if row == ['Timestamp', 'Group', 'Sites', 'Host', 'Ip', 'Bandwidth', 'BandwidthRate', 'Hits', 'HitsRate']:
      pass
    else:
      if row[2] in failoversites.keys():
        if int(row[0]) in failoversites[row[2]].keys():
          failoversites[row[2]][int(row[0])] += int(row[7])
        else:
          failoversites[row[2]][int(row[0])] = int(row[7])
      else:
        failoversites[row[2]] = {}
        failoversites[row[2]][int(row[0])] = int(row[7])
#pprint.pprint(failoversites)

sitesDict = {}#dict containing all and adcos lists - site:{timestamp:{status:,maxFR:,downNodes:}}
sitelistAll = []#list of all sites in ATLAS MRTG monitor
sitelistADCoS = []#ADCoS filtering - list of all sites in ATLAS MRTG monitor without T3s
with open(inputfile) as fi:
  siteNew = json.load(fi)

for c in siteNew:
  sitelistAll.append(c[0])
  if c[1]["tier"] !=3:
    sitelistADCoS.append(c[0])
sitesDict['sitelistAll'] = sitelistAll
sitesDict['sitelistADCoS'] = sitelistADCoS

#failover plots
offset1d = 24*3600
x1 = []
#print now, now-offset1d
for b in range(0,24):#from one day ago to one hour ago
  x1.append((int(now.timestamp())-offset1d+1800)+3600*b)#moving one second back to avoid situation when time of failover would be same as bin limit, i.e. avoiding decision to which bin such case should go
#print x1

#remove old plots
for pl in glob.glob(csvpath+'*.png'):
  os.remove(pl)

#if the site is not co-located, use it; if it is colocated, use it only if it was not used before - this way, if there are failovers from site directly (e.g. RAL-LCG2) and from something around (RAL-LCG2,UKI-SOUTHGRID-RALPP) the plot with direct failovers is not overwritten by plot with failovers from the other one
dashplotsites = {}#{name in dashboard:name in failovers}
for s in failoversites.keys():
  if ',' not in s:
    if s in sitelistAll and s not in dashplotsites:
      dashplotsites[s] = s
#all non-co-located sites need to be written first
for s in failoversites.keys():
  if ',' in s:
    for u in s.split(','):
      if u in sitelistAll and u not in dashplotsites:
        dashplotsites[u] = s
#print(dashplotsites)

for s in dashplotsites.keys():
  x = []
  y = []
  counter = 0
  for bbb in x1:
    for t in failoversites[dashplotsites[s]].keys():
      if bbb < t and t < bbb + 3600:#if the time  of failover goes between two bin edges
        y.append(failoversites[dashplotsites[s]][t])
    x.append(counter)
    counter += 1
    if len(y) < len(x):
      y.append(0.001)
  fplot(s,x,y)

idata = {}#time and status of current situation
allDict = {}#time and status of history+current
todayDict = {}#time indexeds (index-in-day index-in-hour) and status of history(1 day)+current
offsetLong = 30*24*3600
#current data
nowindex = ''
with open(squidstatusfile) as fs:
  reader = csv.reader(fs)
  for row in reader:
    site = {}
    siteDict = {}
    siteDict["status"] = row[2]
    siteDict["maxFR"] = row[3]
    siteDict["downNodes"] = row[4]
    site[row[0].split(" ")[0]+" "+row[0].split(" ")[1].split(":")[0]+":"+row[0].split(" ")[1].split(":")[1]] = siteDict
    if len(nowindex) == 0:#fill the index only once
      if int(row[0].split(" ")[1].split(":")[1]) < 20:
        nowindex = '1'
      elif int(row[0].split(" ")[1].split(":")[1]) < 45:
        nowindex = '2'
      elif int(row[0].split(" ")[1].split(":")[1]) < 55:
        nowindex = '3'
    idata[row[1]] = site
#pprint.pprint(idata)

#combining current and old data into one big dictionary + failover data enrichment
for site in sorted(sitelistAll):
  allDict[site] = {}
  todayDict[site] = {}
  #adding current data
  for sitename in idata.keys():
    if sitename == site:
      #print sitename,sitedetails[sitedetails.keys()[0]]
      allDict[sitename][list(idata[sitename])[0]] = idata[sitename][list(idata[sitename])[0]]#there is only one value of the time in current
      allDict[sitename][list(idata[sitename])[0]]['failoverHits'] = 0#for the current squid status, the failover value is in the future
  #extracting historical data
  if os.path.exists(csvpath+site+'.csv'):
    with open(csvpath+site+'.csv', 'r') as sitehistoryr:
      reader = csv.reader(sitehistoryr)
      tempDict = {}
      for row in reader:
        if row[0] not in allDict[site].keys():#if time is not amongst those already recorded
          tempDict = {}
          tempDict["status"] = row[1]
          tempDict["maxFR"] = row[2]
          tempDict["downNodes"] = row[3]
          #check failover only of it is not already done, i.e. if there is just an empty string
          tempDict["failoverHits"] = 0
          for fsite in failoversites.keys():
            if site==fsite or site in fsite.split(','):#match either the sitename or (if the site name from failover is combination of several sites) one of the parts
              for ftime in failoversites[fsite].keys():
                #if time from the bin (read from the file) is less than time from failover (ftime) but more than failover time minus one hour (i.e. if time from the bin belongs to the interval which the failover data represent)
                if datetime.strptime(row[0], "%Y-%m-%d %H:%M") < datetime.fromtimestamp(ftime):
                  if datetime.strptime(row[0], "%Y-%m-%d %H:%M") > datetime.fromtimestamp(ftime) - timedelta(hours=1):
                    tempDict["failoverHits"] = failoversites[fsite][ftime]
          allDict[site][row[0]] = tempDict
  #writing all back into the files
  with open(csvpath+site+'.csv', 'w') as sitehistoryw:
    writer = csv.writer(sitehistoryw)
    for outtime in sorted(allDict[site].keys()):
      if now - timedelta(seconds=offsetLong) < datetime.strptime(outtime, "%Y-%m-%d %H:%M").replace(tzinfo=timezone.utc):#store offsetLong of data in the file
        writer.writerow([outtime,allDict[site][outtime]['status'],allDict[site][outtime]['maxFR'],allDict[site][outtime]['downNodes'],allDict[site][outtime]['failoverHits']])
#pprint.pprint(allDict)

#extracting one day from the all dictionary to be displayed on the webpage
for site in allDict.keys():
  if len(allDict[site]) > 0:#if site has no squids to monitor
    todayDict[site] = {}
    for tim in allDict[site].keys():
      if now - timedelta(seconds=offset1d) < datetime.strptime(tim, "%Y-%m-%d %H:%M").replace(tzinfo=timezone.utc):#just one day of data to display
        nindex = ''
        if int(tim.split(" ")[1].split(":")[1]) < 20:#to catch first run of current hour at 00
          nindex = '1'
          todayDict[site][str(tim.split(" ")[1].split(":")[0])+' '+nindex] = allDict[site][tim]#there is only one value of the time in current
        elif int(tim.split(" ")[1].split(":")[1]) < 45:#to catch second run of current hour at 25
          nindex = '2'
          todayDict[site][str(tim.split(" ")[1].split(":")[0])+' '+nindex] = allDict[site][tim]#there is only one value of the time in current
        elif int(tim.split(" ")[1].split(":")[1]) < 55:#to catch third run of current hour at 50
          nindex = '3'
          todayDict[site][str(tim.split(" ")[1].split(":")[0])+' '+nindex] = allDict[site][tim]
#pprint.pprint(todayDict)

#extracting a list of nodes which are/were down in the last 24 hours - to display on the bottom of the page
degradation = {}
for site in allDict.keys():
  for timing in allDict[site].keys():
    if now - timedelta(seconds=offset1d) < datetime.strptime(timing, "%Y-%m-%d %H:%M").replace(tzinfo=timezone.utc):
      if len(allDict[site][timing]['downNodes']) > 0:
        if site in degradation.keys():
          if timing in degradation[site].keys():
            pass
          else:
            degradation[site][timing] = {}
            degradation[site][timing] = allDict[site][timing]['downNodes']
        else:
          degradation[site] = {}
          degradation[site][timing] = {}
          degradation[site][timing] = allDict[site][timing]['downNodes']
#print(degradation)

##############################################################
#############################HTML#############################
##############################################################

#####################################################################
#############################Common part#############################
#####################################################################
html = '<!DOCTYPE html>'
html += '<html lang="en">\n'
html += indent[0]+'<head>\n'
html += indent[1]+'<meta charset="utf-8">\n'
html += indent[1]+'<title>ATLAS site squid availability overview</title>\n'
html += indent[1]+'<meta name="viewport" content="width=device-width, initial-scale=1">\n'
html += indent[1]+'<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">\n'
html += indent[1]+'<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">\n'
html += indent[1]+'<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">\n'
html += indent[1]+'<style>\n'
#general style
html += indent[2]+'#body {\n'
html += indent[3]+'margin: 0px;\n'
html += indent[3]+'padding: 0px;\n'
html += indent[2]+'}\n'
html += indent[2]+'#main {\n'
html += indent[3]+'overflow: auto;\n'#If overflow is clipped, a scroll-bar should be added to see the rest of the content
html += indent[2]+'}\n'
html += indent[2]+'#content {\n'
html += indent[3]+'float: left;\n'
html += indent[2]+'}\n'
html += indent[2]+'.row:after {\n'
html += indent[3]+'content: "";\n'
html += indent[3]+'display: table;\n'
html += indent[3]+'clear: both;\n'
html += indent[2]+'}\n'
html += indent[2]+'.okbox{ \n'
html += indent[3]+'background-color: green;\n'
html += indent[3]+'width: 10px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.degbox{ \n'
html += indent[3]+'background-color: orange;\n'
html += indent[3]+'width: 10px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.downbox{ \n'
html += indent[3]+'background-color: red;\n'
html += indent[3]+'width: 10px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.nobox{ \n'
html += indent[3]+'background-color: white;\n'
html += indent[3]+'width: 10px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.gapbox{ \n'
html += indent[3]+'background-color: white;\n'
html += indent[3]+'width: 1px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'.headbox{ \n'
html += indent[3]+'background-color: white;\n'
html += indent[3]+'width: 31px;\n'
html += indent[3]+'height: 16px;\n'
html += indent[3]+'display:inline-block\n'
html += indent[2]+'}\n'
html += indent[2]+'table, th, td {\n'
html += indent[3]+'border: 1px solid black;\n'
html += indent[3]+'border-collapse: collapse;\n'
html += indent[3]+'border-spacing: 10px 0px;\n'
html += indent[2]+'}\n'
html += indent[2]+'.profile {\n'
html += indent[3]+'display: inline-block;\n'
html += indent[3]+'margin: 0 0em;\n'
html += indent[3]+'cursor: pointer;\n'
html += indent[3]+'transition: 0.5s;\n'
html += indent[2]+'}\n'
html += indent[2]+'.content {\n'
html += indent[3]+'position: absolute;\n'
html += indent[3]+'visibility: hidden;\n'
html += indent[3]+'opacity: 0;\n'
html += indent[3]+'transition: 0.5s;\n'
html += indent[2]+'}\n'
html += indent[2]+'.content .header {\n'
html += indent[3]+'align-items: center;\n'
html += indent[2]+'}\n'
html += indent[2]+'.content .header img {\n'
html += indent[3]+'width: 50%;\n'
html += indent[3]+'height: 50%;\n'
html += indent[3]+'object-fit: contain;\n'
html += indent[2]+'}\n'
html += indent[2]+'.profile:hover .content {\n'
html += indent[3]+'visibility: visible;\n'
html += indent[3]+'opacity: 1;\n'
html += indent[2]+'}\n'
html += indent[1]+'</style>\n'
html += indent[0]+'</head>\n'
#body
html += indent[0]+'<body>\n'

##############################################################
#############################All##############################
##############################################################

for filtering in ['sitelistADCoS','sitelistAll']:
  if filtering == 'sitelistADCoS':
    html += indent[1]+'<div id="adcos" class="w3-container tab">\n'
  elif filtering == 'sitelistAll':
    html += indent[1]+'<div id="all" class="w3-container tab" style="display:none">\n'
  #navbar
  html += indent[2]+'<div class="w3-bar w3-light-grey">\n'
  if filtering == 'sitelistADCoS':
    html += indent[3]+'<button class="w3-bar-item w3-button w3-indigo" onclick="openTab('+"'"+"adcos"+"'"+')">ADCoS filtering</button>\n'
    html += indent[3]+'<button class="w3-bar-item w3-button" onclick="openTab('+"'"+"all"+"'"+')">All</button>\n'
  elif filtering == 'sitelistAll':
    html += indent[3]+'<button class="w3-bar-item w3-button" onclick="openTab('+"'"+"adcos"+"'"+')">ADCoS filtering</button>\n'
    html += indent[3]+'<button class="w3-bar-item w3-button w3-indigo" onclick="openTab('+"'"+"all"+"'"+')">All</button>\n'
  html += indent[3]+'<button class="w3-bar-item w3-button w3-hover-light-grey w3-right" >Start of the last update (UTC)='+str(datetime.now(tz=timezone.utc).strftime("%Y-%m-%d %H:%M"))+'</button>\n'
  html += indent[2]+'</div>\n'
  html += indent[2]+'<h4 style="text-align:center">Availability overview:</h4>\n'
  html += indent[2]+'<table>\n'
  #table header
  html += '<tr><th>Site Name,history<th>downtime </th><th style="text-align:center">f/r</th><th>Availability (UTC hours)</th></tr>\n'
  #row with hours
  html +=indent[3]+'<tr><td></td><td></td><td></td><td><span class="nobox"></span>'
  for hour in timeline:
    html +='<span class="headbox">'+str(hour).zfill(2)+'</span>'
  html +='</td></tr>\n'

  for site in sitesDict[filtering]:
    if site in list(idata):#if site is decommissioned, it will drop from availability data - then do not include it on the page 
      availstring = ''
      c = 0
      for hour in timeline:
        c += 1
        if c == 1:
          for hourindex in range(1, int(nowindex)+1):#the beginning of availability - empty offset in the beginning
            availstring += '--><span class="nobox"></span><!-- '
          for hourindex in range(int(nowindex)+1,4):
            availstring += availabilityBox(site, str(hour).zfill(2)+" "+str(hourindex))
        elif c > 1 and c < len(timeline):#middle of the availability - everything is full
          for hourindex in range(1,4):
            availstring += availabilityBox(site, str(hour).zfill(2)+" "+str(hourindex))
        elif c == len(timeline):#the end of availability - empty offset at the end
          for hourindex in range(1,int(nowindex)+1):
            availstring += availabilityBox(site, str(hour).zfill(2)+" "+str(hourindex))
          for hourindex in range(int(nowindex)+1,4):#the beginning of availability - empty offset in the beginning
            availstring += '--><span class="nobox"></span><!-- '
        availstring += '--><span class="gapbox"></span><!-- '
      hhour = list(idata[site])[0].split(" ")[1].split(":")[0]
      html +=indent[3]+'<tr><td><a href="http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgatlas2/indexatlas2site'+site+'.html">'+site+'</a>,<a href="'+csvlink+site+'.csv"><i class="fa fa-download"></i></a></td>'+downtimecolor(site)+'<td bgcolor="'+FRcolor(hhour+" "+nowindex)+'</td><td><!-- '+availstring+'--></td></tr>\n'
  html += indent[2]+'</table>\n'

  html += indent[2]+'<div>\n'
  html += indent[3]+'<div style="float:left;width:40%;">\n'
  html += indent[4]+'<h4 style="text-align:center">Page description:</h4>\n'
  html += indent[4]+'<ul>\n'
  html += indent[5]+'<li>Last update (timestamp)</li>\n'
  html += indent[6]+'<ul>\n'
  html += indent[7]+'<li>If the page was not updated for 1 hour, it will turn red. </li>\n'
  html += indent[6]+'</ul>\n'
  html += indent[5]+'<li>Site name</li>\n'
  html += indent[6]+'<ul>\n'
  html += indent[7]+'<li>name of the site with link to its MRTG monitoring page </li>\n'
  html += indent[6]+'</ul>\n'
  html += indent[5]+'<li>history</li>\n'
  html += indent[6]+'<ul>\n'
  html += indent[7]+'<li>link history of states in form of csv file (for easier searching)</li>\n'
  html += indent[6]+'</ul>\n'
  html += indent[5]+'<li>downtime</li>\n'
  html += indent[6]+'<ul>\n'
  html += indent[7]+'<li>If the site\'s squid had a downtime declared on it in AGIS when the script ran. If there was a downtime, a GOCDB link is provided.</li>\n'
  html += indent[6]+'</ul>\n'
  html += indent[5]+'<li>f/r (fetch/request ratio)</li>\n'
  html += indent[6]+'<ul>\n'
  html += indent[6]+'<li>Values of fetch and requests are taken from weekly average in 30 min bins of the MRTG.</li>\n'
  html += indent[6]+'<li>The value is either f/r value of a single squid or, if there are more squids at the site, it is maximum of f/r values. </li>\n'
  html += indent[6]+'<li>If there is less than 500 request on average, the ratio is set to 0 to avoid false alarms caused by inactive squids</li>\n'
  html += indent[7]+'<li>Background color can change depending on the f/r value:</li>\n'
  html += indent[8]+'<ul>\n'
  html += indent[9]+'<li><table style="vertical-align:bottom;border:0;display:inline-block;"><td bgcolor="Red">f/r>0.75</td></table></li>\n'
  html += indent[9]+'<li><table style="vertical-align:bottom;border:0;display:inline-block;"><td bgcolor="Orange">0.75=>f/r>0.5</td></table></li>\n'
  html += indent[9]+'<li><table style="vertical-align:bottom;border:0;display:inline-block;"><td bgcolor="Yellow">0.5=>f/r>0.25</td></table></li>\n'
  html += indent[9]+'<li><table style="vertical-align:bottom;border:0;display:inline-block;"><td bgcolor="White">0.25=>f/r</td></table></li>\n'
  html += indent[8]+'</ul>\n'
  html += indent[7]+'<li>If the f/r value is "high", it means the squid has problem with caching and many requests are read from database instead of squid\'s cache.</li>\n'
  html += indent[6]+'</ul>\n'
  html += indent[5]+'<li>Availability overview</li>\n'
  html += indent[6]+'<ul>\n'
  html += indent[7]+'<li>Shows colored field representing availability of sites squid(s)</li>\n'
  html += indent[8]+'<ul>\n'
  html += indent[9]+'<li><table style="vertical-align:bottom;border:0;display:inline-block;"><span class="okbox"></span></table> in case all site squids are OK</li>\n'
  html += indent[9]+'<li><table style="vertical-align:bottom;border:0;display:inline-block;"><span class="downbox"></span></table> in case all site squids are down</li>\n'
  html += indent[9]+'<li><table style="vertical-align:bottom;border:0;display:inline-block;"><span class="degbox"></span></table> in case there are some OK squids and some down squids</li>\n'
  html += indent[8]+'</ul>\n'
  html += indent[6]+'</ul>\n'
  html += indent[5]+'<li>Failover information enrichment</li>\n'
  html += indent[6]+'<ul>\n'
  html += indent[7]+'<li>if there was a failover from a site on the <a href="http://wlcg-squid-monitor.cern.ch/failover/failoverATLAS/failover.html">failover monitor</a>, it will be displayed as white F in Avalability overview</li>\n'
  html += indent[7]+'<li>hovering over the F will show a plot with information about number of hits from the site</li>\n'
  html += indent[7]+'<li>for sites which share their geoip, F will be displayed for each of them</li>\n'
  html += indent[6]+'</ul>\n'

  html += indent[4]+'</ul>\n'
  html += indent[3]+'</div>\n'

  html += indent[3]+'<div style="float:left;width:40%;">\n'
  html += indent[4]+'<h4 style="text-align:center">Degradation summary:</h4>\n'
  html += indent[4]+'<ul>\n'
  if len(degradation) == 0:
    html += indent[5]+'<li><b>no service is degraded</b></li>\n'
  else:
    for site in sorted(degradation.keys()):
      if site in sitesDict[filtering]:
        html += indent[5]+'<li><b>'+site+'</b></li>\n'
        html += indent[5]+'<ul style="max-height: 40px;overflow-y: auto;">\n'
        for timestamp in reversed(sorted(degradation[site])):
          dsqtext = ''
          for dsq in sorted(degradation[site][timestamp].split(';')):
            if len(dsq) > 0:
              dsqtext += '<a href="http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgall/'+site+'_'+dsq+'/proxy-hit.html">'+dsq+'</a>, '
          html += indent[6]+'<li><b>timestamp: </b>'+timestamp+'; <b>machine(s): </b>'+dsqtext[:-2]+'</li>\n'
        html += indent[5]+'</ul>\n'
  html += indent[4]+'</ul>\n'
  html += indent[3]+'</div>\n'

  html += indent[2]+'</div>\n'
  html += indent[1]+'</div>\n'
  html += '\n'

##################################################################
#############################Finalize#############################
##################################################################
html += indent[1]+'<script>\n'
html += indent[2]+'function openTab(tabName) {\n'
html += indent[3]+'var i;\n'
html += indent[3]+'var x = document.getElementsByClassName("tab");\n'
html += indent[3]+'for (i = 0; i < x.length; i++) {\n'
html += indent[4]+'x[i].style.display = "none";\n'
html += indent[3]+'}\n'
html += indent[3]+'document.getElementById(tabName).style.display = "block";\n'
html += indent[2]+'}\n'
html += indent[1]+'</script>\n'
html += indent[0]+'</body>\n</html>\n'

fs = open(outfile,'w')
fs.write(html)
fs.close()
print('ATLAS squid availability script finished: '+str(datetime.now(tz=timezone.utc))+' UTC')
