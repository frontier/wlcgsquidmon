#! /usr/bin/python3
import os
import json
import datetime
from bs4 import BeautifulSoup, Comment
import csv

def cachingFRratio(name):
  if os.path.exists(name):
    with open(name, 'r') as f:
      htmltext = BeautifulSoup(f, "html.parser")
      comments = htmltext.find_all(text=lambda text:isinstance(text,Comment))
      units = {}
      units['req/min'] = 1
      units['kreq/min'] = 1000
      units['Mreq/min'] = 1000000
      units['Greq/min'] = 1000000000
      for comment in comments:
        if comment == " Begin `Weekly\' Graph (30 Minute interval) ":
          divsection = comment.find_next_sibling('div')
          soup2 = BeautifulSoup(str(divsection), 'html.parser')
          reqs = soup2.find_all("tr", { "class" : "in" })#requests - number of documents client asks from the squid
          fetch = soup2.find_all("tr", { "class" : "out" })#fetches - number of documents squid asks from database
          soup3 = BeautifulSoup(str(reqs), 'html.parser')
          #currentIn = soup3.find_all("td")[2].string
          #currentInValue = float(currentIn.split()[0])
          #currentInUnit = int(units[str(currentIn.split()[1])])
          avgIn = soup3.find_all("td")[1].string#requests
          avgInValue = float(avgIn.split()[0])
          avgInUnit = int(units[str(avgIn.split()[1])])
          soup4 = BeautifulSoup(str(fetch), 'html.parser')
          #currentOut = soup4.find_all("td")[2].string
          #currentOutValue = float(currentOut.split()[0])
          #currentOutUnit   = int(units[str(currentOut.split()[1])])
          avgOut = soup4.find_all("td")[1].string#fetches
          avgOutValue = float(avgOut.split()[0])
          avgOutUnit   = int(units[str(avgOut.split()[1])])
          if avgInValue > 0:
            if avgInValue < 500:
              return float(0)
            else:
              return float((avgOutValue*avgOutUnit)/(avgInValue*avgInUnit))
          else:
            return 'N/A'
      else:
        return 'N/A'

def check(name):
  if os.path.exists(name):
    with open(file_name) as fp:
      for line in fp.readlines():
        if 'time' in line:
          return "OK"
      else:
        fp.seek(0)#after the file was read in for loop, the read cursor is at the end of the file and there is nothing more to read, i.e. fp.read() is empty - seek(0) sets the reading cursor back to the beginning
        htmltext = BeautifulSoup(fp.read(), "html.parser")
        comments = htmltext.find_all(text=lambda text:isinstance(text,Comment))
        weeklyaverage = -0.1
        for comment in comments:
          if comment == " Begin `Weekly\' Graph (30 Minute interval) ":
            weeksection = comment.find_next_sibling('div')
            soup2 = BeautifulSoup(str(weeksection), 'html.parser')
            reqs = soup2.find_all("tr", { "class" : "in" })
            soup3 = BeautifulSoup(str(reqs), 'html.parser')
            currentvalue = soup3.find_all("td")[2].string
            weeklyaverage = currentvalue.split()[0]
        if float(weeklyaverage) > 0:
          return "OK green "
        else:
          return "down red "
  else:
    return "NotAvailable grey "


print('Squid Status for ATLAS - script started: '+str(datetime.datetime.now()))

mrtg_link = "http://wlcg-squid-monitor.cern.ch/snmpstats/mrtgatlas2/indexatlas2siteMRTG_LINK.html"
if os.path.exists('/home/squidmon/'):
  jsons = '/home/squidmon/www/snmpstats/mrtgatlas2'
  ssb_file = '/home/squidmon/www/ssb/frontier-squid.data'
  proxyHit_path = "/home/squidmon/www/snmpstats/mrtgall/"
  outfilepath = '/home/squidmon/www/ssb/availabilityDetailsATLAS.csv'
else:
  jsons = ''
  ssb_file = 'frontier-squid.data'
  proxyHit_path = '../mrtgall/'
  outfilepath = 'availabilityDetailsATLAS.csv'

ssbList = []
now = datetime.datetime.now()
now = now.strftime("%Y-%m-%d %H:%M:%S")

with open(os.path.join(jsons, 'input.json')) as f:
  siteNew = json.load(f)

FILE2 = open (ssb_file,"w")

#print siteNew
with open(outfilepath, 'w') as outFile:
  writer = csv.writer(outFile)
  for i in siteNew:
    downNode = []
    downNodeString = ''
    downcount = 0
    maxfr = float(0)
    nsquids = 0
    for j in i[1]['endpoint']:#from the list of squid names
      for s in j.keys():
        #print(s+' '+str(j[s]))
        if j[s] > 1:#checks machines behind alias
          for m in range(0,j[s]):
            nsquids+=1
            folder=i[1]['rcname']+"_"+s+"_"+str(m)
            file_name = "%s/%s/proxy-hit.html"%(proxyHit_path, folder)#full address on the machine of each proxy-hit.html file
            #print file_name," ",check(file_name)
            squidCachingFRratio = cachingFRratio(file_name)
            if isinstance(squidCachingFRratio, float):
              print(s+'_'+str(m)+' '+check(file_name)+' '+str("{0:.2f}".format(squidCachingFRratio)))
            else:
              print(s+'_'+str(m)+' '+check(file_name)+' '+str(squidCachingFRratio))
            if check(file_name) == "down red ":
              downcount += 1
              downNode.append(s+"_"+str(m))
            if isinstance(squidCachingFRratio, float):
              if squidCachingFRratio > maxfr:
                maxfr = squidCachingFRratio
        else:#for machines without alias
          nsquids+=1#checks all squids whether they are single machine or alias
          folder=i[1]['rcname']+"_"+s
          file_name = "%s/%s/proxy-hit.html"%(proxyHit_path, folder)#full address on the machine of each proxy-hit.html file
          #print file_name," ",check(file_name)
          squidCachingFRratio = cachingFRratio(file_name)
          if isinstance(squidCachingFRratio, float):
            print(s+' '+check(file_name)+' '+str("{0:.2f}".format(squidCachingFRratio)))
          else:
            print(s+' '+check(file_name)+' '+str(squidCachingFRratio))
          if check(file_name) == "down red ":
            downNode.append(s)
            downcount+=1
          if isinstance(squidCachingFRratio, float):
            maxfr = squidCachingFRratio

    for dn in list(set(downNode)):
      downNodeString += dn
      downNodeString += ';'
    #print downNodeString

    if downcount==nsquids:
      ssb = "%s %s %s%s\n"%(now,i[0],"down red ",mrtg_link.replace('MRTG_LINK',i[0]))
      writer.writerow([now,i[0],"down", maxfr, downNodeString])
    elif downcount==0:
      ssb = "%s %s %s%s\n"%(now,i[0],"OK green ",mrtg_link.replace('MRTG_LINK',i[0]))
      writer.writerow([now,i[0],"OK", maxfr, downNodeString])
    else:
      ssb = "%s %s %s%s\n"%(now,i[0],"degraded orange ",mrtg_link.replace('MRTG_LINK',i[0]))
      writer.writerow([now,i[0],"degraded", maxfr, downNodeString])
    ssbList.append(ssb)

ssbList.sort(key=lambda x: x.lower())

FILE2.writelines(ssbList)
FILE2.close()
print('Squid Status for ATLAS - script finished: '+str(datetime.datetime.now()))
