#! /usr/bin/python3
import os
import json
import datetime
from bs4 import BeautifulSoup, Comment
import csv
import pprint
import socket
import maxminddb
from unidecode import unidecode

def simple_get_hosts_ip_addrs(hostname):
  parts = hostname.split('.')#if the input is IPv4 then it is each octet; if the input is squid name then it is just a list of name parts, e.g. ['cache02', 'hep', 'wisc', 'edu']
  if len(parts) == 4 and not isinstance(parts[0], str): #returns true if there are four elements and they are all numbers
    if all((0 <= int(x) < 256) for x in parts):
      ip_addresses = [hostname]
  else:
    try:
      info = socket.getaddrinfo(hostname, 0, socket.AF_INET)
      ip_addresses = list(set(e[4][0] for e in info))#turns set with IP address into list
    except (socket.error, socket.herror):
      parts6 = hostname.split(':')#if the input is IPv6 then it is each octet; if the input is squid name then it is just a list of name parts, e.g.
      if len(parts6) > 1: #as far as I can see, hostnames do not contain colon (:)
        ip_addresses = [hostname]
      else:
        try:
          info6 = socket.getaddrinfo(hostname, 0, socket.AF_INET6)
          ip_addresses = list(set(e[4][0] for e in info6))#turns set with IP address into list
        except (socket.error, socket.herror):
          ip_addresses = ['NoIpFound']
  return ip_addresses

def cachingFRratio(name):
  if os.path.exists(name):
    with open(name, 'r') as f:
      htmltext = BeautifulSoup(f, "html.parser")
      comments = htmltext.find_all(text=lambda text:isinstance(text,Comment))
      units = {}
      units['req/min'] = 1
      units['kreq/min'] = 1000
      units['Mreq/min'] = 1000000
      units['Greq/min'] = 1000000000
      for comment in comments:
        if comment == " Begin `Weekly\' Graph (30 Minute interval) ":
          divsection = comment.find_next_sibling('div')
          soup2 = BeautifulSoup(str(divsection), 'html.parser')
          reqs = soup2.find_all("tr", { "class" : "in" })#requests - number of documents client asks from the squid
          fetch = soup2.find_all("tr", { "class" : "out" })#fetches - number of documents squid asks from database
          soup3 = BeautifulSoup(str(reqs), 'html.parser')
          #currentIn = soup3.find_all("td")[2].string
          #currentInValue = float(currentIn.split()[0])
          #currentInUnit = int(units[str(currentIn.split()[1])])
          avgIn = soup3.find_all("td")[1].string#requests
          avgInValue = float(avgIn.split()[0])
          avgInUnit = int(units[str(avgIn.split()[1])])
          soup4 = BeautifulSoup(str(fetch), 'html.parser')
          #currentOut = soup4.find_all("td")[2].string
          #currentOutValue = float(currentOut.split()[0])
          #currentOutUnit   = int(units[str(currentOut.split()[1])])
          avgOut = soup4.find_all("td")[1].string#fetches
          avgOutValue = float(avgOut.split()[0])
          avgOutUnit   = int(units[str(avgOut.split()[1])])
          if avgInValue > 0:
            if avgInValue < 500:
              return float(0)
            else:
              return float((avgOutValue*avgOutUnit)/(avgInValue*avgInUnit))
          else:
            return 'N/A'
      else:
        return 'N/A'

def check(name):
  if os.path.exists(name):
    with open(file_name) as fp:
      for line in fp.readlines():
        if 'time' in line:
          return "OK"
      else:
        fp.seek(0)#after the file was read in for loop, the read cursor is at the end of the file and there is nothing more to read, i.e. fp.read() is empty - seek(0) sets the reading cursor back to the beginning
        htmltext = BeautifulSoup(fp.read(), "html.parser")
        comments = htmltext.find_all(text=lambda text:isinstance(text,Comment))
        weeklyaverage = -0.1
        for comment in comments:
          if comment == " Begin `Weekly\' Graph (30 Minute interval) ":
            weeksection = comment.find_next_sibling('div')
            soup2 = BeautifulSoup(str(weeksection), 'html.parser')
            reqs = soup2.find_all("tr", { "class" : "in" })
            soup3 = BeautifulSoup(str(reqs), 'html.parser')
            currentvalue = soup3.find_all("td")[2].string
            weeklyaverage = currentvalue.split()[0]
        if float(weeklyaverage) > 0:
          return "OK"
        else:
          return "down"
  else:
    return "NotAvailable"

print('Squid Status for All - script started: '+str(datetime.datetime.now()))

if os.path.exists('/home/squidmon/'):
  jsonpath = '/home/squidmon/www/grid-squids.json'
  proxyHit_path = "/home/squidmon/www/snmpstats/mrtgall/"
  outfilepath = '/home/squidmon/www/ssb/availabilityDetailsAll.csv'
  geoipfile = '/var/lib/cvmfs-server/geo/GeoLite2-City.mmdb'
else:
  jsonpath = 'grid-squids.json'
  proxyHit_path = '../mrtgall'
  outfilepath = 'availabilityDetailsAll.csv'
  geoipfile = 'GeoLite2-City.mmdb'

gireader = maxminddb.open_database(geoipfile)

now = datetime.datetime.now()
now = now.strftime("%Y-%m-%d %H:%M:%S")

with open(jsonpath) as f:
  siteNew = json.load(f)
#pprint.pprint(siteNew)

#adapting content of cms-squids.json from squid-based to site-based
sitesList = []
sitedetails = {}
for squidname in sorted(siteNew.keys()):
  sitesList.append(siteNew[squidname]['name'])
for s in sorted(list(set(sitesList))):
  sitedetails[s] = []
  for squidname in sorted(siteNew.keys()):
    if s == siteNew[squidname]['name']:
      if 'monitored' in siteNew[squidname].keys() and not siteNew[squidname]['monitored']:
        #print "not monitored:", squidname, siteNew[squidname]
        pass
      else:
        endpointdetails = {}
        endpointdetails['squid-name'] = squidname
        endpointdetails['squid-nmachines'] = len(siteNew[squidname]['ips'])
        if siteNew[squidname]['source'] == 'exception':
          ip = simple_get_hosts_ip_addrs(unidecode(squidname))
          if ip[0] == 'NoIpFound':#to change the hostname from unicode to str
            if len(squidname.split('.')[-1]) == 2:#if the last part of the fqdn has two characters, put in into the egi
              endpointdetails['squid-source'] = 'egi;exception'
            else:
              endpointdetails['squid-source'] = 'osg;exception'
          else:
            countrycodes = []
            for ips in ip:
              geoip = gireader.get(ips)
              if geoip is not None:
                #print(squidname)
                #print(gireader.get(ips)['country']['iso_code'])
                countrycodes.append(geoip['country']['iso_code'])
              else:#if the maxminddb seek fails for the IP
                if squidname[-7:] == 'cern.ch':
                  countrycodes.append('CH')
                else:
                  print('---machine '+squidname+' (IP: '+ips+') not known to GeoLite2-City.mmdb---')
                  countrycodes.append('US')#putting unknown machine into the OSG, just to avoid crashing the script
            if list(set(countrycodes))[0] == 'US':
              endpointdetails['squid-source'] = 'osg;exception'
            else:
              endpointdetails['squid-source'] = 'egi;exception'
        else:
          endpointdetails['squid-source'] = siteNew[squidname]['source']
        sitedetails[s].append(endpointdetails)
#pprint.pprint(sitedetails)

inputList = []
for s in sorted(list(set(sitesList)), key=lambda k:k.lower()):
  if len(sitedetails[s]) > 0:#ignore sites without any monitored squid
    siteList = []
    siteList.append(s)
    sdetails = {}
    for squidname in sorted(siteNew.keys()):
      if s == siteNew[squidname]['name']:
        sdetails['endpoints'] = sitedetails[s]
    siteList.append(sdetails)
    inputList.append(siteList)
#pprint.pprint(inputList)

with open(outfilepath, 'w') as outFile:
  writer = csv.writer(outFile)
  for i in inputList:
    #if i[0] == 'BP_CERN':
    #if i[0] == 'AGLT2':
      downNode = []
      downNodeString = ''
      downcount = 0
      maxfr = float(0)
      nsquids = 0
      checkResult = ''
      NAflag = False
      squidSource = []
      sourceString = ''
      for j in i[1]['endpoints']:#from the list of squid names
        if j['squid-nmachines'] > 1:#checks machines behind alias
          for m in range(0,j['squid-nmachines']):
            nsquids+=1
            folder=i[0]+"_"+j['squid-name']+"_"+str(m)
            #print folder
            file_name = "%s/%s/proxy-hit.html"%(proxyHit_path, folder)#full address on the machine of each proxy-hit.html file
            squidCachingFRratio = cachingFRratio(file_name)
            checkResult = check(file_name)
            if isinstance(squidCachingFRratio, float):
              print(j['squid-name']+'_'+str(m)+' '+checkResult+' '+str("{0:.2f}".format(squidCachingFRratio)))
            else:
              print(j['squid-name']+'_'+str(m)+' '+checkResult+' '+str(squidCachingFRratio))
            if checkResult == "NotAvailable":
              NAflag = True
            if checkResult == "down":
              downcount += 1
              downNode.append(j['squid-name']+"_"+str(m))
            if isinstance(squidCachingFRratio, float):
              if squidCachingFRratio > maxfr:
                maxfr = squidCachingFRratio
            squidSource.append(j['squid-source'])
        else:#for machines without alias
          nsquids+=1#checks all squids whether they are single machine or alias
          folder=i[0]+"_"+j['squid-name']
          file_name = "%s/%s/proxy-hit.html"%(proxyHit_path, folder)#full address on the machine of each proxy-hit.html file
          squidCachingFRratio = cachingFRratio(file_name)
          checkResult = check(file_name)
          if isinstance(squidCachingFRratio, float):
            print(j['squid-name']+' '+checkResult+' '+str("{0:.2f}".format(squidCachingFRratio)))
          else:
            print(j['squid-name']+' '+checkResult+' '+str(squidCachingFRratio))
          if checkResult == "NotAvailable":
            NAflag = True
          if checkResult == "down":
            downNode.append(j['squid-name'])
            downcount+=1
          if isinstance(squidCachingFRratio, float):
            maxfr = squidCachingFRratio
          squidSource.append(j['squid-source'])

      for sour in sorted(list(set(squidSource))):
        if len(list(set(squidSource))) == 1:
          sourceString += sour
        else:#it can be either [u'egi', 'egi;exception'] or [u'osg', 'osg;exception']
          if ';' in sour:#I pick egi;exception
            sourceString += sour

      for dn in list(set(downNode)):
        downNodeString += dn
        downNodeString += ';'
      #print downNodeString

      if NAflag:
        writer.writerow([now,i[0],"NotAvailable", maxfr, downNodeString, sourceString])
      else:
        if downcount==nsquids:
          writer.writerow([now,i[0],"down", maxfr, downNodeString, sourceString])
        elif downcount==0:
          writer.writerow([now,i[0],"OK", maxfr, downNodeString, sourceString])
        else:
          writer.writerow([now,i[0],"degraded", maxfr, downNodeString, sourceString])

print('Squid Status for All - script finished: '+str(datetime.datetime.now()))
