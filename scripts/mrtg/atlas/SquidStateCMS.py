#! /usr/bin/python3
import os
import json
import datetime
from bs4 import BeautifulSoup, Comment
import csv
import pprint

def cachingFRratio(name):
  if os.path.exists(name):
    with open(name, 'r') as f:
      htmltext = BeautifulSoup(f, "html.parser")
      comments = htmltext.find_all(text=lambda text:isinstance(text,Comment))
      units = {}
      units['req/min'] = 1
      units['kreq/min'] = 1000
      units['Mreq/min'] = 1000000
      units['Greq/min'] = 1000000000
      for comment in comments:
        if comment == " Begin `Weekly\' Graph (30 Minute interval) ":
          divsection = comment.find_next_sibling('div')
          soup2 = BeautifulSoup(str(divsection), 'html.parser')
          reqs = soup2.find_all("tr", { "class" : "in" })#requests - number of documents client asks from the squid
          fetch = soup2.find_all("tr", { "class" : "out" })#fetches - number of documents squid asks from database
          soup3 = BeautifulSoup(str(reqs), 'html.parser')
          #currentIn = soup3.find_all("td")[2].string
          #currentInValue = float(currentIn.split()[0])
          #currentInUnit = int(units[str(currentIn.split()[1])])
          avgIn = soup3.find_all("td")[1].string#requests
          avgInValue = float(avgIn.split()[0])
          avgInUnit = int(units[str(avgIn.split()[1])])
          soup4 = BeautifulSoup(str(fetch), 'html.parser')
          #currentOut = soup4.find_all("td")[2].string
          #currentOutValue = float(currentOut.split()[0])
          #currentOutUnit   = int(units[str(currentOut.split()[1])])
          avgOut = soup4.find_all("td")[1].string#fetches
          avgOutValue = float(avgOut.split()[0])
          avgOutUnit   = int(units[str(avgOut.split()[1])])
          if avgInValue > 0:
            if avgInValue < 500:
              return float(0)
            else:
              return float((avgOutValue*avgOutUnit)/(avgInValue*avgInUnit))
          else:
            return 'N/A'
      else:
        return 'N/A'

def check(name):
  if os.path.exists(name):
    with open(file_name, 'r') as fp:
      for line in fp.readlines():
        if 'time' in line:
          return "OK"
      else:
        fp.seek(0)#after the file was read in for loop, the read cursor is at the end of the file and there is nothing more to read, i.e. fp.read() is empty - seek(0) sets the reading cursor back to the beginning
        htmltext = BeautifulSoup(fp.read(), "html.parser")
        comments = htmltext.find_all(text=lambda text:isinstance(text,Comment))
        weeklyaverage = -0.1
        for comment in comments:
          if comment == " Begin `Weekly\' Graph (30 Minute interval) ":
            weeksection = comment.find_next_sibling('div')
            soup2 = BeautifulSoup(str(weeksection), 'html.parser')
            reqs = soup2.find_all("tr", { "class" : "in" })
            soup3 = BeautifulSoup(str(reqs), 'html.parser')
            currentvalue = soup3.find_all("td")[2].string
            weeklyaverage = currentvalue.split()[0]
        if float(weeklyaverage) > 0:
          return "OK"
        else:
          return "down"
  else:
    return "NotAvailable"

print('Squid Status for CMS - script started: '+str(datetime.datetime.now()))

if os.path.exists('/home/squidmon/'):
  jsonpath = '/home/squidmon/www/cms-squids.json'
  proxyHit_path = "/home/squidmon/www/snmpstats/mrtgall/"
  outfilepath = '/home/squidmon/www/ssb/availabilityDetailsCMS.csv'
else:
  jsonpath = 'cms-squids.json'
  proxyHit_path = '../mrtgall'
  outfilepath = 'availabilityDetailsCMS.csv'

ssbList = []
now = datetime.datetime.now()
now = now.strftime("%Y-%m-%d %H:%M:%S")

with open(jsonpath) as f:
  siteNew = json.load(f)
#pprint.pprint(siteNew)

#adapting content of cms-squids.json from squid-based to site-based
sitesList = []
sitedetails = {}
for squidname in sorted(siteNew.keys()):
  sitesList.append(siteNew[squidname]['name'])
for s in sorted(list(set(sitesList))):
  sitedetails[s] = []
  for squidname in sorted(siteNew.keys()):
    if s == siteNew[squidname]['name']:
      if 'monitored' in siteNew[squidname].keys() and not siteNew[squidname]['monitored']:
        #print "not monitored:", squidname, siteNew[squidname]
        pass
      else:
        endpointdetails = {}
        endpointdetails['squid-name'] = squidname
        endpointdetails['squid-nmachines'] = len(siteNew[squidname]['ips'])
        endpointdetails['squid-sitename'] = siteNew[squidname]['gridname']#one CMS site can be several LCG sites, e.g. T3_US_OSG includes BNL-ATLAS, SU-ITS, MWT2, and AGLT2
        sitedetails[s].append(endpointdetails)
#pprint.pprint(sitedetails)

inputList = []
for s in sorted(list(set(sitesList))):
  if len(sitedetails[s]) > 0:#ignore sites without any monitored squid
    siteList = []
    siteList.append(s)
    sdetails = {}
    for squidname in sorted(siteNew.keys()):
      if s == siteNew[squidname]['name']:
        sdetails['endpoints'] = sitedetails[s]
    siteList.append(sdetails)
    inputList.append(siteList)
#pprint.pprint(inputList)

with open(outfilepath, 'w') as outFile:
  writer = csv.writer(outFile)
  for i in inputList:
    downNode = {}
    downNodeString = ''
    downNodePathString = ''
    downcount = 0
    maxfr = float(0)
    nsquids = 0
    checkResult = ''
    NAflag = False
    for j in i[1]['endpoints']:#from the list of squid names
      if j['squid-nmachines'] > 1:#checks machines behind alias
        for m in range(0,j['squid-nmachines']):
          nsquids+=1
          folder=j['squid-sitename']+"_"+j['squid-name']+"_"+str(m)
          #print folder
          file_name = "%s/%s/proxy-hit.html"%(proxyHit_path, folder)#full address on the machine of each proxy-hit.html file
          squidCachingFRratio = cachingFRratio(file_name)
          checkResult = check(file_name)
          if isinstance(squidCachingFRratio, float):
            print(j['squid-name']+'_'+str(m)+' '+checkResult+' '+str("{0:.2f}".format(squidCachingFRratio)))
          else:
            print(j['squid-name']+'_'+str(m)+' '+checkResult+' '+str(squidCachingFRratio))
          if checkResult == "NotAvailable":
            NAflag = True
          if checkResult == "down":
            downcount += 1
            downNode[j['squid-name']+"_"+str(m)] = folder
          if isinstance(squidCachingFRratio, float):
            if squidCachingFRratio > maxfr:
              maxfr = squidCachingFRratio
      else:#for machines without alias
        nsquids+=1#checks all squids whether they are single machine or alias
        folder=j['squid-sitename']+"_"+j['squid-name']
        file_name = "%s/%s/proxy-hit.html"%(proxyHit_path, folder)#full address on the machine of each proxy-hit.html file
        squidCachingFRratio = cachingFRratio(file_name)
        checkResult = check(file_name)
        if isinstance(squidCachingFRratio, float):
          print(j['squid-name']+'_'+checkResult+' '+str("{0:.2f}".format(squidCachingFRratio)))
        else:
          print(j['squid-name']+'_'+checkResult+' '+ str(squidCachingFRratio))
        if checkResult == "NotAvailable":
          NAflag = True
        if checkResult == "down":
          downNode[j['squid-name']] = folder
          downcount+=1
        if isinstance(squidCachingFRratio, float):
          maxfr = squidCachingFRratio

    for dn in list(set(downNode.keys())):
      downNodeString += dn
      downNodeString += ';'
      downNodePathString += downNode[dn]
      downNodePathString += ';'
      #print downNodeString

    if NAflag:
      writer.writerow([now,i[0],"NotAvailable", maxfr, downNodeString, downNodePathString])
    else:
      if downcount==nsquids:
        writer.writerow([now,i[0],"down", maxfr, downNodeString, downNodePathString])
      elif downcount==0:
        writer.writerow([now,i[0],"OK", maxfr, downNodeString, downNodePathString])
      else:
        writer.writerow([now,i[0],"degraded", maxfr, downNodeString, downNodePathString])

print('Squid Status for CMS - script finished: '+str(datetime.datetime.now()))
