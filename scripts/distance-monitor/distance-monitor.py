"""
Prepare information for monitoring of average distance between client and server
"""


from __future__ import print_function
import argparse
import csv
import json
import math
import os
import socket
from datetime import datetime
from time import time
import pytz
import maxminddb


# coordinates_cache has IP address as key and (latitude, longitude) as value
coordinates_cache = {}
# distances_cache has (client, server) as key and distance as value
distances_cache = {}
# awstats_information has awstats name as key and
# {"hostname": hostname1, "site_project": site_project1} as value
awstats_information = {}


def get_coordinates(ip, gireader):
    """
    Get coordinates from IP address
    """
    if ip in coordinates_cache:
        return coordinates_cache[ip]

    latitude = None
    longitude = None
    gir = gireader.get(ip)
    if gir and "location" in gir:
        location = gir["location"]
        if "latitude" in location and "longitude" in location:
            latitude = location["latitude"]
            longitude = location["longitude"]

    coordinates_cache[ip] = (latitude, longitude)

    return latitude, longitude


def get_distance(client, server, gireader):
    """
    Get distance in kilometers between client and server
    """
    cache_key = (client, server)
    if cache_key in distances_cache:
        return distances_cache[cache_key]

    if client in ["127.0.0.1", "::1"]:
        distance = 0
    else:
        latitude_client, longitude_client = get_coordinates(client, gireader)
        latitude_server, longitude_server = get_coordinates(server, gireader)
        distance = 20000 # the longest distance in kilometers between two points
        if latitude_client is not None and longitude_client is not None:
            distance = 6373 * distance_on_unit_sphere(latitude_client,
                                                      longitude_client,
                                                      latitude_server,
                                                      longitude_server)

    distances_cache[cache_key] = distance

    return distance


def distance_on_unit_sphere(lat1, long1, lat2, long2):
    """
    Compute the distance between two locations on Earth from coordinates
    Function came from http://www.johndcook.com/python_longitude_latitude.html
    """
    if (lat1 == lat2) and (long1 == long2):
        return 0.0

    # Convert latitude and longitude to
    # spherical coordinates in radians.
    degrees_to_radians = math.pi / 180.0

    # phi = 90 - latitude
    phi1 = (90.0 - lat1) * degrees_to_radians
    phi2 = (90.0 - lat2) * degrees_to_radians

    # theta = longitude
    theta1 = long1 * degrees_to_radians
    theta2 = long2 * degrees_to_radians

    # Compute spherical distance from spherical coordinates.

    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) =
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length

    cos = (math.sin(phi1) * math.sin(phi2) * math.cos(theta1 - theta2) +
           math.cos(phi1) * math.cos(phi2))
    arc = math.acos(cos)

    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    return arc


def get_ip(hostname):
    """
    Get IP address from hostname
    """
    try:
        ip = socket.getaddrinfo(hostname, None)
        ip = sorted(ip, key=lambda x: 1 if x[0] == socket.AF_INET6 else 0)
        ip = ip[0][4][0]
    except:
        ip = None

    return ip


def file_exists(file_name):
    """
    Check if file or directory exists
    """
    if not os.path.exists(os.path.expanduser(file_name)):
        print("ERROR: file or directory %s does not exist" % file_name)
        return False

    return True


def get_pages_list(pages_dictionary, timestamp, group):
    """
    Convert pages dictionary to pages list
    """
    pages_list = []
    for page, page_value in pages_dictionary.items():
        hits_values = page_value.values()
        hits_sum = sum(hits_values)
        if hits_sum < 1000:
            continue
        for distance, hits in page_value.items():
            pages_list.append([timestamp, group, page, distance, hits])

    return pages_list


def get_pages_dictionary(data, gireader):
    """"
    Create dictionary where page as key and {distance1: hits1, distance2: hits2, ...} as value
    """
    pages_dictionary = {}
    for element in data:
        page = element[1]
        if page not in pages_dictionary:
            pages_dictionary[page] = {}

        distance = get_distance(element[0], element[3], gireader)
        if distance not in pages_dictionary[page]:
            pages_dictionary[page][distance] = 0

        pages_dictionary[page][distance] += int(element[2])

    return pages_dictionary


def get_today_stats(file_name, timezone):
    """
    Get today's stats of one servers group
    """
    today_stats = []
    if file_exists(file_name):
        print("INFO: reading previous server group records from %s " % (file_name))
        with open(os.path.expanduser(file_name), "r") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter="\t")
            for row in csv_reader:
                # Ignore lines that are empty
                if not row:
                    continue

                # If comment with Day has different day of the year than today - return current
                # stats list - an empty list
                # Ignore comments in general
                if row[0].startswith("#"):
                    if row[0] == "#Day":
                        if int(row[1]) != datetime.now(pytz.timezone(timezone)).timetuple().tm_yday:
                            return today_stats
                    continue

                today_stats.append(row)
    else:
        directory_name = file_name[:file_name.rfind("/")]
        if not file_exists(directory_name):
            os.makedirs(os.path.expanduser(directory_name), 0o755)

    return today_stats


def get_current_stats(awstats, awstats_directory, timezone, gireader):
    """
    Get stats of one servers group
    """
    current_stats = []
    today = datetime.now(pytz.timezone(timezone)).strftime("%m%Y%d")
    for awstats_name in awstats:
        print("INFO: processing server %s" % (awstats_name))
        awstats_child = awstats_information.get(awstats_name)
        if not awstats_child:
            print("ERROR: can not get awstats information of %s" % (awstats_name))
            continue

        parent_name = awstats_name
        awstats_parent = awstats_child
        if '-' in awstats_name:
            possible_parent_name = awstats_name[:awstats_name.rfind("-")]
            possible_awstats_parent = awstats_information.get(possible_parent_name)
            if possible_awstats_parent:
                parent_name = possible_parent_name
                awstats_parent = possible_awstats_parent

        pages_file = "%s%s/%s/pages%s.%s.txt" % (awstats_directory,
                                                 awstats_parent["site_project"],
                                                 awstats_name,
                                                 today,
                                                 parent_name)
        if not file_exists(pages_file):
            continue

        server_hostname = awstats_parent["hostname"]
        server_ip = get_ip(server_hostname)
        if not server_ip:
            print("ERROR: can not get IP address of %s" % (server_hostname))
            continue

        latitude_server, longitude_server = get_coordinates(server_ip, gireader)
        if not latitude_server or not longitude_server:
            print("ERROR: can not get latitute and longitude of %s" % (server_ip))
            continue

        current_stats = current_stats + get_stats_with_server(pages_file, server_ip)

    return current_stats


def get_revised_stats(current_stats, today_stats):
    """
    Get stats between today and current run of this script
    """
    print("INFO: getting stats between today and current run of this script")
    if not today_stats:
        return current_stats

    revised_stats = []
    for current_stats_element in current_stats:
        client = current_stats_element[0]
        page = current_stats_element[1]
        server = current_stats_element[3]
        for today_stats_element in today_stats:
            if (client == today_stats_element[0] and
                page == today_stats_element[1] and
                server == today_stats_element[3]):
                hits = int(current_stats_element[2]) - int(today_stats_element[2])
                if hits == 0:
                    break

                revised_stats.append([client, page, hits, server])
                break
        else:
            revised_stats.append([client, page, current_stats_element[2], server])

    return revised_stats


def get_timestamp_stats(file_name, lower_threshold):
    """
    Get stats of experiment servers groups that are not older then threshold
    """
    stats = []
    print("INFO: reading previous monitoring records from %s " % (file_name))
    with open(os.path.expanduser(file_name), "r") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter="\t")
        for index, row in enumerate(csv_reader):
            # Skip header - first line
            if index == 0:
                continue

            # Ignore lines that are empty or start with #
            if not row or row[0].startswith("#"):
                continue

            # If first column value - timestamp - is lower than threshold then ignore this line
            row[0] = int(row[0])
            if row[0] < lower_threshold:
                continue

            stats.append(row)

    return stats


def get_stats_with_server(file_name, server_ip):
    """
    Get stats of one server and add server IP address
    """
    stats = []
    print("INFO: reading server %s records from %s" % (server_ip, file_name))
    with open(os.path.expanduser(file_name), "r") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=" ")
        for row in csv_reader:
            # Ignore lines that are empty or start with #
            if not row or row[0].startswith("#"):
                continue

            row.append(server_ip)
            stats.append(row)

    return stats


def update_file(file_name, data, *args):
    """
    Update CSV file
    """
    directory_name = file_name[:file_name.rfind("/")]
    if not file_exists(file_name[:file_name.rfind("/")]):
        os.makedirs(os.path.expanduser(directory_name), 0o755)
    print("INFO: updating file %s" % (file_name))
    file_name_new = file_name + ".new"
    with open(file_name_new, "w") as csv_file:
        csv_writer = csv.writer(csv_file, delimiter="\t")
        for arg in args:
            csv_writer.writerow(arg)
        for element in data:
            csv_writer.writerow(element)

    os.rename(file_name_new, file_name)


def fill_awstats_information(nodes_mapping_file):
    """
    Create dictionary where awstats name as key and
    {"hostname": hostname1, "site_project": site_project1} as value
    """
    print("INFO: reading nodes mapping file information from %s" % (nodes_mapping_file))
    with open(os.path.expanduser(nodes_mapping_file)) as f:
        for line in f:
            values = line.split()
            site_project = values[0]
            hostname = values[1]
            awstats_name = values[2]
            awstats_information[awstats_name] = {"hostname": hostname, "site_project": site_project}

    return len(awstats_information) > 0


def create_monitoring_config(config_data, directory, groups):
    """
    Create config for monitoring
    """
    monitoring_config = {
        "experiment": config_data["experiment"],
        "groups": {key: value.get("name", key) for key, value in groups.items()},
        "history": config_data["history"],
    }
    file_name = '%sconfig_%s.json' % (os.path.expanduser(directory), config_data["experiment"])
    print("INFO: creating monitoring config %s" % (file_name))
    file_name_new = file_name + ".new"
    with open(file_name_new, "w") as json_file:
        json.dump(monitoring_config, json_file)
    os.rename(file_name_new, file_name)


def create_symbolic_links(source_directory, destination_directory, files):
    """
    Create symbolic links
    """
    source_directory = os.path.expanduser(source_directory)
    if not file_exists(source_directory):
        print("ERROR: symbolic links can not be created from source %s" % (source_directory))
        return

    destination_directory = os.path.expanduser(destination_directory)
    for f in files:
        source_file = source_directory + f
        if not file_exists(source_file):
            print("ERROR: symbolic link can not be created from source %s" % (source_file))
            continue

        destination_file = destination_directory + f
        if os.path.islink(destination_file):
            if os.readlink(destination_file) != source_file:
                print("INFO: creating symbolic link %s -> %s" % (destination_file, source_file))
                destination_file_new = destination_file + ".new"
                os.symlink(source_file, destination_file_new)
                os.rename(destination_file_new, destination_file)
        else:
            print("INFO: creating symbolic link %s -> %s" % (destination_file, source_file))
            os.symlink(source_file, destination_file)


def read_from_config(config, config_name, attribute, default_value=None):
    """
    Read a value from config, return default value or raise exception if value does not exist
    """
    value = config.get(attribute)
    if not value:
        if default_value is None:
            print('ERROR: configuration file %s is missing "%s"' % (config_name, attribute))
            raise Exception('Configuration file %s is missing "%s"' % (config_name, attribute))

        return default_value

    return value


def update_groups(groups, stratum1_config):
    """
    Update groups by merging dictionaries
    """
    with open(os.path.expanduser(stratum1_config)) as json_file:
        data = json.load(json_file)
    groups.update(data)

    return groups


def main():
    """
    Prepare information for monitoring of average distance between client and server
    """
    print("INFO: started at " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", required=True, help="Get information from configuration file")
    parser.add_argument("--test", action="store_true", help="Add output files to current directory")
    args = parser.parse_args()
    test = args.test
    config_file = args.config

    if not file_exists(config_file):
        return

    with open(config_file) as json_file:
        try:
            config_data = json.load(json_file)
        except Exception as ex:
            print("ERROR: error reading %s: %s" % (config_file, ex))
            return

    if test:
        etc_directory = "etc/"
        www_directory = "www/"
    else:
        etc_directory = read_from_config(config_data, config_file, "etc_directory")
        www_directory = read_from_config(config_data, config_file, "www_directory")

    wwwsrc_directory = read_from_config(config_data, config_file, "wwwsrc_directory")
    experiment = read_from_config(config_data, config_file, "experiment")
    awstats_directory = read_from_config(config_data, config_file, "awstats_directory")
    symbolic_links = read_from_config(config_data, config_file, "symbolic_links")
    nodes_mapping_file = read_from_config(config_data, config_file, "nodes_mapping_file")
    geo_db = read_from_config(config_data, config_file, "geo_db")
    groups = read_from_config(config_data, config_file, "groups", {})
    history = read_from_config(config_data, config_file, "history")
    span = history.get("span", 72)
    stratum1_config = read_from_config(config_data, config_file, "stratum1_config", "")

    if not file_exists(nodes_mapping_file):
        return

    if not fill_awstats_information(nodes_mapping_file):
        print("ERROR: nodes mapping file %s is empty" % (nodes_mapping_file))
        return

    try:
        gireader = maxminddb.open_database(geo_db)
    except Exception as ex:
        print("ERROR: can not open database %s: %s" % (geo_db, ex))
        return

    if stratum1_config and file_exists(stratum1_config):
        groups = update_groups(groups, stratum1_config)

    timestamp_now = int(time())
    pages_list = []
    for group_key, group_value in groups.items():
        print("INFO: processing group %s" % (group_key))
        awstats = group_value.get("awstats")
        if not awstats:
            print('ERROR: configuration file %s is missing "awstats" in group %s' %
                  (config_file, group_key))
            continue

        timezone = group_value.get("timezone")
        if not timezone:
            print('ERROR: configuration file %s is missing "timezone" in group %s' %
                  (config_file, group_key))
            continue

        file_today_stats = etc_directory + "today_stats_" + experiment + "_" + group_key + ".tsv"
        today_stats = get_today_stats(file_today_stats, timezone)
        current_stats = get_current_stats(awstats, awstats_directory, timezone, gireader)
        revised_stats = get_revised_stats(current_stats, today_stats)
        pages_dictionary = get_pages_dictionary(revised_stats, gireader)
        pages_list += get_pages_list(pages_dictionary, timestamp_now, group_key)
        update_file(os.path.expanduser(file_today_stats),
                    current_stats,
                    ["#Day", datetime.now(pytz.timezone(timezone)).timetuple().tm_yday],
                    ["#Client", "URL", "Hits", "Server"])

    records_file = www_directory + "records_" + experiment + ".tsv"
    if file_exists(records_file):
        lower_threshold = timestamp_now - span * 3600
        pages_list += get_timestamp_stats(records_file, lower_threshold)

    update_file(os.path.expanduser(records_file),
                pages_list,
                ["Timestamp", "Group", "Page", "Distance", "Hits"])
    create_monitoring_config(config_data, www_directory, groups)
    create_symbolic_links(wwwsrc_directory, www_directory, symbolic_links)
    print("INFO: finished at " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


if __name__ == "__main__":
    main()
